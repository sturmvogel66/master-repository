# OTO - Otomis
# 2013-aug-07 - GG - EUIV changes

government = tribal_monarchy
mercantilism = 0.1
primary_culture = chichimecha
religion = aztec_reformed
technology_group = mesoamerican
capital = 2404

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1   = {
	monarch = {
		name = "Monarch"
		dynasty = "Meztitlan"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
