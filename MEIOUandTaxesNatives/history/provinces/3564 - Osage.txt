# No previous file for Osage

owner = OSA
controller = OSA
add_core = OSA
is_city = yes
culture = osagee
religion = totemism
capital = "Osage"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 30
native_ferocity = 3
native_hostileness = 6

1554.1.1  = {  }
1760.1.1  = { trade_goods = wheat } #Arkansas Osage migrate southward
1808.11.10 = { owner = USA
		controller = USA
		add_core = USA
		remove_core = OSA
		culture = american
		religion = protestant
	is_city = yes
} #Treaty of Fort Clark
