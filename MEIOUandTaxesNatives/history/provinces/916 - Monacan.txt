# No previous file for Monacan

owner = POW
controller = POW
add_core = POW
is_city = yes
culture = powhatan
religion = totemism
capital = "Monacan"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 35
native_ferocity = 3
native_hostileness = 4

1584.1.1 = {  }
1644.1.1 = {	owner = ENG 
		controller = ENG 
		culture = english 
		trade_goods = tobacco
		is_city = yes
		religion = protestant } #Settlement of the Northern Neck after second Anglo-Powhatan war
1669.1.1 = { add_core = ENG }
1707.5.12 = {
	
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
} 
1749.5.2 = { capital = "Alexandria" } #settlements expand west
1750.1.1  = { citysize = 1980 }
1764.7.1  = { culture = american unrest = 6 } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
	    } # Declaration of independence
1782.11.1 = { unrest = 0 remove_core = GBR } # Preliminary articles of peace, the British recognized American independence
1800.1.1  = { citysize = 2900 }
