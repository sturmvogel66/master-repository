name="MEIOU and Taxes Units Module v1.0"
path="mod/MEIOUandTaxesUnits"
dependencies={
	"MEIOU and Taxes 1.27"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesUS.jpg"
supported_version="1.19.*.*"
