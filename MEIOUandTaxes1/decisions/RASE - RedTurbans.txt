country_decisions = {

	core_zhili = {
		potential = {
			culture_group = chinese_group
			hebei_area = { NOT = { is_core = ROOT } }
			NOT = { has_country_flag = claimed_zhili }
		}
		allow = {
			hebei_area = { type = all owned_by = ROOT }
		}
		effect = {
			hebei_area = { add_core = ROOT }
			set_country_flag = claimed_zhili
		}
		ai_will_do = {
			factor = 1
		}
	}
#	core_nanzhili = {
#		potential = {
#			culture_group = chinese_group
#			NOT = { has_country_flag = claimed_nanzhili }
#		}
#		allow = {
#			nanzhili = { type = all owned_by = ROOT }
#		}
#		effect = {
#			nanzhili = { add_core = ROOT }
#			set_country_flag = claimed_nanzhili
#		}
#		ai_will_do = {
#			factor = 1
#		}
#	}
	
	core_shandong_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_shandong }
			shandong_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			shandong_area = { type = all owned_by = ROOT }
		}
		effect = {
			shandong_area = { add_core = ROOT }
			set_country_flag = claimed_shandong
		}
		ai_will_do = {
			factor = 1
		}
	}

	core_jiangsu_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_jiangsu }
			jiangsu_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			jiangsu_area = { type = all owned_by = ROOT }
		}
		effect = {
			jiangsu_area = { add_core = ROOT }
			set_country_flag = claimed_jiangsu
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	core_anhui_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_anhui }
			anhui_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			anhui_area = { type = all owned_by = ROOT }
		}
		effect = {
			anhui_area = { add_core = ROOT }
			set_country_flag = claimed_anhui
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	core_zhejiang_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_zhejiang }
			zhejiang_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			zhejiang_area = { type = all owned_by = ROOT }
		}
		effect = {
			zhejiang_area = { add_core = ROOT }
			set_country_flag = claimed_zhejiang
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	core_fujian_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_fujian }
			fujian_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			fujian_area = { type = all owned_by = ROOT }
		}
		effect = {
			fujian_area = { add_core = ROOT }
			set_country_flag = claimed_fujian
		}
		ai_will_do = {
			factor = 1
		}
	}

	core_guangdong_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_guangdong }
			guangdong_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			guangdong_area = { type = all owned_by = ROOT }
		}
		effect = {
			guangdong_area = { add_core = ROOT }
			set_country_flag = claimed_guangdong
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	core_guangxi_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_guangxi }
			guangxi_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			guangxi_area = { type = all owned_by = ROOT }
		}
		effect = {
			guangxi_area = { add_core = ROOT }
			set_country_flag = claimed_guangxi
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	core_shanxi_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_shanxi }
			shanxi_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			shanxi_area = { type = all owned_by = ROOT }
		}
		effect = {
			shanxi_area = { add_core = ROOT }
			set_country_flag = claimed_shanxi
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	core_gansu_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_gansu }
			gansu_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			gansu_area = { type = all owned_by = ROOT }
		}
		effect = {
			gansu_area = { add_core = ROOT }
			set_country_flag = claimed_gansu
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	core_henan_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_henan }
			henan_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			henan_area = { type = all owned_by = ROOT }
		}
		effect = {
			henan_area = { add_core = ROOT }
			set_country_flag = claimed_henan
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	core_jiangxi_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_jiangxi }
			jiangxi_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			jiangxi_area = { type = all owned_by = ROOT }
		}
		effect = {
			jiangxi_area = { add_core = ROOT }
			set_country_flag = claimed_jiangxi
		}
		ai_will_do = {
			factor = 1
		}
	}
	
#	core_huguang = {
#		potential = {
#			culture_group = chinese_group
#			NOT = { has_country_flag = claimed_huguang }
#		}
#		allow = {
#			huguang = { type = all owned_by = ROOT }
#		}
#		effect = {
#			huguang = { add_core = ROOT }
#			set_country_flag = claimed_huguang
#		}
#		ai_will_do = {
#			factor = 1
#		}
#	}

	core_hunan_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_hunan }
			hunan_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			hunan_area = { type = all owned_by = ROOT }
		}
		effect = {
			hunan_area = { add_core = ROOT }
			set_country_flag = hunan
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	core_hubei_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_hubei }
			hubei_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			hubei_area = { type = all owned_by = ROOT }
		}
		effect = {
			hubei_area = { add_core = ROOT }
			set_country_flag = claimed_hubei
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	core_guizhou_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_guizhou }
			guizhou_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			guizhou_area = { type = all owned_by = ROOT }
		}
		effect = {
			guizhou_area = { add_core = ROOT }
			set_country_flag = claimed_guizhou
		}
		ai_will_do = {
			factor = 1
		}
	}
	
#	core_shaangan = {
#		potential = {
#			culture_group = chinese_group
#			NOT = { has_country_flag = claimed_shaangan }
#		}
#		allow = {
#			shaangan = { type = all owned_by = ROOT }
#		}
#		effect = {
#			shaangan = { add_core = ROOT }
#			set_country_flag = claimed_shaangan
#		}
#		ai_will_do = {
#			factor = 1
#		}
#	}
	
	core_shaanxi_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_shaanxi }
			shaanxi_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			shaanxi_area = { type = all owned_by = ROOT }
		}
		effect = {
			shaanxi_area = { add_core = ROOT }
			set_country_flag = claimed_shaanxi
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	core_sichuan_area = {
		potential = {
			culture_group = chinese_group
			NOT = { has_country_flag = claimed_sichuan }
			sichuan_area = { NOT = { is_core = ROOT } }
		}
		allow = {
			sichuan_area = { type = all owned_by = ROOT }
		}
		effect = {
			sichuan_area = { add_core = ROOT }
			set_country_flag = claimed_sichuan
		}
		ai_will_do = {
			factor = 1
		}
	}
	
#	commented out because yunnan_area wasn't Chinese early game
#	core_yunnan_area = {
#		potential = {
#			culture_group = chinese_group
#			NOT = { has_country_flag = claimed_yunnan }
#			yunnan_area = { NOT = { is_core = ROOT } }
#		}
#		allow = {
#			yunnan_area = { type = all owned_by = ROOT }
#		}
#		effect = {
#			yunnan_area = { add_core = ROOT }
#			set_country_flag = claimed_yunnan
#		}
#		ai_will_do = {
#			factor = 1
#		}
#	}
	
}
