country_decisions = {

	raise_court_level = {
		potential = {
			ai = no
			NOT = { has_country_flag = court_best }
			has_global_flag = courtsetup
			}
		allow = {
			has_regency = no
			is_colonial_nation = no
			}
		effect = {
			add_dip_power = -25
			clr_country_flag = court_good
			random_list = {
				30 = { add_ruler_modifier = { name = indulgent } }
				30 = { add_ruler_modifier = { name = generous } }
				40 = { }
				}
			if = { 
				limit = { has_country_modifier = court_better }
				clr_country_flag = court_better
				remove_country_modifier = court_better
				set_country_flag = court_best
				add_country_modifier = { name = court_best duration = 7300 }
				if = { 
					limit = { #Rule for life
						#Countries that have shortened life spans
						NOT = { government = papal_government }
						#Countries that have 7-8 year terms
						NOT = { government = noble_republic }
						NOT = { government = colonial_government }
						#Countries that have 4-5 year terms
						NOT = { government = merchant_republic }
						NOT = { government = administrative_republic }
						NOT = { government = constitutional_republic }
						NOT = { government = siena_republic }
						NOT = { government = revolutionary_republic }
						NOT = { government = imperial_city }
						NOT = { government = merchant_imperial_city }
						NOT = { government = ambrosian_republic }
						}
					if = { #Full cost
						limit = { NOT = { monthly_income = 8 } }
						add_treasury = -6.25
						}
					if = { 
						limit = { monthly_income = 8 }
						add_years_of_income = -0.0625
						}
					}
				if = { 
					limit = { #7-8 year terms
						OR = { 
							government = papal_government
							government = noble_republic
							government = colonial_government
							}
						}
					if = { #50% discount
						limit = { NOT = { monthly_income = 8 } }
						add_treasury = -3
						}
					if = { 
						limit = { monthly_income = 8 }
						add_years_of_income = -0.0312
						}
					}
				if = { 
					limit = { #4-5 year terms
						OR = { 
							government = merchant_republic
							government = administrative_republic
							government = constitutional_republic
							government = siena_republic
							government = revolutionary_republic
							government = imperial_city
							government = merchant_imperial_city
							government = ambrosian_republic
							}
						}
					if = { #75% discount
						limit = { NOT = { monthly_income = 8 } }
						add_treasury = -1.5
						}
					if = { 
						limit = { monthly_income = 8 }
						add_years_of_income = -0.015
						}
					}
				}
			if = { 
				limit = { 
					NOT = { has_country_modifier = court_better }
					NOT = { has_country_modifier = court_best }
					NOT = { has_country_modifier = court_worst }
					NOT = { has_country_modifier = court_bad }
					}
				clr_country_flag = court_good
				add_country_modifier = { name = court_better duration = 7300 }
				set_country_flag = court_better
				if = { 
					limit = { #Rule for life
						#Countries that have shortened life spans
						NOT = { government = papal_government }
						#Countries that have 7-8 year terms
						NOT = { government = noble_republic }
						NOT = { government = colonial_government }
						#Countries that have 4-5 year terms
						NOT = { government = merchant_republic }
						NOT = { government = administrative_republic }
						NOT = { government = constitutional_republic }
						NOT = { government = siena_republic }
						NOT = { government = revolutionary_republic }
						NOT = { government = imperial_city }
						NOT = { government = merchant_imperial_city }
						NOT = { government = ambrosian_republic }
						}
					if = { #Full cost
						limit = { NOT = { monthly_income = 8 } }
						add_treasury = -3
						}
					if = { 
						limit = { monthly_income = 8 }
						add_years_of_income = -0.0312
						}
					}
				if = { 
					limit = { #7-8 year terms
						OR = { 
							government = papal_government
							government = noble_republic
							government = colonial_government
							}
						}
					if = { #50% discount
						limit = { NOT = { monthly_income = 8 } }
						add_treasury = -1.5
						}
					if = { 
						limit = { monthly_income = 8 }
						add_years_of_income = -0.015
						}
					}
				if = { 
					limit = { #4-5 year terms
						OR = { 
							government = merchant_republic
							government = administrative_republic
							government = constitutional_republic
							government = siena_republic
							government = revolutionary_republic
							government = imperial_city
							government = merchant_imperial_city
							government = ambrosian_republic
							}
						}
					if = { #75% discount
						limit = { NOT = { monthly_income = 8 } }
						add_treasury = -1
						}
					if = { 
						limit = { monthly_income = 8 }
						add_years_of_income = -0.0075
						}
					}
				}
			if = { 
				limit = { has_country_modifier = court_bad }
				clr_country_flag = court_bad
				remove_country_modifier = court_bad
				clr_country_flag = court_good
				set_country_flag = court_good
				if = { 
					limit = { #Rule for life
						#Countries that have shortened life spans
						NOT = { government = papal_government }
						#Countries that have 7-8 year terms
						NOT = { government = noble_republic }
						NOT = { government = colonial_government }
						#Countries that have 4-5 year terms
						NOT = { government = merchant_republic }
						NOT = { government = administrative_republic }
						NOT = { government = constitutional_republic }
						NOT = { government = siena_republic }
						NOT = { government = revolutionary_republic }
						NOT = { government = imperial_city }
						NOT = { government = merchant_imperial_city }
						NOT = { government = ambrosian_republic }
						}
					if = { #Full cost
						limit = { NOT = { monthly_income = 8 } }
						add_treasury = -1.5
						}
					if = { 
						limit = { monthly_income = 8 }
						add_years_of_income = -0.015
						}
					}
				if = { 
					limit = { #7-8 year terms
						OR = { 
							government = papal_government
							government = noble_republic
							government = colonial_government
							}
						}
					if = { #50% discount
						limit = { NOT = { monthly_income = 8 } }
						add_treasury = -1
						}
					if = { 
						limit = { monthly_income = 8 }
						add_years_of_income = -0.0075
						}
					}
				if = { 
					limit = { #4-5 year terms
						OR = { 
							government = merchant_republic
							government = administrative_republic
							government = constitutional_republic
							government = siena_republic
							government = revolutionary_republic
							government = imperial_city
							government = merchant_imperial_city
							government = ambrosian_republic
							}
						}
					if = { #75% discount
						limit = { NOT = { monthly_income = 8 } }
						add_treasury = -0.5
						}
					if = { 
						limit = { monthly_income = 8 }
						add_years_of_income = -0.0032
						}
					}
				}
			if = { 
				limit = { has_country_modifier = court_worst }
				clr_country_flag = court_worst
				remove_country_modifier = court_worst
				set_country_flag = court_bad
				add_country_modifier = { name = court_bad duration = 7300 }
				if = { 
					limit = { #Rule for life
						#Countries that have shortened life spans
						NOT = { government = papal_government }
						#Countries that have 7-8 year terms
						NOT = { government = noble_republic }
						NOT = { government = colonial_government }
						#Countries that have 4-5 year terms
						NOT = { government = merchant_republic }
						NOT = { government = administrative_republic }
						NOT = { government = constitutional_republic }
						NOT = { government = siena_republic }
						NOT = { government = revolutionary_republic }
						NOT = { government = imperial_city }
						NOT = { government = merchant_imperial_city }
						NOT = { government = ambrosian_republic }
						}
					if = { #Full cost
						limit = { NOT = { monthly_income = 8 } }
						add_treasury = -1
						}
					if = { 
						limit = { monthly_income = 8 }
						add_years_of_income = -0.0075
						}
					}
				if = { 
					limit = { #7-8 year terms
						OR = { 
							government = papal_government
							government = noble_republic
							government = colonial_government
							}
						}
					if = { #50% discount
						limit = { NOT = { monthly_income = 8 } }
						add_treasury = -0.5
						}
					if = { 
						limit = { monthly_income = 8 }
						add_years_of_income = -0.0032
						}
					}
				if = { 
					limit = { #4-5 year terms
						OR = { 
							government = merchant_republic
							government = administrative_republic
							government = constitutional_republic
							government = siena_republic
							government = revolutionary_republic
							government = imperial_city
							government = merchant_imperial_city
							government = ambrosian_republic
							}
						}
					if = { #75% discount
						limit = { NOT = { monthly_income = 8 } }
						add_treasury = -0.25
						}
					if = { 
						limit = { monthly_income = 8 }
						add_years_of_income = -0.002
						}
					}
				}
			}
		ai_will_do = {
			factor = 0
			}
		}

}
