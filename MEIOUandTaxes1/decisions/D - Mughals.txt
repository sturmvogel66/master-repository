########################################
#                                      #
#      Mughals.txt      #
#                                      #
########################################
#
# added this decision because there is a posibility that the Mughals can lose India in 1530
# This gives them the ability to regroup and not disappear, since Kabul provided the Timurids with a lot of support
#
########################################

country_decisions = {
	become_kabulistan  = {
		potential = {
			tag = MUG
			NOT = { check_variable = { which = "Demesne_in_India" value = 15 } }
			owns = 451
			NOT = { owns = 2163 } #Delhi
			NOT = { exists = DUR }
		}
		allow = {
			NOT = {	owns = 2113 } #Lahore
			is_at_war = no
		}
		effect = {
			afghanistan_region = { limit = { owned_by = ROOT } remove_core = DUR add_core = DUR }
			afghanistan_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = DUR }
			change_tag = DUR
			DUR = {
				change_primary_culture = pashtun
				change_unit_type = muslim
				change_government = despotic_monarchy
				set_government_rank = 4
				set_capital = 451
			}
			add_stability_3 = yes
			add_years_of_income = 1.25
			add_legitimacy = 75
			increase_centralisation = yes
		}
		ai_will_do = {
			factor = 1
		}
	}
}