#by Marco Dandolo

country_decisions = {
	
	renaissance_architecture = {
		potential = {
			NOT = { has_country_flag = cultural_decision_off }
			OR = {
			    technology_group = western
				culture_group = west_slavic
			}
			NOT = { adm_tech = 28 }
			NOT = {
			    capital_scope = {
				    has_province_modifier = renaissance_city
				}
			}
			NOT = {
			    capital_scope = {
				    has_province_modifier = renaissance_city_wip
					has_province_modifier = baroque_city_wip
					has_province_modifier = rokoko_city_wip
					has_province_modifier = classical_city_wip
				}
			}
			NOT = { has_country_flag = new_capital_architecture }
			NOT = { has_country_flag = renaissance_architecture }
		}
		allow = {			
			adm_tech = 16 #basic Renaissance
			adm = 2 #not a complete idiot
			advisor = architect
			manpower = 0.5
			years_of_income = 1
		}
	 	effect = {
			add_years_of_income = -0.8
			add_manpower = -0.15
			every_province = {
					limit = {
							is_capital = yes
							owned_by = ROOT
					}
				add_province_modifier = {
					name = "renaissance_city_wip"
					duration = -1
				}
			}
			set_country_flag = renaissance_architecture
			country_event = { id = architecture.1 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	baroque_architecture = {
		potential = {
			NOT = { has_country_flag = cultural_decision_off }
			OR = {
			    technology_group = western
				culture_group = west_slavic
			}
			adm_tech = 25
			NOT = { adm_tech = 37 }
			NOT = {
			    capital_scope = {
				    has_province_modifier = baroque_city
				}
			}
			NOT = {
			    capital_scope = {
					has_province_modifier = renaissance_city_wip
				    has_province_modifier = baroque_city_wip
					has_province_modifier = rokoko_city_wip
					has_province_modifier = classical_city_wip
				}
			}
			NOT = { has_country_flag = new_capital_architecture }
			NOT = { has_country_flag = baroque_architecture }
		}
		allow = {			
			adm_tech = 28 #basic Baroque
			adm = 2 #not a complete idiot
			advisor = architect
			manpower = 0.5
			years_of_income = 1
		}
	 	effect = {
			add_years_of_income = -0.8
			add_manpower = -0.15
			every_province = {
					limit = {
							is_capital = yes
							owned_by = ROOT
					}
				add_province_modifier = {
					name = "baroque_city_wip"
					duration = -1
				}
			}
			country_event = { id = architecture.1 }
			set_country_flag = baroque_architecture
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	Roccoco_architecture = {
		potential = {
			NOT = { has_country_flag = cultural_decision_off }
			OR = {
			    technology_group = western
				culture_group = west_slavic
			}
			adm_tech = 34
			NOT = { adm_tech = 47 }
			NOT = {
			    capital_scope = {
				    has_province_modifier = rokoko_city
				}
			}
			NOT = {
			    capital_scope = {
				    has_province_modifier = renaissance_city_wip
					has_province_modifier = baroque_city_wip
					has_province_modifier = rokoko_city_wip
					has_province_modifier = classical_city_wip
				}
			}
			NOT = { has_country_flag = new_capital_architecture }
			NOT = { has_country_flag = roccoco_architecture }
		}
		allow = {			
			adm_tech = 36 #basic Rokoko
			adm = 2 #not a complete idiot
			advisor = architect
			manpower = 0.5
			years_of_income = 1
		}
	 	effect = {
			add_years_of_income = -0.8
			add_manpower = -0.15
			every_province = {
					limit = {
							is_capital = yes
							owned_by = ROOT
					}
				add_province_modifier = {
					name = "rokoko_city_wip"
					duration = -1
				}
			}
			country_event = { id = architecture.1 }
			set_country_flag = roccoco_architecture
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	classical_architecture = {
		potential = {
			NOT = { has_country_flag = cultural_decision_off }
			OR = {
			    technology_group = western
				culture_group = west_slavic
			}
			adm_tech = 44
			NOT = {
			    capital_scope = {
				    has_province_modifier = classical_city
				}
			}
			NOT = {
			    capital_scope = {
					has_province_modifier = renaissance_city_wip
					has_province_modifier = baroque_city_wip
					has_province_modifier = rokoko_city_wip
				    has_province_modifier = classical_city_wip
				}
			}
			NOT = { has_country_flag = new_capital_architecture }
			NOT = { has_country_flag = classical_architecture }
		}
		allow = {			
			adm_tech = 45 #basic Classicism
			adm = 2 #not a complete idiot
			advisor = architect
			manpower = 0.5
			years_of_income = 1
		}
	 	effect = {
			add_years_of_income = -0.8
			add_manpower = -0.15
			every_province = {
					limit = {
							is_capital = yes
							owned_by = ROOT
					}
				add_province_modifier = {
					name = "classical_city_wip"
					duration = -1
				}
			}
			country_event = { id = architecture.1 }
			set_country_flag = classical_architecture
		}
		ai_will_do = {
			factor = 1
		}
	}
}
