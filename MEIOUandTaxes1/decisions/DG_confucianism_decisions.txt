country_decisions = {

	adopt_confucianism = { 
		potential = {
			OR = { 
				dominant_religion = confucianism
				has_country_flag = was_confucianism
				}
			NOT = { religion = confucianism }
			any_owned_province = { 
				religion = confucianism
				OR = { 
					has_owner_culture = yes
					has_owner_accepted_culture = yes
					AND = { 
						is_core = owner
						is_in_capital_area = yes
						}
					}
				}
			}
		allow = { 
			is_force_converted = no
			stability = 1
			}
		effect = { 
			if = { 
				limit = { 
					NOT = { num_of_religion = { religion = confucianism value = 0.25 } }
					}
				custom_tooltip = convert_population_0
				subtract_stability_3 = yes
				}
			if = { 
				limit = { 
					num_of_religion = { religion = confucianism value = 0.25 }
					NOT = { dominant_religion = confucianism }
					}
				custom_tooltip = convert_population_25
				subtract_stability_2 = yes
				}
			if = { 
				limit = { 
					dominant_religion = confucianism
					NOT = { num_of_religion = { religion = confucianism value = 0.75 } }
					}
				custom_tooltip = convert_population_50
				subtract_stability_1 = yes
				}
			if = { 
				limit = { 
					dominant_religion = confucianism
					num_of_religion = { religion = confucianism value = 0.75 }
					}
				custom_tooltip = convert_population_75
				}
			if = { 
				limit = { patriarch_authority = 0.75 }
				custom_tooltip = convert_church_strong
				subtract_stability_3 = yes
				}
			if = { 
				limit = { 
					patriarch_authority = 0.50
					NOT = { patriarch_authority = 0.75 }
					}
				custom_tooltip = convert_piety_moderate
				subtract_stability_2 = yes
				}
			if = { 
				limit = { 
					patriarch_authority = 0.25
					NOT = { patriarch_authority = 0.50 }
					}
				custom_tooltip = convert_church_weak
				subtract_stability_1 = yes
				}
			if = { 
				limit = { NOT = { patriarch_authority = 0.25 } }
				custom_tooltip = convert_church_insignificant
				}
			if = {
				limit = { 
					NOT = { capital_scope = { religion = confucianism } }
					NOT = { ADM = 3 }
					}
				custom_tooltip = convert_ruler_incompetent
				}
			if = {
				limit = { 
					capital_scope = { religion = confucianism }
					NOT = { ADM = 3 }
					}
				custom_tooltip = convert_ruler_weak
				add_stability_1 = yes
				}
			if = {
				limit = { 
					NOT = { capital_scope = { religion = confucianism } }
					ADM = 3
					NOT = { ADM = 5 }
					}
				custom_tooltip = convert_ruler_weak
				add_stability_1 = yes
				}
			if = {
				limit = { 
					capital_scope = { religion = confucianism }
					ADM = 3
					NOT = { ADM = 5 }
					}
				custom_tooltip = convert_ruler_moderate
				add_stability_2 = yes
				}
			if = {
				limit = { 
					NOT = { capital_scope = { religion = confucianism } }
					ADM = 5
					}
				custom_tooltip = convert_ruler_moderate
				add_stability_2 = yes
				}
			if = {
				limit = { 
					capital_scope = { religion = confucianism }
					ADM = 5
					}
				custom_tooltip = convert_ruler_strong
				add_stability_3 = yes
				}
			if = {
				limit = { advisor = philosopher }
				custom_tooltip = convert_philosopher
				add_stability_1 = yes
				}
			if = {
				limit = { 
					NOT = { has_country_flag = was_confucianism }
					check_variable = { which = sympathy_to_old_religion value = 1 }
					}
				custom_tooltip = convert_recent
				add_stability_1 = yes
				}
			if = {
				limit = { 
					has_country_flag = was_confucianism
					NOT = { check_variable = { which = sympathy_to_old_religion value = 1 } }
					}
				custom_tooltip = convert_previous_old
				add_stability_1 = yes
				}
			if = {
				limit = { 
					has_country_flag = was_confucianism
					check_variable = { which = sympathy_to_old_religion value = 1 }
					}
				custom_tooltip = convert_previous_recent
				add_stability_2 = yes
				}
			change_religion = confucianism
			}
		ai_will_do = { 
			factor = 1 
			}
		}

}