# Special Missions

reclaim_jerusalem = {

	type = country
	
	category = MIL

	target_provinces = {
		province_id = 379
	}
	allow = {
		NOT = { is_year = 1600 }
		mil = 4
		#Judea must be owned by non-christians.
		379 = {	
			owner = { 
				NOT = { religion_group = christian } 
			}
		}
		religion_group = christian
		NOT = { has_country_flag = reclaimed_jerusalem }
		is_at_war = no
		monthly_income = 80
		is_subject = no
		capital_scope = {
			continent = europe
		}
	}
	abort = {
		OR = {
			NOT = { religion_group = christian }
			379 = {
				owner = { 
					religion_group = christian
				}
			}
			is_subject = yes
		}
	}
	success = {
		379 = { owned_by = ROOT }
	}
	chance = {
		factor = 10000
		modifier = {
			factor = 0.05
			is_year = 1500
		}
	}
	immediate = {
		add_claim = 379
	}
	abort_effect = {
		remove_claim = 379
	}
	effect = {
		add_prestige = 40
		add_treasury = 1000
		set_country_flag = reclaimed_jerusalem
		add_ruler_modifier = {
			name = liberator_of_jerusalem
		}
		if = {
			limit = {
				379 = { NOT = { is_core = ROOT } }
			}
			379 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


reclaim_mecca = {

	type = country
	
	category = MIL

	target_provinces = {
		province_id = 385
	}
	allow = {
		#Mecca must be owned by non-muslims.
		385 = {	
			owner = { 
				NOT = { religion_group = muslim } 
			}
		}
		religion_group = muslim
		is_at_war = no
		is_subject = no
		NOT = { has_country_flag = reclaimed_mecca }
		monthly_income = 80
	}
	abort = {
		OR = {
			NOT = { religion_group = muslim }
			385 = {
				owner = { 
					religion_group = muslim
				}
			}
			is_subject = yes
		}
	}
	success = {
		385 = { owned_by = ROOT }
	}
	chance = {
		factor = 10000
	}
	immediate = {
		add_claim = 385
	}
	abort_effect = {
		remove_claim = 385
	}
	effect = {
		add_prestige = 40
		add_treasury = 1000
		set_country_flag = reclaimed_mecca
		add_ruler_modifier = {
			name = liberator_of_mecca
		}
		if = {
			limit = {
				385 = { NOT = { is_core = ROOT } }
			}
			385 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}
