########################################
# Flavor Events for Vientiane
#
# Scripted by Sara Wendel-�rtqvist
########################################


# The Succession Crisis
country_event = {
	id = flavor_vie.1 # id = 3903
	title = "flavor_vie.EVTNAME1"
	desc = "flavor_vie.EVTDESC1"
	picture = ANGRY_MOB_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = VIE
		owns = 615
		NOT = { exists = LUA }
		is_year = 1700
		NOT = { is_year = 1750 }
	}
	
	mean_time_to_happen = {
		months = 200
	}
	
	option = {
		name = "flavor_vie.EVTOPTA1"
			every_owned_province = {
				limit = {
					or = {
						province_id = 615
						province_id = 2729
						province_id = 2730
						province_id = 2731
					}
				}
			add_province_modifier = {
				name = "nationalists_organizing"
				duration = 3650
			}
			add_unrest = 10	
		}
	}
	option = {
		name = "flavor_vie.EVTOPTB1"
		subtract_stability_1 = yes
		every_owned_province = { add_unrest = 5 }
	}
}
