########################################
# Vietnamese events
#
# written by Fryz and chatnoir17
########################################
### Death of Kim Nguyen
country_event = {
	id = hundred_viet.1
	
	title = hundred_viet.1.t
	desc = hundred_viet.1.d

	picture = BATTLE_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		NOT = { has_global_flag = kim_nguyen_dead }
		has_global_flag = 1530_start_date
		tag = ANN
		NOT = { exists = TOK }
	}
		
	
	option = {
		name = "hundred_viet.1a" # Trinh take over completely
		ai_chance = { factor = 20 }
		set_global_flag = kim_nguyen_dead
		change_tag = TOK
		define_ruler = {
			name = "Kiem"
			dynasty = "Trinh"
			DIP = 3
			ADM = 3
			MIL = 4
			attach_leader = "Kiem Trinh"
		}
		define_heir = {
			claim = 100
			dynasty = "Trinh"
			name = "Coi"
			adm = 3
			dip = 3
			mil = 3
			birth_date = 1550.1.1
		}
		hidden_effect = {
			remove_country_modifier = nguyen_trinh_alliance
			VUU = { remove_country_modifier = nguyen_trinh_alliance }
			every_province = {
				limit = {
					region = vietnam_region
					owner = { primary_culture = vietnamese }
				}
				add_core = owner
			}
		}
		random_owned_province = {
			limit = {
				culture = vietnamese
			}
			add_province_modifier = {
				name = "nobles_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
		
	}
	option = {
		name = "hundred_viet.1b" # Grant territories to Nguyen Hoang
		trigger = {
			any_owned_province = {
				area = annam_area
			}
		}
		ai_chance = { factor = 80 }
		set_global_flag = kim_nguyen_dead
		every_owned_province = {
			limit = {
				OR = {
					area = vietnam_area
					area = north_laos_area
				}
			}
			add_core = TOK
		}
		release = TOK
		hidden_effect = {
			TOK = {
				define_ruler = {
					name = "Kiem"
					dynasty = "Trinh"
					DIP = 3
					ADM = 3
					MIL = 4
					attach_leader = "Kiem Trinh"
				}
				define_heir = {
					claim = 100
					dynasty = "Trinh"
					name = "Coi"
					adm = 3
					dip = 3
					mil = 3
					birth_date = 1550.1.1
				}
				infantry = capital
				infantry = capital
				infantry = capital
				infantry = capital
				infantry = capital
				infantry = capital
				infantry = capital
				cavalry = capital
				cavalry = capital
				artillery = capital
				artillery = capital
			}
			remove_country_modifier = nguyen_trinh_alliance
			VUU = { remove_country_modifier = nguyen_trinh_alliance }
			every_province = {
				limit = {
					region = vietnam_region
					owner = { primary_culture = vietnamese }
				}
				add_core = owner
			}
			every_province = {
				limit = {
					area = vietnam_area
				}
				add_core = TOK
			}
			every_province = {
				limit = {
					area = annam_area
				}
				add_core = ANN
			}
		}
	}
}

### Fall of Mac
country_event = {
	id = hundred_viet.2
	
	title = hundred_viet.2.t
	desc = hundred_viet.2.d

	picture = BATTLE_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	
	option = {
		name = "hundred_viet.2a" # We are fleeing
		set_global_flag = fall_of_mac
		change_tag = MAC
		set_government_rank = 4
		add_prestige = -20
		set_capital = 2393
		every_owned_province = {
			add_unrest = 5
		}
	}
}

## Dai Viet Flavor Eent

# The Rise of Mac Dang Dong

country_event = {
	id = flavor_psmod.15
	
	title = flavor_psmod.EVTNAME15
	desc = flavor_psmod.EVTDESC15

	picture = BAD_WITH_MONARCH_eventPicture	
	
	fire_only_once = yes
	
	trigger = {
		tag = DAI
		is_year = 1483
		NOT = { is_year = 1543 }
		ANN = { exists = no }
		TOK = { exists = no }
	}
	
	mean_time_to_happen = {
		months = 600
		modifier = {
			factor = 0.5
			NOT = { stability = 1 }
		}
	}
	
	option = {
		name = "flavor_psmod.EVTOPTA15" # Glory for the Mac Dynasty!
		ai_chance = { factor = 80 }
		define_ruler = {
			name = "Dang Dong"
			dynasty = "Mac"
		}
		add_manpower = -2
		random_owned_province = {
			limit = {
				culture = vietnamese
				NOT = { has_province_modifier = nationalists_organizing }
			}
			add_province_modifier = {
				name = "nationalists_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
		random_owned_province = {
			limit = {
				culture = vietnamese
				NOT = { has_province_modifier = nationalists_organizing }
			}
			add_province_modifier = {
				name = "nationalists_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
		random_owned_province = {
			limit = {
				culture = vietnamese
				NOT = { has_province_modifier = nationalists_organizing }
			}
			add_province_modifier = {
				name = "nationalists_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
	}
	
	option = {
		name = "flavor_psmod.EVTOPTB15" # The Le dynasty should rule Dai Viet forever!
		ai_chance = { factor = 20 }
		subtract_stability_1 = yes
		add_mil_power = -50
		capital_scope = {
			add_province_modifier = {
				name = "pretender_organizing"
				duration = 3650
			}
			add_unrest = 10
		}
	}
}

# Civil Wars between the Nguyen and Trinh Lords (Nguyen Version)

country_event = {
	id = flavor_psmod.16
	
	title = flavor_psmod.EVTNAME16
	desc = flavor_psmod.EVTDESC16

	picture = BATTLE_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		NOT = { has_global_flag = nguyen_trinh_civil_war }
		OR = {
			tag = DAI
			tag = TOK
		}
		ANN = { exists = no }
		is_year = 1525
		NOT = { is_year = 1635 }
	}
	
	mean_time_to_happen = {
		months = 1200
		modifier = {
			factor = 0.5
			NOT = { stability = 1 }
		}
	}
	
	option = {
		name = "flavor_psmod.EVTOPTA16" # The Trinh family is my sworn enemy!
		ai_chance = { factor = 80 }
		set_global_flag = nguyen_trinh_civil_war
		subtract_stability_1 = yes
		add_manpower = -2
		random_owned_province = {
			limit = {
				culture = vietnamese
				NOT = { has_province_modifier = nationalists_organizing }
			}
			add_province_modifier = {
				name = "nationalists_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
		random_owned_province = {
			limit = {
				culture = vietnamese
				NOT = { has_province_modifier = nationalists_organizing }
			}
			add_province_modifier = {
				name = "nationalists_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
	}
	option = {
		name = "flavor_psmod.EVTOPTB16" # Let's keep the alliance with them
		ai_chance = { factor = 20 }
		set_global_flag = nguyen_trinh_civil_war
		add_manpower = 2		
		add_years_of_income = -0.25
		add_dip_power = -100
	}
}

# Civil Wars between the Nguyen and Trinh Lords (Trinh Version)

country_event = {
	id = flavor_psmod.17
	
	title = flavor_psmod.EVTNAME17
	desc = flavor_psmod.EVTDESC17

	picture = BATTLE_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		NOT = { has_global_flag = nguyen_trinh_civil_war }
		OR = {
			tag = DAI
			tag = ANN
		}
		TOK = { exists = no }
		is_year = 1503
		NOT = { is_year = 1623 }
	}
	
	mean_time_to_happen = {
		months = 1200
		modifier = {
			factor = 0.5
			NOT = { stability = 1 }
		}
	}
	
	option = {
		name = "flavor_psmod.EVTOPTA17" # The Trinh family is my sworn enemy!
		ai_chance = { factor = 80 }
		set_global_flag = nguyen_trinh_civil_war
		subtract_stability_1 = yes
		add_manpower = -2
		random_owned_province = {
			limit = {
				culture = vietnamese
				NOT = { has_province_modifier = nationalists_organizing }
			}
			add_province_modifier = {
				name = "nationalists_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
		random_owned_province = {
			limit = {
				culture = vietnamese
				NOT = { has_province_modifier = nationalists_organizing }
			}
			add_province_modifier = {
				name = "nationalists_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
	}
	
	option = {
		name = "flavor_psmod.EVTOPTB17" # Let's keep the alliance with them
		ai_chance = { factor = 20 }
		set_global_flag = nguyen_trinh_civil_war
		add_manpower = 2		
		add_years_of_income = -0.25
		add_dip_power = -100
	}
}