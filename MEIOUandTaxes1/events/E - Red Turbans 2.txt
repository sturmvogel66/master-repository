namespace = tianxia

### Event List ###
#1. Red Turban Incursions of Goryeo
#2. Ispah Rebellion - Postponed
#3. Chen Youliang assassinates Xu Shouhui
#4. Yidu Falls
#5. Zhu Yuanzhang invites Han Liner
#6. Fall of Dadu
#7. Chinese Unification

#### Red Turban Incursions of Goryeo ####
#Red turbans lost control of Liaodong
country_event = {
	id = "tianxia.1"
	
	title = "tianxia.1.t"
	desc = "tianxia.1.d"
	
	picture = COUNTRY_COLLAPSE_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		OR = {
			tag = QII
			any_country = {
				has_country_flag = fall_of_yidu
			}
		}
		NOT = {
			has_country_flag = red_turban_invasion 
			controls = 2249
		}
	}
	
	mean_time_to_happen = {
		days = 14
	}
	
	option = {
		name = "tianxia.1a"
		KOR = { set_country_flag = red_turban_invasion }
		set_country_flag = red_turban_invasion
		add_treasury = -50
		if = {
			limit = {
				QII = { owns = 2249}
			}
			QII = { 2249 = { cede_province = YUA } }
		}
		if = {
			limit = {
				has_country_flag = fall_of_yidu
				owns = 2249
			}
			2249 = { cede_province = YUA }
		}
		ai_chance = { factor = 95 }
	}
	option = {
		name = "tianxia.1b"
		set_country_flag = red_turban_invasion
		if = {
			limit = {
				QII = { owns = 2249}
			}
			QII = { 2249 = { cede_province = YUA } }
		}
		if = {
			limit = {
				has_country_flag = fall_of_yidu
				owns = 2249
			}
			2249 = { cede_province = YUA }
		}	
		ai_chance = { factor = 5 }
	}
}

#Liaodong Red Turban remnants invade Goryeo
country_event = {
	id = "tianxia.2"
	
	title = "tianxia.2.t"
	desc = "tianxia.2.d"
	
	picture = HORDE_ON_HORSEBACK_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = KOR
		has_country_flag = red_turban_invasion
		NOT = { has_country_flag = red_turban_incursion }
	}
	
	mean_time_to_happen = {
		days = 14
	}
	
	option = {
		name = "tianxia.2a"
		set_country_flag = red_turban_incursion
		if = {
			limit = {
				QII = { NOT = { has_country_flag = fall_of_yidu } }
			}
			730 = {
				spawn_rebels = {
					type = anti_tax_rebels
					size = 2
					friend = QII
					leader = "Duo Guan"
				}
			}
		}
		if = {
			limit = {
				SNG = { has_country_flag = fall_of_yidu }
			}
			730 = {
				spawn_rebels = {
					type = anti_tax_rebels
					size = 2
					friend = SNG
					leader = "Duo Guan"
				}
			}
		}
		if = {
			limit = {
				MNG = { has_country_flag = fall_of_yidu }
			}
			730 = {
				spawn_rebels = {
					type = anti_tax_rebels
					size = 2
					friend = MNG
					leader = "Duo Guan"
				}
			}
		}
	}
}

#### Ispah Rebellion ####
#Postponed
#Ispah Rebellion
#country_event = {
#	id = "tianxia.3"
#	
#	title = "tianxia.3.t"
#	desc = "tianxia.3.d"
#	
#	picture = ANGRY_MOB_eventPicture
#	
#	fire_only_once = yes
#	
#	trigger = {
#		tag = CEN
#		NOT = { has_country_flag = ispah_rebellion }
#	}
#	
#	mean_time_to_happen = {
#		months = 12
#	}
#	
#	option = {
#		name = "tianxia.3a"
#		set_country_flag = ispah_rebellion
#		1053 = {
#			change_religion = shiite
#			spawn_rebels = {
#				type = shiite
#				size = 2
#				friend = QII
#				leader = "Saif al-din"
#			}
#			spawn_rebels = {
#				type = shiite
#				size = 2
#				friend = QII
#				leader = "Kamal al-din"
#			}
#		}
#	}
#}

#### Chen Youliang assassinates Xu Shouhui ####
## Assassination attempt - Han
country_event = {
	id = "tianxia.4"
	
	title = "tianxia.4.t"
	desc = "tianxia.4.d"
	
	picture = ANGRY_MOB_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = DAA
		is_year = 1360
		TIA = { ai = yes }
		NOT = {
			has_country_flag = xu_assassination
			is_year = 1361
		}
	}
	
	mean_time_to_happen = {
		months = 3
	}
	
	option = {
		name = "tianxia.4a"
		TIA = { set_country_flag = xu_assassination }
		set_country_flag = xu_assassination		
		ai_chance = { factor = 95 }
	}
	option = {
		name = "tianxia.4b"
		set_country_flag = xu_assassination
		ai_chance = { factor = 5 }
	}
}

## Tianwanese reception
country_event = {
	id = "tianxia.5"
	
	title = "tianxia.5.t"
	desc = "tianxia.5.d"
	
	picture = ANGRY_MOB_eventPicture
	fire_only_once = yes
	
	trigger = {
		tag = TIA
		has_country_flag = xu_assassination
		NOT = {
			has_country_flag = assassin_attempt
		}
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = "tianxia.5a"
		set_global_flag = assassin_success
		set_country_flag = assassin_attempt
		ai_chance = { factor = 95 }
	}
	option = {
		name = "tianxia.5b"
		set_global_flag = assassin_fail
		set_country_flag = assassin_attempt
		ai_chance = { factor = 5 }
	}
}

## Assassination Successes
country_event = {
	id = "tianxia.6"
	
	title = "tianxia.6.t"
	desc = "tianxia.6.d"
	
	picture = ANGRY_MOB_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = DAA
		has_global_flag = assassin_success
		has_country_flag = xu_assassination
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = "tianxia.6a"
		inherit = TIA
		change_government = chinese_monarchy_4
		add_prestige = 10
		clr_global_flag = assassin_success	
	}
}

## Assassination Fails
country_event = {
	id = "tianxia.7"
	
	title = "tianxia.7.t"
	desc = "tianxia.7.d"
	
	picture = ANGRY_MOB_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = DAA
		has_country_flag = xu_assassination
		has_global_flag = assassin_fail
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = "tianxia.7a"
		add_opinion = {
			who = TIA
			modifier = claimed_throne_on_us
		}
		add_prestige = -10
		clr_global_flag = assassin_fail
	}
}
#### Fall of Yidu ####
#Yidu Red Turbans destroyed
country_event = {
	id = "tianxia.8"
	
	title = "tianxia.8.t"
	desc = "tianxia.8.d"
	
	picture = COUNTRY_COLLAPSE_eventPicture
	major = yes
	fire_only_once = yes
	
	trigger = {
		tag = QII
		NOT = { has_country_flag = fall_of_yidu }
		NOT = {
			controls = 699
		}
	}
	
	mean_time_to_happen = {
		days = 14
	}
	
	option = {
		name = "tianxia.8a"
		kill_ruler = yes
		define_ruler = {
			name = "Jizu"
			dynasty = "Xu"
			dip = 3
			adm = 4
			mil = 6
			age = 38
			claim = 50
		}
		define_ruler_to_general = {
			fire = 4
			shock = 4
			manuever = 3
			siege = 1
		}
		set_country_flag = fall_of_yidu
		ai_chance = { factor = 5 }
	}
	option = {
		name = "tianxia.8b"
		if = {
			limit = {
				MNG = { has_country_flag = han_liner_invited }
			}
			MNG = { 
				inherit = QII
				set_country_flag = fall_of_yidu
			}
		}
		if = {
			limit = {
				exists = SNG
			}
			SNG = { 
				inherit = QII
				set_country_flag = fall_of_yidu
			}
		}
		ai_chance = { factor = 95 }
	}
}
#### Invitation of Han Liner ####
#Invite Han Liner?
country_event = {
	id = "tianxia.9"
	
	title = "tianxia.9.t"
	desc = "tianxia.9.d"
	
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = MNG
		is_year = 1364
		NOT = {
			has_country_flag = han_liner_invited
			is_year = 1368
		}
	}
	
	mean_time_to_happen = {
		months = 6
	}
	
	option = {
		name = "tianxia.9a"
		SNG = { set_country_flag = han_liner_invited }
		set_country_flag = han_liner_invited
		ai_chance = { factor = 95 }
	}
	option = {
		name = "tianxia.9b"
		set_country_flag = han_liner_invited
		ai_chance = { factor = 5 }
	}
}

#Invited!
country_event = {
	id = "tianxia.10"
	
	title = "tianxia.10.t"
	desc = "tianxia.10.d"
	
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = SNG
		has_country_flag = han_liner_invited
		NOT = {
			has_country_flag = han_liner_going
		}
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = "tianxia.10a"
		MNG = { set_country_flag = han_liner_going }
		set_country_flag = han_liner_going
		ai_chance = { factor = 95 }
	}
	option = {
		name = "tianxia.10b"
		set_country_flag = han_liner_going
		ai_chance = { factor = 5 }
	}
}
#Kill him!
country_event = {
	id = "tianxia.11"
	
	title = "tianxia.11.t"
	desc = "tianxia.11.d"
	
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = MNG
		has_country_flag = han_liner_going
		NOT = {
			has_country_flag = han_liner_sink
		}
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = "tianxia.11a"
		set_country_flag = han_liner_dead
		set_country_flag = han_liner_sink
		ai_chance = { factor = 95 }
	}
	option = {
		name = "tianxia.11b"
		add_prestige = 10
		add_legitimacy = 10
		set_country_flag = han_liner_sink
		ai_chance = { factor = 5 }
	}
}

#Ming inherits Song
country_event = {
	id = "tianxia.12"
	
	title = "tianxia.12.t"
	desc = "tianxia.12.d"
	
	picture = DIPLOMACY_eventPicture
	major = yes
	fire_only_once = yes
	
	trigger = {
		tag = MNG
		has_country_flag = han_liner_dead
		has_country_flag = han_liner_sink
		NOT = {
			has_country_flag = song_inherited
		}
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = "tianxia.12a"
		set_country_flag = song_inherited
		inherit = SNG
	}
}

#Yidu Reception
country_event = {
	id = "tianxia.13"
	
	title = "tianxia.13.t"
	desc = "tianxia.13.d"
	
	picture = DIPLOMACY_eventPicture

	fire_only_once = yes
	
	trigger = {
		tag = QII
		MNG = { has_country_flag = song_inherited }
		NOT = {
			has_country_flag = yidu_choice
		}
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = "tianxia.13a"
		MNG = { set_country_flag = yidu_choice }
		set_country_flag = yidu_choice
		ai_chance = { factor = 95 }
	}
	option = {
		name = "tianxia.13b"
		set_country_flag = yidu_choice
		ai_chance = { factor = 5 }
	}
}

#Ming Reception of Surrender of Yidu
country_event = {
	id = "tianxia.14"
	
	title = "tianxia.14.t"
	desc = "tianxia.14.d"
	
	picture = DIPLOMACY_eventPicture

	fire_only_once = yes
	
	trigger = {
		tag = MNG
		has_country_flag = yidu_choice
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = "tianxia.14a"
		inherit = QII
		clr_country_flag = yidu_choice
		ai_chance = { factor = 90 }
	}
	option = {
		name = "tianxia.14b"
		vassalize = QII
		clr_country_flag = yidu_choice
		ai_chance = { factor = 5 }
	}
	option = {
		name = "tianxia.14c"
		clr_country_flag = yidu_choice
		ai_chance = { factor = 5 }
	}
}
###Fall of Dadu###
#country_event = {
#	id = "tianxia.18"
#	
#	title = "tianxia.18.t"
#	desc = "tianxia.18.d"
#	
##
#	fire_only_once = yes
#	
#	trigger = {
#		tag = YUA
#		capital = 708
#		NOT = { controls = 708 }
#		any_country = {
#			controls = 708
#			war_with = ROOT
#			culture_group = chinese_group
#		}
#	}
#	
#	mean_time_to_happen = {
#		days = 30
#	}
#	
#	option = {
#		name = "tianxia.18a"
#		set_capital = 2265
#		every_country = {
#			limit = {
#				controls = 708
#			}
#			add_core = 708
#			708 = {
#				cede_province = PREV
#				change_religion = owner
#				change_province_name = "Shuntian"
#				rename_capital = "Beiping"
#			}
#		}
#		ai_chance = { factor = 55 }
#	}
#	option = {
#		name = "tianxia.18b"
#		set_capital = 3273
#		every_country = {
#			limit = {
#				controls = 708
#			}
#			add_core = 708
#			708 = {
#				cede_province = PREV
#				change_religion = owner
#				change_province_name = "Shuntian"
#				rename_capital = "Beiping"
#			}
#		}
#		ai_chance = { factor = 45 }
#	}
#}

###The Unified China###
country_event = {
	id = "tianxia.15"
	
	title = "tianxia.15.t"
	desc = "tianxia.15.d"
	
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes
	
	option = {
		name = "tianxia.15a"
		north_china_region = { remove_core = FROM }
		south_china_region = { remove_core = FROM }
		xinan_region = { remove_core = FROM }
		every_owned_province = {
			add_core = FROM
		}
		inherit = FROM
		ai_chance = { factor = 100 }
	}
	option = {
		name = "tianxia.15b"
		vassalize = FROM
		FROM = { set_country_flag = request_defect }
		ai_chance = { factor = 0 }
	}
	option = {
		name = "tianxia.15c"
		FROM = {
			country_event = { id = "tianxia.16" days = 7 }
		}
		ai_chance = { factor = 0 }
	}
}

#Defection Refused
country_event = {
	id = "tianxia.16"
	
	title = "tianxia.16.t"
	desc = "tianxia.16.d"
	
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes
	
	option = {
		name = "tianxia.16a"
		add_ruler_modifier = { name = "unified_china" }
		clr_country_flag = request_defect
	}
}

