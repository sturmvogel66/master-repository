
# A Traitor in the Parliament
country_event = {
	id = republics.24
	title = "EVTNAME9560"
	desc = "EVTDESC9560"
	picture = LIBERUM_VETO_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = bad_relations_timer 
		}
		OR = { 
			government = noble_republic
			government = oligarchic_republic
			government = merchant_oligarchic_republic
		}
		any_neighbor_country = {
			NOT = { has_opinion = { who = ROOT value = 0 } }
			OR = {
				advisor = spymaster
				advisor = inquisitor
				advisor = diplomat
				advisor = acardinal
			}
		}
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 1.2
			spymaster = 3
		}
		modifier = {
			factor = 1.2
			advisor = spymaster
		}
		modifier = {
			factor = 1.2
			advisor = inquisitor
		}
	}

	option = {
		name = "EVTOPTA9560" # Of course it was one of them
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = bad_relations_timer
				duration = 1825
				hidden = yes
			}
		}
		random_country = {
			limit = {
				is_neighbor_of = ROOT
				NOT = { has_opinion = { who = ROOT value = 0 } }
			}
			add_opinion = { who = ROOT modifier = opinion_traitor }
		}
	}
	option = {
		name = "EVTOPTB9560" # It's better to play it safe until we know more
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = bad_relations_timer
				duration = 1825
				hidden = yes
			}
		}
		add_prestige = -10
	}
}

# Political Chaos
country_event = {
	id = republics.25
	title = "EVTNAME9561"
	desc = "EVTDESC9561"
	picture = LIBERUM_VETO_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = political_chaos_timer 
		}
		OR = { 
			government = noble_republic
			government = oligarchic_republic
			government = merchant_oligarchic_republic
		}
		NOT = { adm = 3 }
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			NOT = { mil = 2 }
		}
		modifier = {
			factor = 1.1
			mil = 4
		}
		modifier = {
			factor = 1.1
			mil = 5
		}
		modifier = {
			factor = 1.1
			mil = 6
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
	}

	option = {
		name = "EVTOPTA9561" # That's just the nature of politics
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = political_chaos_timer
				duration = 1825
				hidden = yes
			}
		}
		random_owned_province = {
			limit = { is_capital = yes }
			add_unrest = 4
		}
	}
	option = {
		name = "EVTOPTB9561" # Some restructuring might be necessary
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = political_chaos_timer
				duration = 1825
				hidden = yes
			}
		}
		add_years_of_income = -0.20
		random = {
			chance = 40
			add_stability_1 = yes
		}
	}
}

# Influenced Decisions
country_event = {
	id = republics.26
	title = "EVTNAME9562"
	desc = "EVTDESC9562"
	picture = DEBATE_REPUBLICAN_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = influenced_decisions_timer 
		}
		government = noble_republic
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
		modifier = {
			factor = 0.9
			NOT = { adm = 1 }
		}
		modifier = {
			factor = 1.1
			adm = 4
		}
		modifier = {
			factor = 1.1
			adm = 5
		}
		modifier = {
			factor = 1.1
			adm = 6
		}
	}

	option = {
		name = "EVTOPTA9562"		# It has to stop
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = influenced_decisions_timer
				duration = 1825
				hidden = yes
			}
		}
		random = {
			chance = 50
			subtract_stability_1 = yes
		}
	}
	option = {
		name = "EVTOPTB9562"		# That can't be true
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = influenced_decisions_timer
				duration = 1825
				hidden = yes
			}
		}
		add_prestige = -5
	}
}

# Financial Disagreements
country_event = {
	id = republics.27
	title = "EVTNAME9563"
	desc = "EVTDESC9563"
	picture = ECONOMY_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = financial_disagreements_timer 
		}
		OR = {
			government = noble_republic
			government = oligarchic_republic
			government = merchant_oligarchic_republic
			# government = signora_monarchy
		}
		num_of_loans = 1
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 1.2
			advisor = treasurer
		}
		modifier = {
			factor = 1.2
			advisor = master_of_mint
		}
		modifier = {
			factor = 1.2
			treasurer = 3
		}
		modifier = {
			factor = 1.1
			adm = 4
		}
		modifier = {
			factor = 1.1
			adm = 5
		}
		modifier = {
			factor = 1.1
			adm = 6
		}
		modifier = {
			factor = 0.8
			num_of_loans = 2
		}
		modifier = {
			factor = 0.9
			inflation = 4
		}
		modifier = {
			factor = 0.9
			inflation = 6
		}
		modifier = {
			factor = 0.9
			inflation = 8
		}
		modifier = {
			factor = 0.9
			inflation = 10
		}
		modifier = {
			factor = 0.9
			NOT = { adm = 1 }
		}
	}

	option = {
		name = "EVTOPTA9563" # Mint more money
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = financial_disagreements_timer
				duration = 1825
				hidden = yes
			}
		}
		add_years_of_income = 0.25
		add_inflation = 4
	}
	option = {
		name = "EVTOPTB9563" # Raise taxes
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = financial_disagreements_timer
				duration = 1825
				hidden = yes
			}
		}
		add_years_of_income = 0.25
		subtract_stability_1 = yes
	}
}

# Military Spending
country_event = {
	id = republics.28
	title = "EVTNAME9564"
	desc = "EVTDESC9564"
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { has_country_modifier = trade_and_production_cutbacks }
		NOT = { has_country_modifier = military_cutbacks }
		OR = {
			government = noble_republic
			government = oligarchic_republic
			government = merchant_oligarchic_republic
		}
		is_at_war = yes
		OR = {
			NOT = { trade_efficiency = 0.4 }
			NOT = { production_efficiency = 0.4 }
		}
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			manpower_percentage = 0.7
		}
		modifier = {
			factor = 0.9
			manpower_percentage = 0.8
		}
		modifier = {
			factor = 0.9
			manpower_percentage = 0.9
		}
		modifier = {
			factor = 0.9
			NOT = { production_efficiency = 0.3 }
		}
		modifier = {
			factor = 0.9
			NOT = { production_efficiency = 0.2 }
		}
		modifier = {
			factor = 0.9
			NOT = { trade_efficiency = 0.3 }
		}
		modifier = {
			factor = 0.9
			NOT = { trade_efficiency = 0.2 }
		}
		modifier = {
			factor = 0.9
			is_monarch_leader = yes
		}
		modifier = {
			factor = 0.9
			mil = 6
		}
		modifier = {
			factor = 0.9
			mil = 5
		}
		modifier = {
			factor = 1.2
			NOT = { mil = 1 }
		}
		modifier = {
			factor = 0.9
			advisor = army_reformer
		}
		modifier = {
			factor = 0.9
			advisor = army_organiser
		}
		modifier = {
			factor = 0.9
			advisor = fortification_expert
		}
		modifier = {
			factor = 0.9
			advisor = naval_reformer
		}
		modifier = {
			factor = 1.2
			advisor = natural_scientist
		}
		modifier = {
			factor = 1.2
			advisor = trader
		}
	}

	option = {
		name = "EVTOPTA9564" # Well, trade and production aren't as important
		ai_chance = { factor = 55 }
		add_country_modifier = {
			name = "trade_and_production_cutbacks"
			duration = 730
		}
	}
	option = {
		name = "EVTOPTB9564" # Cut back on military expenses
		ai_chance = { factor = 45 }
		add_country_modifier = {
			name = "military_cutbacks"
			duration = 730
		}
	}
}

# A Strong Army or Navy?
country_event = {
	id = republics.29
	title = "EVTNAME9565"
	desc = "EVTDESC9565"
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { has_country_modifier = naval_modifier }
		NOT = { has_country_modifier = land_modifier }
		government = constitutional_republic
		naval_ideas = 1
		leadership_ideas = 1
		num_of_ports = 1
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			num_of_ports = 2
		}
		modifier = {
			factor = 0.9
			mil = 4
		}
		modifier = {
			factor = 0.9
			mil = 5
		}
		modifier = {
			factor = 0.9
			mil = 6
		}
		modifier = {
			factor = 0.9
			is_monarch_leader = yes
		}
		modifier = {
			factor = 0.9
			advisor = naval_reformer
		}
		modifier = {
			factor = 0.9
			advisor = navigator
		}
		modifier = {
			factor = 1.1
			advisor = army_reformer
		}
		modifier = {
			factor = 1.1
			advisor = grand_captain
		}
		modifier = {
			factor = 1.1
			advisor = army_organiser
		}
	}

	option = {
		name = "EVTOPTA9565" # A large navy is the only option
		ai_chance = { factor = 55 }
		set_country_flag = strong_army_or_navy
		add_country_modifier = {
			name = naval_modifier
			duration = 3650
		}
	}
	option = {
		name = "EVTOPTB9565" # A large army would be more beneficial
		ai_chance = { factor = 45 }
		set_country_flag = strong_army_or_navy
		add_country_modifier = {
			name = land_modifier
			duration = 3650
		}
		random = {
			chance = 20
			subtract_stability_1 = yes
		}
	}
}

# Military Leniency
country_event = {
	id = republics.30
	title = "EVTNAME9566"
	desc = "EVTDESC9566"
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = foreign_control_timer
		}
		government = constitutional_republic
		is_at_war = no
		any_core_province = {
			is_core = ROOT
			any_neighbor_province = {
				NOT = { owned_by = ROOT }
				is_core = ROOT
			}
		}
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = "EVTOPTA9566"		# They fail to see the bigger picture
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = foreign_control_timer
				duration = 1825
				hidden = yes
			}
		}
		add_prestige = -5	
		add_republican_tradition = -2
	}
	option = {
		name = "EVTOPTB9566"		# It's not of any interest anymore
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = foreign_control_timer
				duration = 1825
				hidden = yes
			}
		}
		subtract_stability_1 = yes
		add_republican_tradition = 1
	}
}

# Too many liberties
country_event = {
	id = republics.31
	title = "EVTNAME9567"
	desc = "EVTDESC9567"
	picture = DEBATE_REPUBLICAN_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = too_many_liberties_timer
		}
		government = constitutional_republic
		has_regency = no
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			is_monarch_leader = yes
		}
		modifier = {
			factor = 0.9
			NOT = { dip = 1 }
		}
		modifier = {
			factor = 1.1
			dip = 4
		}
		modifier = {
			factor = 1.1
			dip = 5
		}
		modifier = {
			factor = 1.1
			dip = 6
		}
	}

	option = {
		name = "EVTOPTA9567" # They are exaggerating
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = too_many_liberties_timer
				duration = 1825
				hidden = yes
			}
		}
		add_prestige = -2
		add_republican_tradition = 1
	}
	option = {
		name = "EVTOPTB9567" # Take appropriate measurements
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = too_many_liberties_timer
				duration = 1825
				hidden = yes
			}
		}
		random_owned_province = {
			limit = { is_capital = yes }
			add_unrest = 2
		}
		add_republican_tradition = -1
	}
}

# Economic Decisions
country_event = {
	id = republics.32
	title = "EVTNAME9568"
	desc = "EVTDESC9568"
	picture = ECONOMY_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = short_term_boost
		}
		NOT = { 
			has_country_modifier = long_term_boost
		}
		government = constitutional_republic
		OR = {
			advisor = treasurer
			advisor = master_of_mint
			advisor = trader
		}
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.8
			is_bankrupt = yes
		}
		modifier = {
			factor = 0.9
			num_of_loans = 1
		}
		modifier = {
			factor = 0.9
			num_of_loans = 2
		}
		modifier = {
			factor = 0.9
			num_of_loans = 3
		}
		modifier = {
			factor = 0.9
			treasurer = 5
		}
		modifier = {
			factor = 0.9
			treasurer = 6
		}
		modifier = {
			factor = 0.9
			statesman = 3
		}
	}

	option = {
		name = "EVTOPTA9568" # A short term bost is probably the best option
		ai_chance = { factor = 55 }
		add_country_modifier = {
			name = "short_term_boost"
			duration = 365
		}	
	}
	option = {
		name = "EVTOPTB9568" # Consider the long term consequences
		ai_chance = { factor = 45 }
		add_country_modifier = {
			name = "long_term_boost"
			duration = 730
		}
		add_republican_tradition = 1
	}
}

# Military Expenses
country_event = {
	id = republics.33
	title = "EVTNAME9569"
	desc = "EVTDESC9569"
	picture = ECONOMY_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = military_maintenance_timer
		}
		NOT = { 
			has_country_modifier = decreased_military_funding
		}
		government = constitutional_republic
		army_size_percentage = 70
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			advisor = treasurer
		}
		modifier = {
			factor = 0.9
			advisor = master_of_mint
		}
		modifier = {
			factor = 1.1
			advisor = naval_reformer
		}
		modifier = {
			factor = 1.1
			advisor = army_reformer
		}
		modifier = {
			factor = 1.1
			advisor = army_organiser
		}
		modifier = {
			factor = 1.2
			is_monarch_leader = yes
		}
		modifier = {
			factor = 1.1
			mil = 4
		}
		modifier = {
			factor = 1.1
			mil = 5
		}
		modifier = {
			factor = 1.1
			mil = 6
		}
		modifier = {
			factor = 0.9
			num_of_loans = 1
		}
		modifier = {
			factor = 0.9
			num_of_loans = 2
		}
	}

	option = {
		name = "EVTOPTA9569" # Convince them that we can afford it
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = military_maintenance_timer
				duration = 730
				hidden = yes
			}
		}
		add_years_of_income = -0.10
	}
	option = {
		name = "EVTOPTB9569" # Agree to make some cutbacks
		ai_chance = { factor = 45 }
		set_country_flag = military_maintenance
		add_country_modifier = {
			name = "decreased_military_funding"
			duration = 730
		}
		add_republican_tradition = 1
	}
}

# Sympathisers of the Old Government
country_event = {
	id = republics.34
	title = "EVTNAME9570"
	desc = "EVTDESC9570"
	picture = DEBATE_REPUBLICAN_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = sympathisers_of_old_government_timer
		}
		government = revolutionary_republic
		NOT = { mil = 2 }
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			NOT = { adm = 1 }
		}
		modifier = {
			factor = 1.1
			adm = 4
		}
		modifier = {
			factor = 1.1
			adm = 5
		}
		modifier = {
			factor = 1.1
			adm = 6
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
		modifier = {
			factor = 1.1
			is_monarch_leader = yes
		}
		modifier = {
			factor = 1.1
			advisor = statesman
		}
	}

	option = {
		name = "EVTOPTA9570" # That won't be necessary
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = sympathisers_of_old_government_timer
				duration = 1825
				hidden = yes
			}
		}
		random_owned_province = {
			limit = { is_capital = yes }
			add_unrest = 5
		}
		add_republican_tradition = 1
	}
	option = {
		name = "EVTOPTB9570"		# Get to it
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = sympathisers_of_old_government_timer
				duration = 1825
				hidden = yes
			}
		}
		add_years_of_income = -0.20
		add_republican_tradition = -1
	}
}

# A New Constitution
country_event = {
	id = republics.35
	title = "EVTNAME9571"
	desc = "EVTDESC9571"
	picture = ADVISOR_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = a_new_constitution_timer
		}
		government = revolutionary_republic
		NOT = { economic_ideas = 4 }
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			NOT = { economic_ideas = 2 }
		}
		modifier = {
			factor = 0.9
			NOT = { economic_ideas = 1 }
		}
		modifier = {
			factor = 0.9
			NOT = { adm = 1 }
		}
		modifier = {
			factor = 1.1
			adm = 4
		}
		modifier = {
			factor = 1.1
			adm = 5
		}
		modifier = {
			factor = 1.1
			adm = 6
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
	}

	option = {
		name = "EVTOPTA9571"		# It will be solved soon enough
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = a_new_constitution_timer
				duration = 1825
				hidden = yes
			}
		}
		random = {
			chance = 40
			subtract_stability_1 = yes
		}
		add_republican_tradition = 2
	}
	option = {
		name = "EVTOPTB9571"		# Request some help
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = a_new_constitution_timer
				duration = 1825
				hidden = yes
			}
		}
		add_years_of_income = -0.3
		define_advisor = {
			type = statesman
			skill = 3
		}
		add_republican_tradition = 1
	}
}

# Areas of Research
country_event = {
	id = republics.36
	title = "EVTNAME9572"
	desc = "EVTDESC9572"
	picture = ECONOMY_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = even_spread_of_investments 
		}
		NOT = { 
			has_country_modifier = areas_of_research_timer 
		}
		government = revolutionary_republic
		OR = {
			advisor = naval_reformer
			advisor = army_reformer
			advisor = army_organiser
			advisor = recruitmaster
			advisor = trader
			advisor = natural_scientist
		}		
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
		modifier = {
			factor = 0.9
			NOT = { adm = 1 }
		}
		modifier = {
			factor = 1.1
			adm = 4
		}
		modifier = {
			factor = 1.1
			adm = 5
		}
		modifier = {
			factor = 1.1
			adm = 6
		}
		modifier = {
			factor = 1.1
			statesman = 3
		}
	}

	option = {
		name = "EVTOPTA9572" # An even spread should do the trick
		ai_chance = { factor = 55 }
		add_country_modifier = {
			name = "even_spread_of_investments"
			duration = 730
		}
		add_republican_tradition = 1
	}
	option = {
		name = "EVTOPTB9572" # There's no need for any extra investments
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = areas_of_research_timer
				duration = 730
				hidden = yes
			}
		}
		add_inflation = 3
		add_years_of_income = 0.25
	}
}

# Lack of Support
country_event = {
	id = republics.37
	title = "EVTNAME9573"
	desc = "EVTDESC9573"
	picture = LIBERUM_VETO_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_modifier = premature_election_timer
		}
		NOT = {
			has_country_modifier = financial_arguments
		}
		government = revolutionary_republic
		has_regency = no
		NOT = { dip = 2 }
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			NOT = { dip = 1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
		modifier = {
			factor = 0.9
			num_of_loans = 1
		}
		modifier = {
			factor = 0.9
			num_of_loans = 2
		}
		modifier = {
			factor = 0.9
			inflation = 4
		}
		modifier = {
			factor = 0.9
			inflation = 8
		}
		modifier = {
			factor = 0.9
			inflation = 12
		}
	}

	option = {
		name = "EVTOPTA9573"		# Ignore them
		ai_chance = { factor = 55 }
		add_country_modifier = {
			name = "financial_arguments"
			duration = 730
		}
		add_republican_tradition = -1
	}
	option = {
		name = "EVTOPTB9573"		# Hear them out once more
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = premature_election_timer
				duration = 730
				hidden = yes
			}
		}
		add_years_of_income = -0.20
		add_republican_tradition = 1
	}
}

# Anxious Parliament
country_event = {
	id = republics.38
	title = "EVTNAME9574"
	desc = "EVTDESC9574"
	picture = LIBERUM_VETO_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = anxious_parliament_timer
		}
		NOT = { 
			has_country_modifier = emergency_plan 
		}
		government = revolutionary_republic
		advisor = spymaster
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.8
			spymaster = 3
		}
		modifier = {
			factor = 0.8
			advisor = inquisitor
		}
		modifier = {
			factor = 0.8
			inquisitor = 3
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
		modifier = {
			factor = 0.8
			NOT = { mil = 1 }
		}
		modifier = {
			factor = 1.1
			mil = 4
		}
		modifier = {
			factor = 1.1
			mil = 5
		}
		modifier = {
			factor = 1.1
			mil = 6
		}
	}

	option = {
		name = "EVTOPTA9574" # It's nothing to worry about
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = anxious_parliament_timer
				duration = 730
				hidden = yes
			}
		}
		random_core_province = {
			limit = {
				NOT = { has_province_modifier = turmoil_organizing }
			}
			add_province_modifier = {
				name = "turmoil_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
	}
	option = {
		name = "EVTOPTB9574" # It's better to be safe than sorry
		ai_chance = { factor = 45 }
		add_years_of_income = -0.20
		add_country_modifier = {
			name = "emergency_plan"
			duration = 730
		}
	}
}

# Troublesome Parliament
country_event = {
	id = republics.39
	title = "EVTNAME9575"
	desc = "EVTDESC9575"
	picture = LIBERUM_VETO_eventPicture
	
	is_triggered_only = yes

	trigger = {
		check_variable = { which = centralization_decentralization value = 0 }
		NOT = {
			has_country_modifier = bothersome_parliament
		}
		government = republican_dictatorship
		has_regency = no
		OR = {
			NOT = { mil = 2 }
			NOT = { adm = 2 }
		}
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			NOT = { mil = 1 }
		}
		modifier = {
			factor = 0.9
			NOT = { adm = 1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
		modifier = {
			factor = 1.1
			is_monarch_leader = yes
		}
	}

	option = {
		name = "EVTOPTA9575"		# Make it a bit more clear to them
		ai_chance = { factor = 55 }
		#set_country_flag = troublesome_parliament
		increase_centralisation = yes
		random_owned_province = {
			limit = { is_capital = yes }
			add_unrest = 3
		}
		add_republican_tradition = -1
	}
	option = {
		name = "EVTOPTB9575"		# Let them think they have a say in things
		ai_chance = { factor = 45 }
		#set_country_flag = troublesome_parliament		
		add_country_modifier = {
			name = "bothersome_parliament"
			duration = 730
		}
		add_republican_tradition = 2		
	}
}

# Overspending
country_event = {
	id = republics.40
	title = "EVTNAME9576"
	desc = "EVTDESC9576"
	picture = ECONOMY_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = over_budget_timer
		}
		government = republican_dictatorship
		stability = -2
		advisor = treasurer
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 1.1
			advisor = statesman
		}
		modifier = {
			factor = 0.9
			advisor = master_of_mint
		}
		modifier = {
			factor = 0.9
			num_of_loans = 1
		}
		modifier = {
			factor = 0.9
			num_of_loans = 2
		}
		modifier = {
			factor = 0.9
			inflation = 5
		}
		modifier = {
			factor = 0.9
			inflation = 10
		}
		modifier = {
			factor = 0.9
			NOT = { adm = 1 }
		}
		modifier = {
			factor = 0.9
			NOT = { adm = 2 }
		}
		modifier = {
			factor = 1.1
			adm = 4
		}
		modifier = {
			factor = 1.1
			adm = 5
		}
		modifier = {
			factor = 1.1
			adm = 6
		}
	}

	option = {
		name = "EVTOPTA9576"		# Try to convince them of otherwise
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = over_budget_timer
				duration = 1825
				hidden = yes
			}
		}
		add_years_of_income = -0.10
	}
	option = {
		name = "EVTOPTB9576"		# Let them worry
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = over_budget_timer
				duration = 1825
				hidden = yes
			}
		}
		subtract_stability_1 = yes	
	}
}

#Embezzlement
country_event = {
	id = republics.41
	title = "EVTNAME9577"
	desc = "EVTDESC9577"
	picture = ECONOMY_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = embezzlement_timer 
		}
		government = republican_dictatorship
		has_advisor = yes
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.8
			is_bankrupt = yes
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			adm = 6
		}
		modifier = {
			factor = 1.1
			adm = 5
		}
		modifier = {
			factor = 1.1
			adm = 4
		}
		modifier = {
			factor = 0.9
			NOT = { adm = 1 }
		}
	}

	option = {
		name = "EVTOPTA9577" # There's probably a good explanation for this
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = embezzlement_timer
				duration = 1825
				hidden = yes
			}
		}
		add_prestige = -5
		add_republican_tradition = -1
	}	
	option = {
		name = "EVTOPTB9577" # Get rid of him
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = embezzlement_timer
				duration = 1825
				hidden = yes
			}
		}
		random = {
			chance = 50
			random_owned_province = {
				limit = { is_capital = yes }
				add_unrest = 5
			}
		}	
		add_republican_tradition = 1	
	}
}

# Planned Coup
country_event = {
	id = republics.42
	title = "EVTNAME9578"
	desc = "EVTDESC9578"
	picture = DEBATE_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		NOT = { 
			has_country_modifier = planned_coup_timer
		}
		NOT = { 
			has_country_modifier = spies_investigate
		}
		government = republican_dictatorship
		has_regency = no
		OR = {
			army_reformer = 3
			grand_captain = 3
			commandant = 3
			overextension_percentage = 0.25
			NOT = { prestige = 1 }
		}
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.2
			stability = 2
		}
		modifier = {
			factor = 1.3
			stability = 3
		}
		modifier = {
			factor = 1.1
			mil = 6
		}
		modifier = {
			factor = 1.1
			mil = 5
		}
		modifier = {
			factor = 1.1
			mil = 4
		}
		modifier = {
			factor = 0.9
			NOT = { mil = 1 }
		}
		modifier = {
			factor = 0.9
			NOT = { adm = 1 }
		}
	}

	option = {
		name = "EVTOPTA9578" # You can never be too careful
		ai_chance = { factor = 55 }
		set_country_flag = planned_coup
		add_years_of_income = -0.30
		add_country_modifier = {
			name = spies_investigate
			duration = 1825
		}
		random = {
			chance = 30
			capital_scope = {
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				add_unrest = 15
			}
		}
	}
	option = {
		name = "EVTOPTB9578" # They wouldn't dare!
		ai_chance = { factor = 45 }
		hidden_effect = {
			add_country_modifier = {
				name = planned_coup_timer
				duration = 1825
				hidden = yes
			}
		}
		random = {
			chance = 80
			capital_scope = {
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				add_unrest = 15
			}
		}		
	}
}

# Possible Traitors
country_event = {
	id = republics.43
	title = "EVTNAME9579"
	desc = "EVTDESC9579"
	picture = DEBATE_REPUBLICAN_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { 
			has_country_modifier = possible_traitors_timer
		}
		NOT = { 
			has_country_modifier = counter_espionage
		}
		government = republican_dictatorship
		any_neighbor_country = {
			mil = 3
			NOT = { alliance_with = ROOT }
			NOT = { marriage_with = ROOT }
		}		
	}

	mean_time_to_happen = {
		days = 1

		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.2
			stability = 2
		}
		modifier = {
			factor = 1.3
			stability = 3
		}
		modifier = {
			factor = 0.9
			is_at_war = yes
		}
		modifier = {
			factor = 0.9
			NOT = { mil = 1 }
		}
		modifier = {
			factor = 0.9
			NOT = { adm = 1 }
		}
		modifier = {
			factor = 1.1
			adm = 4
		}
		modifier = {
			factor = 1.1
			adm = 5
		}
		modifier = {
			factor = 1.1
			adm = 6
		}
	}

	option = {
		name = "EVTOPTA9579" # Confront them
		ai_chance = { factor = 55 }
		hidden_effect = {
			add_country_modifier = {
				name = possible_traitors_timer
				duration = 1825
				hidden = yes
			}
		}
		random_country = {
			limit = {
				is_neighbor_of = ROOT
				mil = 3
				NOT = { alliance_with = ROOT }
				NOT = { marriage_with = ROOT }
			}
			add_opinion = { who = ROOT modifier = opinion_confront_spies }
		}		
	}
	option = {
		name = "EVTOPTB9579" # Send a few spies of your own
		ai_chance = { factor = 45 }
		set_country_flag = possible_traitors
		add_country_modifier = {
			name = counter_espionage
			duration = 1825
		}
		add_prestige = -10
	}
}
