
country_event = {
	id = border_friction.1
	title = "border_friction.1t"
	desc = "border_friction.1d"
	picture = ANGRY_MOB_eventPicture
	
	trigger = {
		any_country = {
			alliance_with = ROOT
			any_owned_province = {
				is_claim = ROOT
			}
		}
	}
	
	mean_time_to_happen = {
		months = 600
		
		modifier = {
			factor = 1.2
			advisor = statesman
		}

	}
	
	option = {
		name = "border_friction.1a" 

		random_country = {
			limit = { 
				alliance_with = ROOT
				any_owned_province = {
					is_claim = ROOT
				}
			}
			add_opinion = { who = ROOT modifier = border_friction_from_event }
		}
	}
	option = {
		name = "border_friction.1b" 
		ai_chance = { factor = 0 }
		add_dip_power = -100	
	}
}

country_event = {
	id = border_friction.2
	title = "border_friction.1t"
	desc = "border_friction.2d"
	picture = ANGRY_MOB_eventPicture
	
	trigger = {
		ai = no
		is_at_war = no
		is_subject  = no
		NOT = { war_exhaustion = 4 }
		manpower_percentage = 0.95
		stability = -2
		OR = { has_regency = no has_consort_regency = yes }
		any_country = {
			any_owned_province = {
				is_claim = ROOT
			}
			NOT = { is_subject_of = ROOT  }
			NOT = { truce_with = ROOT }
			NOT = { alliance_with = ROOT }
		}	
	}
	
	mean_time_to_happen = {
		months = 600
		
		modifier = {
			factor = 1.2
			advisor = statesman
		}

	}
	
	option = {
		name = "border_friction.1a"
		subtract_stability_1 = yes	
		random_country = {
			limit = {
				any_owned_province = {
					is_claim = ROOT
				}
				NOT = { truce_with = ROOT }
				NOT = { is_subject_of = ROOT  }
				NOT = { alliance_with = ROOT }
			}
			add_opinion = { who = ROOT modifier = border_friction_from_event }
		}
	}
	option = {
		name = "border_friction.1b" 
		ai_chance = { factor = 0 }
		add_dip_power = -100	
	}
}
