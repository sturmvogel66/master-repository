########################################
# Flavor Events for Aragon
#
# written by Sara Wendel-�rtqvist
########################################

# Consulate of the Sea
country_event = {
	id = flavor_ara.1
	title = "flavor_ara.EVTNAME1"
	desc = "flavor_ara.EVTDESC1"
	picture = COURT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = ARA
		NOT = { is_year = 1500 }
		NOT = {
			has_ruler_modifier = ara_consulate_sea
		}
	}
	
	mean_time_to_happen = {
		months = 200
	}
	
	option = {
		name = "flavor_ara.EVTOPTA1"
		add_ruler_modifier = { name = ara_consulate_sea }
	}
}

# Book of the Consulate of the Sea
country_event = {
	id = flavor_ara.2
	title = "flavor_ara.EVTNAME2"
	desc = "flavor_ara.EVTDESC2"
	picture = ECONOMY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = ARA
		has_ruler_modifier = ara_consulate_sea
		NOT = { has_country_modifier = ara_book_consulate_sea }
	}
	
	mean_time_to_happen = {
		months = 200
	}
	
	option = {
		name = "flavor_ara.EVTOPTA2"
		add_years_of_income = -0.5
		add_country_modifier = { 
			name = ara_book_consulate_sea
			duration = -1
		}
	}
	option = {
		name = "flavor_ara.EVTOPTB2"
		add_prestige = -1
	}
}

# Antonio Beccadelli (poet)
country_event = {
	id = flavor_ara.3
	title = "flavor_ara.EVTNAME3"
	desc = "flavor_ara.EVTDESC3"
	picture = ADVISOR_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = ARA
		NOT = { is_year = 1471 }
		OR = {
			dip = 3
			adm = 3
		}
	}
	
	mean_time_to_happen = {
		months = 200
	}
	
	option = {
		name = "flavor_ara.EVTOPTA3"
		add_adm_power = 10
		add_prestige = 10
		define_advisor = {
			type = artist
			name = "Antonio Beccadelli"
			skill = 2
			discount = yes
		}
	}
}

# Iovianus Pontanus
country_event = {
	id = flavor_ara.4
	title = "flavor_ara.EVTNAME4"
	desc = "flavor_ara.EVTDESC4"
	picture = ECONOMY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = ARA
		NOT = { is_year = 1503 }
		OR = {
			dip = 3
			adm = 3
		}
	}
	
	mean_time_to_happen = {
		months = 200
	}
	
	option = {
		name = "flavor_ara.EVTOPTA4"
		add_prestige = 10
	}
	option = {
		name = "flavor_ara.EVTOPTB4"
		add_stability_1 = yes
	}
}

# Monastery of Santa Mar�a de Sigena
country_event = {
	id = flavor_ara.5
	title = "flavor_ara.EVTNAME5"
	desc = "flavor_ara.EVTDESC5"
	picture = ST_PETERS_CHURCH_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = ARA
		NOT = { is_year = 1520 }
		NOT = { has_country_modifier = ara_monastery_maria }
		religion = catholic
	}
	
	mean_time_to_happen = {
		months = 200
	}
	
	option = {
		name = "flavor_ara.EVTOPTA5"
		add_years_of_income = -0.5
		add_country_modifier = { 
			name = ara_monastery_maria 
			duration = 18250
		}
	}
	option = {
		name = "flavor_ara.EVTOPTB5"
		add_years_of_income = -0.5
		add_country_modifier = { 
			name = ara_monastery_painter
			duration = 18250
		}
	}
	option = {
		name = "flavor_ara.EVTOPTC5"
		add_prestige = -1
	}
}

# Sindicat Remen�a
country_event = {
	id = flavor_ara.6
	title = "flavor_ara.EVTNAME6"
	desc = "flavor_ara.EVTDESC6"
	picture = ELECTION_REPUBLICAN_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = ARA
		NOT = { is_year = 1500 }
		OR = { 
			adm = 4 
			dip = 4 
		}
		NOT = { 
			any_owned_province = {
				has_province_modifier = ara_sindicat_remenca 
			}
		}
	}
	
	mean_time_to_happen = {
		months = 200
	}
	
	immediate = {
		hidden_effect = {
			random_owned_province = {
				limit = {
					is_core = ROOT
				}
				set_province_flag = ara_sindicat_remen�a_flag
			}
			random_owned_province = {
				limit = {
					is_core = ROOT
					NOT = {  has_province_flag = ara_sindicat_remen�a_flag }
				}
				set_province_flag = ara_sindicat_remen�a_flag
			}
			random_owned_province = {
				limit = {
					is_core = ROOT
					NOT = { has_province_flag = ara_sindicat_remen�a_flag }
				}
				set_province_flag = ara_sindicat_remen�a_flag
			}
		}
	}
	
	option = {
		name = "flavor_ara.EVTOPTA6"
		add_prestige = -50
		every_owned_province = {
			limit = {
				is_core = ROOT
			}
			add_unrest = -4 
			add_province_modifier = { 
				name = ara_sindicat_remenca 
				duration = -1
			}
		}
		hidden_effect = {
			every_owned_province = {
				limit = {
					has_province_flag = ara_sindicat_remen�a_flag
				}
				clr_province_flag = ara_sindicat_remen�a_flag
			}
		}
	}
	option = {
		name = "flavor_ara.EVTOPTB6"
		every_owned_province = {
			limit = {
				has_province_flag = ara_sindicat_remen�a_flag
				NOT = { has_province_modifier = peasants_organizing }
			}
			clr_province_flag = ara_sindicat_remen�a_flag
			add_province_modifier = {
				name = "revolutionaries_organizing"
				duration = 3650
			}
			add_unrest = 10
		}
	}
}

country_event = {
	id = flavor_ara.11
	title = "flavor_ara.11.n"
	desc = "flavor_ara.11.t"
	picture = COURT_eventPicture
	
	fire_only_once = yes

	trigger = {
		tag = ARA
		exists = SIC
		is_year = 1402
		is_subject = no
		marriage_with = SIC
		SIC = { 
			is_subject_of = ARA
			ai = yes
			is_at_war = no
			has_opinion = { who = ARA value = 100 }
			dynasty = "de Barchinona"
		 }
		dynasty = "de Barchinona"
	}

	mean_time_to_happen = {
		months = 60
		modifier = {
			factor = 0.5
			SIC = { is_female = yes }
		}
		modifier = {
			factor = 0.5
			SIC = { has_regency = yes }
		}
	}

	option = {
		name = "flavor_ara.11.a"

		ai_chance = { factor = 90 }
		inherit = SIC
		125 = {
			add_province_modifier = {
				name = "nobles_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
		2855 = {
			add_province_modifier = {
				name = "nobles_organizing"
				duration = 1825
			}
			add_unrest = 5
		}
	}
	option = {
		name = "flavor_ara.11.b"

		ai_chance = { factor = 10 }
		add_prestige = 5
	}
}

province_event = {
	id = flavor_ara.12
	title = "flavor_ara.12.n"
	desc = "flavor_ara.12.t"
	picture = COURT_eventPicture
	
	fire_only_once = yes

	trigger = {
		province_id = 146
		exists = SIC
		is_year = 1379
		ARA = { 
			is_subject = no
		    marriage_with = SIC
		    dynasty = "de Barchinona"
		 }
		SIC = { 
			owns = 146
			is_subject_of = ARA
			has_opinion = { who = ARA value = 100 }
			ai = yes
			is_at_war = no
            dynasty = "de Barchinona"
		 }
	}

	mean_time_to_happen = {
		months = 24
		modifier = {
			factor = 0.8
			owner = { dip = 4 }
		}
	}

	option = {
		name = "flavor_ara.12.a"

		ai_chance = { factor = 80 }
		cede_province = ARA 
        add_core = ARA
	}
	option = {
		name = "flavor_ara.12.b"

		ai_chance = { factor = 20 }
		add_prestige = 5
	}
}

province_event = {
	id = flavor_ara.21
	title = "flavor_ara.21.n"
	desc = "flavor_ara.21.t"
	picture = ANGRY_MOB_eventPicture
	
	trigger = {
		NOT = { has_province_modifier = at_cultural_uprising }
		OR = {
			culture = catalan
			culture = portugese
			AND = {
				area = sicily_area
				culture = sicilian
				}
			}
		owner = { 
			check_variable = { which = centralization_decentralization value = -1 }
			OR = {
				primary_culture = castillian
				primary_culture = francien
				}
			}
	}

	mean_time_to_happen = {
		months = 120
		
		modifier = {
			factor = 5
			has_province_modifier = lordship_of_valencia
		}
		modifier = {
			factor = 5
			has_province_modifier = lordship_of_catalonia
		}
		modifier = {
			factor = 5
			has_province_modifier = lordship_of_zaragossa
		}
		modifier = {
			factor = 5
			has_province_modifier = lordship_of_mallorca
		}
		modifier = {
			factor = 20
			has_province_modifier = kingdom_of_aragon
		}
		modifier = {
			factor = 5
			has_province_modifier = lordship_of_coimbra
		}
		modifier = {
			factor = 5
			has_province_modifier = lordship_of_tejo
		}
		modifier = {
			factor = 5
			has_province_modifier = lordship_of_algarve
		}
		modifier = {
			factor = 20
			has_province_modifier = kingdom_of_portugal
		}
	}

	option = {
		name = "flavor_ara.21.a"
		add_nationalism = 5
		add_province_modifier = { name = "at_cultural_uprising" duration = 730 }		
	}
	option = {
		name = "flavor_ara.21.b"
		trigger = {
			region = aragon_region
			NOT = { has_province_modifier = kingdom_of_aragon }
		}
		owner = {
			every_owned_province = {
				limit = {
					region = portugal_region
					NOT = { has_province_modifier = kingdom_of_aragon }
				}
				add_permanent_province_modifier = {
					name = kingdom_of_aragon
					duration = -1
				}
				remove_province_modifier = lordship_of_zaragossa
				remove_province_modifier = lordship_of_catalonia
				remove_province_modifier = lordship_of_valencia
				remove_province_modifier = lordship_of_mallorca
			}
		}
	}
	option = {
		name = "flavor_ara.21.c"
		trigger = {
			region = portugal_region
			NOT = { has_province_modifier = kingdom_of_portugal }
		}
		owner = {
			every_owned_province = {
				limit = {
					region = portugal_region
					NOT = { has_province_modifier = kingdom_of_portugal }
				}
				add_permanent_province_modifier = {
					name = kingdom_of_portugal
					duration = -1
				}
				remove_province_modifier = lordship_of_coimbra
				remove_province_modifier = lordship_of_tejo
				remove_province_modifier = lordship_of_algarve
			}
		}
	}
}

country_event = {
	id = flavor_ara.26
	title = "flavor_ara.26.n"
	desc = "flavor_ara.26.t"
	picture = DIPLOMACY_eventPicture

	trigger = {
		tag = POR
		NOT = { is_year = 1370 }
		war_with = CAS
		marriage_with = CAS
		ai = yes
		CAS = { dynasty = "de Borgo�a" }
	}

	mean_time_to_happen = {
		months = 12
	}

	option = {
		name = "flavor_ara.25.a"
		white_peace = CAS
	}
}

country_event = {
	id = flavor_ara.29
	title = "flavor_ara.29.n"
	desc = "flavor_ara.29.t"
	picture = COURT_eventPicture

	fire_only_once = yes
	trigger = {
		tag = ARA
		owns = 2361
		legitimacy = 95
		government = monarchy
		prestige = 30
		stability = 2
		dynasty = "de Barchinona"
	}

	mean_time_to_happen = {
		months = 12
	}

	option = {
		name = "flavor_ara.29.a"
		add_claim = 196
		add_claim = 200
		add_claim = 1375
	} 	
}

country_event = {
	id = flavor_ara.30
	title = "flavor_ara.29.n"
	desc = "flavor_ara.29.t"
	picture = COURT_eventPicture

	fire_only_once = yes
	trigger = {
		tag = ARA
		owns = 201
		owns = 1512
		legitimacy = 95
		government = monarchy
		prestige = 30
		stability = 2
		dynasty = "de Barchinona"
	}

	mean_time_to_happen = {
		months = 12
	}

	option = {
		name = "flavor_ara.29.a"
		add_claim = 1383
		add_claim = 1323
	} 	
}

country_event = {
	id = flavor_ara.101
	title = "flavor_ara.101.n"
	desc = "flavor_ara.101.t"
	picture = COURT_eventPicture
	
	trigger = {
		overlord_of = ATH
		OR = {
			culture_group = iberian
			culture_group = catalan_group
			culture_group = napolitan
		}
		religion_group = christian
	}

	mean_time_to_happen = {
		months = 240
	}
	
	immediate = {
		free_vassal = ATH
		ATH = {
			define_ruler = {
				name = "Rainerio I"
				dynasty = "Acciaioli"
				DIP = 3
				ADM = 2
				MIL = 5
				attach_leader = "Rainerio Acciaioli"
			}
			infantry = 146
			infantry = 146
			infantry = 146
			infantry = 146
			cavalry = 146
			cavalry = 146
			add_treasury = 350
		}
	}

	option = {
		name = "flavor_ara.101.a"
		ai_chance = { factor = 100 }
		# Accept
	}

	option = {
		name = "flavor_ara.101.b"
		ai_chance = { factor = 0 }
		every_province = {
			limit = {
				owned_by = ATH
			}
			add_core = ROOT
		}
	}
}
