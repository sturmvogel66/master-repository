######################################
#                                    #
#    Rising and Declining Nations    #
#                                    #
######################################
# SUMMARY
# Purpose - series of events intended to give help nations rise
# or push them off the cliff.
#
# IDs 0912801 - 913000
# 0912801 - Sets the rising_nation flag
# 0912851 - Removes the rising_nation flag
# 0912900 - Sets the declining_nation flag
# 0912951 - Removes the declining_nation flag
#####################################
# General changes:
# - 
#####################################
# Outstanding issues:
# -
#####################################
# Notes:
# - 
#####################################

# hand_of_god.0001 - Sets the rising nation flag
country_event = {

	id = hand_of_god.0001
	title = "hand_of_god.0001.n"
	desc = "hand_of_god.0001.t"
	picture = RISEOFNATIONS_eventPicture
	
	hidden = yes
	
	trigger = {
		has_global_flag = meiou_and_taxes_mod_start 
		NOT = { has_country_flag = in_play_at_start }
		NOT = { has_country_flag = detected_new_country }
		NOT = { has_country_modifier = blessing_of_god }
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	
	option = { 
		name = "OPT.VERYGOOD"
        set_country_flag = detected_new_country 
        add_country_modifier = { name = blessing_of_god duration = -1 }
	}
}

# hand_of_god.1001 - Removes the rising nation flag
country_event = {

	id = hand_of_god.1001
	title = "hand_of_god.1001.n"
	desc = "hand_of_god.1001.t"
	picture = RISEOFNATIONS_eventPicture

	hidden = yes
	
	trigger = {
		has_country_modifier = blessing_of_god
		# had_country_flag = { flag = rising_nation days = 9000 } #Guaranteed 25 years before this can fire
	}

	mean_time_to_happen = {
		months = 720
		modifier = {
			factor = 0.95
			NOT = { stability = 1 }
		}
		modifier = {
			factor = 0.95
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.95
			inflation = 10
		}
		modifier = {
			factor = 0.95
			NOT = { prestige = 0.00 }
		}
		modifier = {
			factor = 0.95
			NOT = { prestige = -0.10 }
		}
		modifier = {
			factor = 0.95
			NOT = { prestige = -0.20 }
		}
		modifier = {
			factor = 0.90
			is_at_war = yes
		}
		modifier = {
			factor = 0.90
			has_wartaxes = yes
		}
		modifier = {
			factor = 0.90
			war_exhaustion = 5
		}
		modifier = {
			factor = 0.90
			war_exhaustion = 10
		}
		modifier = {
			factor = 0.90
			war_exhaustion = 15
		}
		modifier = {
			factor = 0.90
			war_exhaustion = 20
		}
		modifier = {
			factor = 0.75
			is_bankrupt = yes
		}
		modifier = {
			factor = 0.95
			inflation = 20
		}
		modifier = {
			factor = 0.95
			NOT = { ADM = 4 }
		}
		modifier = {
			factor = 0.95
			NOT = { ADM = 2 }
		}
		modifier = {
			factor = 0.95
			NOT = { MIL = 4 }
		}
		modifier = {
			factor = 0.95
			NOT = { MIL = 2 }
		}
		modifier = {
			factor = 0.95
			NOT = { DIP = 4 }
		}
		modifier = {
			factor = 0.95
			NOT = { DIP = 2 }
		}
		modifier = {
			factor = 0.95
			OR = {
				government = constitutional_monarchy
				government = enlightened_despotism
			}
		}
		modifier = {
			factor = 0.90
			OR = {
				government = absolute_monarchy
				government = republican_dictatorship
			}
		}
		modifier = {
			factor = 0.80
			OR = {
				government = bureaucratic_despotism
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
		}
		modifier = {
			factor = 0.75
			OR = {
				government = eastern_monarchy
				government = indian_monarchy
				government = rajput_monarchy
				government = altaic_monarchy
				government = tribal_monarchy
				government = tribal_republic
				government = chinese_monarchy
				government = chinese_monarchy_2
				government = chinese_monarchy_3
				government = chinese_monarchy_4
				government = chinese_monarchy_5
				government = revolutionary_empire
				government = revolutionary_republic
			}
		}
	}

	option = {
		name = "OPT.UNFORTUNATE"
		remove_country_modifier = blessing_of_god
	}
}

# hand_of_god.2001 - Sets the wrath_of_god flag
country_event = {

	id = hand_of_god.2001
	title = "hand_of_god.2001.n"
	desc = "hand_of_god.2001.t"
	picture = RISEOFNATIONS_eventPicture

	trigger = {
		num_of_cities = 10
		NOT = { stability = 1 }
		NOT = { prestige = 30 }
		NOT = { AND = { ADM = 5 MIL = 5 } }
		NOT = { advisor = statesman }
		OR = { #Must have a reason for going into decline
			NOT = { stability = -1 }
			war_exhaustion = 10
			NOT = { manpower_percentage = 0.10 }
			num_of_loans = 5
			is_bankrupt = yes
			inflation = 10
			NOT = { ADM = 3 DIP = 3 MIL = 3 }
			}
		NOT = { has_country_modifier = blessing_of_god }
		NOT = { has_country_modifier = wrath_of_god }
	}

	mean_time_to_happen = {
		months = 3600	#300 years
		#modifier = {
		#	factor = 0.8
		#	has_country_flag = civil_unrest 
		#}
		#modifier = {
		#	factor = 0.8
		#	has_country_flag = clergy_discontent 
		#}
		#modifier = {
		#	factor = 0.8
		#	has_country_flag = economical_collapse 
		#}
		#modifier = {
		#	factor = 0.8
		#	has_country_flag = internal_strife 
		#}
		#modifier = {
		#	factor = 0.8
		#	has_country_flag = military_collapse 
		#}
		modifier = {
			factor = 0.9
			NOT = { stability = 1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			inflation = 10
		}
		modifier = {
			factor = 0.9
			inflation = 20
		}
		modifier = {
			factor = 0.95
			NOT = { prestige = 0.00 }
		}
		modifier = {
			factor = 0.95
			NOT = { prestige = -0.10 }
		}
		modifier = {
			factor = 0.95
			NOT = { prestige = -0.20 }
		}
		modifier = {
			factor = 0.90
			is_at_war = yes
		}
		modifier = {
			factor = 0.90
			has_wartaxes = yes
		}
		modifier = {
			factor = 0.90
			war_exhaustion = 5
		}
		modifier = {
			factor = 0.90
			war_exhaustion = 10
		}
		modifier = {
			factor = 0.90
			war_exhaustion = 15
		}
		modifier = {
			factor = 0.90
			war_exhaustion = 20
		}
		modifier = {
			factor = 0.75
			is_bankrupt = yes
		}
		modifier = {
			factor = 0.9
			NOT = { ADM = 4 }
		}
		modifier = {
			factor = 0.9
			NOT = { ADM = 2 }
		}
		modifier = {
			factor = 0.9
			NOT = { MIL = 4 }
		}
		modifier = {
			factor = 0.9
			NOT = { MIL = 2 }
		}
		modifier = {
			factor = 0.9
			NOT = { DIP = 4 }
		}
		modifier = {
			factor = 0.9
			NOT = { DIP = 2 }
		}
		modifier = {
			factor = 0.9
			government = enlightened_despotism
		}
		modifier = {
			factor = 0.8
			OR = {
				government = absolute_monarchy
				government = republican_dictatorship
			}
		}
		modifier = {
			factor = 0.7
			OR = {
				government = bureaucratic_despotism
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
		}
		modifier = {
			factor = 0.6
			OR = {
				government = eastern_monarchy
				government = indian_monarchy
				government = rajput_monarchy
				government = altaic_monarchy
				government = tribal_monarchy
				government = tribal_republic
				government = chinese_monarchy
				government = chinese_monarchy_2
				government = chinese_monarchy_3
				government = chinese_monarchy_4
				government = chinese_monarchy_5
			}
		}
		modifier = {
			factor = 0.50
			num_of_cities = 50
		}
		modifier = {
			factor = 0.50
			num_of_cities = 100
		}
		modifier = {
			factor = 0.50
			num_of_cities = 150
		}
		modifier = {
			factor = 0.50
			num_of_cities = 200
		}
		modifier = {
			factor = 2.00
			adm_tech = 10
		}
		modifier = {
			factor = 2.00
			adm_tech = 20
		}
		modifier = {
			factor = 2.00
			adm_tech = 30
		}
		modifier = {
			factor = 2.00
			adm_tech = 50
		}
	}

	immediate = {
		add_country_modifier = { name = wrath_of_god duration = -1 } 
		if = {
			limit = { 
				OR = {
					war_exhaustion = 10
					NOT = { manpower_percentage = 0.10 }
					}
				}
			custom_tooltip = wrath_of_god_exhaustion
			}
		if = { 
			limit = { NOT = { stability = -1 } }
			custom_tooltip = wrath_of_god_stability
			}
		if = {
			limit = { inflation = 10 }
			custom_tooltip = wrath_of_god_inflation
			}
		if = {
			limit = { 
				OR = {
					is_bankrupt = yes
					num_of_loans = 5
					}
				}
			custom_tooltip = wrath_of_god_bankrupt
			}
		if = {
			limit = { 
				NOT = { ADM = 3 MIL = 3 DIP = 3 }
				}
			custom_tooltip = wrath_of_god_ruler
			}
		}
	
	option = {
		name = "OPT.OHNO"
	}
}

# 0912951 - Removes the declining nation
# Replaced by a decision
