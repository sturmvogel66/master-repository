########################################
# Flavor Events for Serbia
#
# Scripted by GiGau
########################################


# Simeon Uros proclaims himself Tsar of the Greeks, Serbs, and Albanians and marches on Serbia
country_event = {
	id = flavor_ser.1
	title = "flavor_ser.1.n"
	desc = "flavor_ser.1.t"
	picture = SERBIA_eventPicture

	fire_only_once = yes

	trigger = {
		tag = SER
		NOT = { is_year = 1360 }
		owns = 3775
	}

	mean_time_to_happen = {
		months = 12
	}

	option = {
		name = "flavor_ser.1.a"
		TSL = {
			add_casus_belli = {
				target = SER
				type = cb_fabricated_claims
				months = 1825
			}
		}
	}
}
country_event = {
	id = flavor_ser.2
	title = "flavor_ser.2.n"
	desc = "flavor_ser.2.t"
	picture = SERBIA_eventPicture

	fire_only_once = yes
	
	is_triggered_only = yes

	trigger = {
		tag = SER
		NOT = { is_year = 1444 }
	}
	
	immediate = {
		hidden_effect = {
			break_union = MON
			break_union = VUK
			break_union = VBZ
			break_union = MKO
			break_union = SRR
			MON = {
				define_ruler = {
					name = "Balsza"
					dynasty = "Balsic"
					DIP = 5 
					ADM = 4
					MIL = 3
					fixed = yes
					age = 50
				}
				define_heir = {
					claim = 100
					name = "George"
					dynasty = "Balsic"
					adm = 1
					dip = 2
					mil = 3
					birth_date = 1320.1.1						
				}
			}
			
			VUK = {
				define_ruler = {
					name = "Branko Mladenovic"
					dynasty = "Brankovic"
					DIP = 3 
					ADM = 3
					MIL = 3
					age = 50
				}
				define_heir = {
					claim = 100
					name = "Vuk"
					dynasty = "Brankovic"
					adm = 4
					dip = 4
					mil = 4
					birth_date = 1345.1.1						
				}
				
			}
			VBZ = {
				define_ruler = {
					name = "Dejan"
					dynasty = "Dragas"
					DIP = 3 
					ADM = 3
					MIL = 3
					age = 50
				}
				define_heir = {
					claim = 100
					name = "Constantine"
					dynasty = "Dragas"
					adm = 4
					dip = 4
					mil = 4
					birth_date = 1354.1.1						
				}
			}			
			MKO = {
				define_ruler = {
					name = "Vukasin"
					dynasty = "Mrnjavcevic"
					DIP = 3 
					ADM = 3
					MIL = 3
					age = 60
				}
				define_heir = {
					claim = 100
					name = "Marko"
					dynasty = "Mrnjavcevic"
					adm = 1
					dip = 1
					mil = 0
					birth_date = 1335.1.1						
				}
			}	
			SRR = {
				define_ruler = {
					name = "Ugljesa"
					dynasty = "Mrnjavcevic"
					DIP = 3 
					ADM = 3
					MIL = 3
					age = 70
				}
			}
			every_country = {
				limit = {
					marriage_with = SER
				}
				break_marriage = SER
			}
		}	
	}

	option = {
		name = "flavor_ser.2.a"
		custom_tooltip = adios_serbia
		define_ruler = {
			name = "Lazar Hrebeljanovic"
			dynasty = "Lazarevic"
			DIP = 3 
			ADM = 3
			MIL = 3
			age = 40
		}
		remove_country_modifier = obstacle_feudal_fragmentation
		set_government_rank = 2
		
	}
}

