########################################
#       Changing Tier Levels.txt       #
# for MEIOU by GG                      #
# v1 2009.09.28                        #
# v2 2010.02.03 FB                     #
# v2.01 2010.06.03 kfoelsch fixed king #
# v3.01 2014.02.08 gigau added to M&T  #
########################################
# SUMMARY
# Purpose - to change the tier levels
# count <> marquis <> duke <> prince/grand duke <> king <> emperor
#
# OVERVIEW
# - For HRE members, promotion requires authorization of the Emperor
# - For non HRE catholic nations, the authorization of the Pope
# - For vassals, the authorization of the overlord
# - For the others, a high prestige.
# - shugos get promoted to sengoku and sengokus get demoted to shugo
# - a powerful enough sengoku gets promoted to shogun
# - if the shogun is still powerful, the following one becomes contender
#
# IDs 0tier_change.1 - 0tier_change.xx
# tier_change.1-34 - responses to promotions requested (triggered by decisions)
# tier_change.1	- Towards margraviateship in the HRE
# tier_change.2	- Towards dukeship in the HRE
# tier_change.3	- Towards princedom in the HRE
# tier_change.4	- Towards kingship in the HRE
# tier_change.11	- Towards margraviateship as vassal
# tier_change.12	- Towards dukeship as vassal
# tier_change.13	- Towards princedom as vassal
# tier_change.14	- Towards kingship as vassal
# tier_change.21	- Towards margraviateship as catholic
# tier_change.22	- Towards dukeship as catholic
# tier_change.23	- Towards princedom as catholic
# tier_change.24	- Towards kingship as catholic
# tier_change.25	- Towards empire crown as catholic
# tier_change.31	- Towards margraviateship as orthodox - TO DO
# tier_change.32	- Towards dukeship as orthodox - TO DO
# tier_change.33	- Towards princedom as orthodox - TO DO
# tier_change.34	- Towards kingship as orthodox - TO DO
# tier_change.35	- Towards empire crown as orthodox - TO DO
# tier_change.51-55 - promotions gifted on merit
# tier_change.51	- count > marquis
# tier_change.52	- marquis > duke
# tier_change.53	- duke > prince/grand duke
# tier_change.54	- prince/grand duke > king
# tier_change.55	- king > emperor
# tier_change.71-99 - special cases
# tier_change.71	- New Japan becomes an empire - TO DO
# tier_change.72	- New Yavapeh becomes an empire - TO DO
# tier_change.73	- New Gupta nation becomes an empire - TO DO
# tier_change.74	- Prussia formed as a feudal duchy - TO DO
# tier_change.75	- Prussia formed as an absolute monarchy - TO DO
#
#####################################
# General changes:
# v2 added tier_change.51-55 - promotions gifted on merit
#
#
#####################################
# Outstanding issues:
# - orthodoc promotions
# - special cases
#
#####################################
# Notes:
# - Events tier_change.1 - tier_change.35 supplement Decisions in ChangingTierLevel.txt
#
#
#####################################


# tier_change.1	- Towards margraviateship in the HRE
country_event = {

	id = tier_change.1
	title = "tier_change.1.n"		# Margravate
	desc = "tier_change.1.t"		# A member of the Empire has asked us to be granted the title of Margrave over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_margraviate_hre
								# ROOT = the emperor
	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.41 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.42 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 2
			add_legitimacy = 10
		}
	}
}

# tier_change.2	- Towards dukeship in the HRE
country_event = {

	id = tier_change.2
	title = "tier_change.2.n"		# Ducal crown
	desc = "tier_change.2.t"		# A member of the Empire has asked us to be granted the duke title over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_dukedom_hre
					# ROOT = the emperor

	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.41 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.42 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 3
			add_legitimacy = 10
		}
	}
}

# tier_change.3	- Towards princedom in the HRE
country_event = {

	id = tier_change.3
	title = "tier_change.3.n"		# Princes crown
	desc = "tier_change.3.t"		# A member of the Empire has asked us to be granted the title Prince over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_princedom_hre
					# ROOT = the emperor

	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.41 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.42 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 4
			add_legitimacy = 10
		}
	}
}

# tier_change.4	- Towards kingship in the HRE
country_event = {

	id = tier_change.4
	title = "tier_change.4.n"		# Kings crown
	desc = "tier_change.4.t"		# A member of the Empire has asked us to be granted the title of King over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_kingship_hre
					# ROOT = the emperor

	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.41 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.42 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 5
			add_legitimacy = 10
		}
	}
}

# tier_change.11	- Towards margraviateship as a vassal
country_event = {

	id = tier_change.11
	title = "tier_change.11.n"		# Margraves crown
	desc = "tier_change.11.t"		# A vassal has asked us to be granted the title of Margrave over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_margraviate_vassal
					# ROOT = the overlord
	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.43 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.44 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 2
			add_legitimacy = 10
		}
	}
}

# tier_change.12	- Towards dukeship as a vassal
country_event = {

	id = tier_change.12
	title = "tier_change.12.n"		# Ducal crown
	desc = "tier_change.12.t"		# A vassal has asked us to be granted the duke title over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_dukedom_vassal
					# ROOT = the overlord
	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.43 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.44 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 3
			add_legitimacy = 10
		}
	}
}

# tier_change.13	- Towards princedom as a vassal
country_event = {

	id = tier_change.13
	title = "tier_change.13.n"		# Princes crown
	desc = "tier_change.13.t"		# A vassal has asked us to be granted the title of Prince over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_princedom_vassal
					# ROOT = the overlord
	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.43 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.44 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 4
			add_legitimacy = 10
		}
	}
}

# tier_change.14	- Towards kingship as a vassal
country_event = {

	id = tier_change.14
	title = "tier_change.14.n"		# Kings crown
	desc = "tier_change.14.t"		# A vassal has asked us to be granted the title of King over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_kingship_vassal
					# ROOT = the overlord
	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.43 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.44 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 5
			add_legitimacy = 10
		}
	}
}

# tier_change.21	- Towards margraviateship as catholic
country_event = {

	id = tier_change.21
	title = "tier_change.21.n"		# Margraves crown
	desc = "tier_change.21.t"		# A catholic country has asked us to be granted the title of Margrave over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_margraviate_catholic
					# ROOT = PAP
	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
			modifier = {
				is_female = yes NOT = { piety = 50 }
				factor = 1.5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.45 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.46 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 2
			add_legitimacy = 10
		}
	}
}

# tier_change.22	- Towards dukeship as catholic
country_event = {

	id = tier_change.22
	title = "tier_change.22.n"		# Ducal crown
	desc = "tier_change.22.t"		# A catholic country has asked us to be granted the duke title over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_dukedom_catholic
					# ROOT = PAP
	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
			modifier = {
				is_female = yes NOT = { piety = 50 }
				factor = 1.5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.45 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.46 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 3
			add_legitimacy = 10
		}
	}
}

# tier_change.23	- Towards princedom as catholic
country_event = {

	id = tier_change.23
	title = "tier_change.23.n"		# Princes crown
	desc = "tier_change.23.t"		# A catholic country has asked us to be granted the title of Prince over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_princedom_catholic
					# ROOT = PAP
	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
			modifier = {
				is_female = yes NOT = { piety = 50 }
				factor = 1.5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.45 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.46 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 4
			add_legitimacy = 10
		}
	}
}

# tier_change.24	- Towards kingship as catholic
country_event = {

	id = tier_change.24
	title = "tier_change.24.n"		# Kings crown
	desc = "tier_change.24.t"		# A catholic country has asked us to be granted the title of King over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_kingship_catholic
					# ROOT = PAP
	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
			modifier = {
				is_female = yes NOT = { piety = 50 }
				factor = 1.5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.45 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.46 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 5
			add_legitimacy = 10
		}
	}
}

# tier_change.25	- Towards empire crown as catholic
country_event = {

	id = tier_change.25
	title = "tier_change.25.n"		# Emperors crown
	desc = "tier_change.25.t"		# A catholic country has asked us to be granted the title of Emperor over his demesne.
	picture = COURT_eventPicture

	is_triggered_only = yes		# called by decision: up_to_emperor_catholic
					# ROOT = PAP
	option = {
		name = "OPT.HELLNO"		# Hell no !
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = 100 } }
				factor = 2
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = 50 } }
				factor = 5
			}
			modifier = {
				is_female = yes NOT = { piety = 50 }
				factor = 1.5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_refused }
			country_event = { id = tier_change.45 days = 5 }
			add_country_modifier = { name = "promotion_denied" duration = 3000 }
		}
	}

	option = {
		name = "OPT.WHYNOT"		# Why not ?
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 2
			}
			modifier = {
				has_opinion = { who = FROM value = 180 }
				factor = 5
			}
		}
		FROM = {
			add_opinion = { who = ROOT modifier = promotion_accepted }
			country_event = { id = tier_change.46 days = 5 }
			add_country_modifier = { name = "newly_promoted" duration = 3000 }
			set_government_rank = 6
			add_legitimacy = 10
		}
	}
}

# tier_change.31	- Towards margraviateship as orthodox
# tier_change.32	- Towards dukeship as orthodox
# tier_change.33	- Towards princedom as orthodox
# tier_change.34	- Towards kingship as orthodox
# tier_change.35	- Towards empire crown as orthodox

# tier_change.41-50 - Notification of the reply
country_event = {
	
	id = tier_change.41
	title = "tier_change.41.n"
	desc = "tier_change.41.t"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		clr_country_flag = promotion_requested
		name = "OPT.UNFORTUNATE"
		add_prestige = -25
	}
}

country_event = {
	
	id = tier_change.42
	title = "tier_change.42.n"
	desc = "tier_change.42.t"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		clr_country_flag = promotion_requested
		name = "OPT.HURRAH"
		add_prestige = 10
	}
}

country_event = {
	
	id = tier_change.43
	title = "tier_change.43.n"
	desc = "tier_change.43.t"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		clr_country_flag = promotion_requested
		name = "OPT.UNFORTUNATE"
		add_prestige = -25
	}
}

country_event = {
	
	id = tier_change.44
	title = "tier_change.44.n"
	desc = "tier_change.44.t"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		clr_country_flag = promotion_requested
		name = "OPT.HURRAH"
		add_prestige = 10
	}
}

country_event = {
	
	id = tier_change.45
	title = "tier_change.45.n"
	desc = "tier_change.45.t"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		clr_country_flag = promotion_requested
		name = "OPT.UNFORTUNATE"
		add_prestige = -25
	}
}

country_event = {
	
	id = tier_change.46
	title = "tier_change.46.n"
	desc = "tier_change.46.t"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		clr_country_flag = promotion_requested
		name = "OPT.HURRAH"
		add_prestige = 10
	}
}


# tier_change.51-55 - promotions gifted on merit
# tier_change.51	- count > marquis
country_event = {
	id = tier_change.51
	title = "tier_change.51.n"
	desc = "tier_change.51.t"
	picture = COURT_eventPicture

	trigger = {
		num_of_cities = 3
		prestige = 10
		OR = {
			DIP = 2
			MIL = 3
			advisor = statesman
		}
		OR = {
			AND = {
				capital_scope = { is_part_of_hre = yes }
				is_subject = no
				emperor = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE catholic country
			AND = {
				NOT = { is_part_of_hre = yes }
				religion = catholic
				is_subject = no
				PAP = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE non-catholic country
			AND = {
				NOT = { is_part_of_hre = yes }
				NOT = { religion = catholic }
				is_subject = no
				prestige = 20
			}
		    #vassals
			#HRE vassal
			AND = {
				is_part_of_hre = yes
				is_subject = yes
				overlord = { has_opinion = { who = ROOT value = 150 } }
				emperor = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE catholic vassal
			AND = {
				NOT = { is_part_of_hre = yes }
				religion = catholic
				is_subject = yes
				overlord = { has_opinion = { who = ROOT value = 150 } }
				PAP = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE non-catholic vassal
			AND = {
				NOT = { is_part_of_hre = yes }
				NOT = { religion = catholic }
				is_subject = yes
				overlord = { has_opinion = { who = ROOT value = 150 } }
				prestige = 20
			}
		}
		OR = {
				government = despotic_monarchy
				government = medieval_monarchy
				government = feudal_monarchy
				government = irish_monarchy
				government = administrative_monarchy
				government = absolute_monarchy
				government = constitutional_monarchy
				government = enlightened_despotism
				government = eastern_monarchy
				government = indian_monarchy
				government = rajput_monarchy
				government = tribal_monarchy
				government = tribal_republic
				government = altaic_monarchy
				government = steppe_horde
				government = colonial_government
				government = feudal_colonial_government
				government = trader_colonial_government
				government = population_colonial_government
				government = trade_company_gov
		}
		government_rank = 1
		NOT = { government_rank = 2 }
		has_regency = no
		# is_female = no
		NOT = { has_country_modifier = newly_promoted }
		NOT = { has_country_modifier = promotion_denied }
	}
	mean_time_to_happen = {
		months = 300
		modifier = {
			factor = 1.25
			is_female = yes
		}
		modifier = {
			factor = 0.9
			stability = -1
		}
		modifier = {
			factor = 0.9
			stability = 1
		}
		modifier = {
			factor = 0.9
			stability = 3
		}
		modifier = {
			factor = 0.9
			DIP = 3
		}
		modifier = {
			factor = 0.9
			DIP = 4
		}
		modifier = {
			factor = 0.9
			DIP = 5
		}
		modifier = {
			factor = 0.9
			DIP = 6
		}
		modifier = {
			factor = 0.9
			prestige = 20
		}
		modifier = {
			factor = 0.9
			prestige = 40
		}
	}

	option = {
		name = "OPT.VERYGOOD"
		add_country_modifier = { name = "newly_promoted" duration = 3000 }
		set_government_rank = 2
	}
}

# tier_change.52	- marquis > duke
country_event = {
	id = tier_change.52
	title = "tier_change.52.n"
	desc = "tier_change.52.t"
	picture = COURT_eventPicture

	trigger = {
		num_of_cities = 5
		prestige = 25
		OR = {
			DIP = 2
			MIL = 3
			advisor = statesman
		}
		OR = {
		    #non-vassals
			#HRE country
			AND = {
				capital_scope = { is_part_of_hre = yes }
				is_subject = no
				emperor = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE catholic country
			AND = {
				NOT = { is_part_of_hre = yes }
				religion = catholic
				is_subject = no
				PAP = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE non-catholic country
			AND = {
				NOT = { is_part_of_hre = yes }
				NOT = { religion = catholic }
				is_subject = no
				prestige = 25
			}
		    #vassals
			#HRE vassal
			AND = {
				is_part_of_hre = yes
				is_subject = yes
				overlord = { has_opinion = { who = ROOT value = 150 } }
				emperor = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE catholic vassal
			AND = {
				NOT = { is_part_of_hre = yes }
				religion = catholic
				is_subject = yes
				overlord = { has_opinion = { who = ROOT value = 150 } }
				PAP = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE non-catholic vassal
			AND = {
				NOT = { is_part_of_hre = yes }
				NOT = { religion = catholic }
				is_subject = yes
				overlord = { has_opinion = { who = ROOT value = 150 } }
				prestige = 25
			}
		}
		OR = {
				government = despotic_monarchy
				government = medieval_monarchy
				government = feudal_monarchy
				government = irish_monarchy
				government = administrative_monarchy
				government = absolute_monarchy
				government = constitutional_monarchy
				government = enlightened_despotism
				government = eastern_monarchy
				government = indian_monarchy
				government = rajput_monarchy
				government = tribal_monarchy
				government = tribal_republic
				government = altaic_monarchy
				government = steppe_horde
				government = colonial_government
				government = feudal_colonial_government
				government = trader_colonial_government
				government = population_colonial_government
				government = trade_company_gov
		}
		has_regency = no
		# is_female = no
		government_rank = 2
		NOT = { government_rank = 3 }
		NOT = { has_country_modifier = newly_promoted }
		NOT = { has_country_modifier = promotion_denied }
	}

	mean_time_to_happen = {
		months = 300
		modifier = {
			factor = 1.25
			is_female = yes
		}
		modifier = {
			factor = 0.9
			stability = -1
		}
		modifier = {
			factor = 0.9
			stability = 1
		}
		modifier = {
			factor = 0.9
			stability = 3
		}
		modifier = {
			factor = 0.9
			DIP = 3
		}
		modifier = {
			factor = 0.9
			DIP = 4
		}
		modifier = {
			factor = 0.9
			DIP = 5
		}
		modifier = {
			factor = 0.9
			DIP = 6
		}
		modifier = {
			factor = 0.9
			prestige = 20
		}
		modifier = {
			factor = 0.9
			prestige = 40
		}
	}

	option = {
		name = "OPT.VERYGOOD"
		add_country_modifier = { name = "newly_promoted" duration = 3000 }
		set_government_rank = 3
	}
}

# tier_change.53	- duke > prince/grand duke
country_event = {
	id = tier_change.53
	title = "tier_change.53.n"
	desc = "tier_change.53.t"
	picture = COURT_eventPicture
	
	trigger = {
		num_of_cities = 7
		prestige = 25
		OR = {
			DIP = 2
			MIL = 3
			advisor = statesman
		}
		OR = {
		    #non-vassals
			#HRE country
			AND = {
				capital_scope = { is_part_of_hre = yes }
				is_subject = no
				emperor = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE catholic country
			AND = {
				NOT = { is_part_of_hre = yes }
				religion = catholic
				is_subject = no
				PAP = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE non-catholic country
			AND = {
				NOT = { is_part_of_hre = yes }
				NOT = { religion = catholic }
				is_subject = no
				prestige = 35
			}
		    #vassals
			#HRE vassal
			AND = {
				is_part_of_hre = yes
				is_subject = yes
				overlord = { has_opinion = { who = ROOT value = 150 } }
				emperor = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE catholic vassal
			AND = {
				NOT = { is_part_of_hre = yes }
				religion = catholic
				is_subject = yes
				overlord = { has_opinion = { who = ROOT value = 150 } }
				PAP = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE non-catholic vassal
			AND = {
				NOT = { is_part_of_hre = yes }
				NOT = { religion = catholic }
				is_subject = yes
				overlord = { has_opinion = { who = ROOT value = 150 } }
				prestige = 35
			}
		}
		OR = {
				government = despotic_monarchy
				government = medieval_monarchy
				government = feudal_monarchy
				government = irish_monarchy
				government = administrative_monarchy
				government = absolute_monarchy
				government = constitutional_monarchy
				government = enlightened_despotism
				government = eastern_monarchy
				government = indian_monarchy
				government = rajput_monarchy
				government = tribal_monarchy
				government = tribal_republic
				government = altaic_monarchy
				government = steppe_horde
				government = colonial_government
				government = feudal_colonial_government
				government = trader_colonial_government
				government = population_colonial_government
				government = trade_company_gov
		}
		NOT = { has_country_modifier = newly_promoted }
		NOT = { has_country_modifier = promotion_denied }
		has_regency = no
		# is_female = no
		government_rank = 3
		NOT = { government_rank = 4 }
	}
	mean_time_to_happen = {
		months = 300
		modifier = {
			factor = 1.25
			is_female = yes
		}
		modifier = {
			factor = 0.9
			stability = -1
		}
		modifier = {
			factor = 0.9
			stability = 1
		}
		modifier = {
			factor = 0.9
			stability = 3
		}
		modifier = {
			factor = 0.9
			DIP = 3
		}
		modifier = {
			factor = 0.9
			DIP = 4
		}
		modifier = {
			factor = 0.9
			DIP = 5
		}
		modifier = {
			factor = 0.9
			DIP = 6
		}
		modifier = {
			factor = 0.9
			prestige = 20
		}
		modifier = {
			factor = 0.9
			prestige = 40
		}
	}

	option = {
		name = "OPT.VERYGOOD"
		add_country_modifier = { name = "newly_promoted" duration = 3000 }
		set_government_rank = 4
	}
}

# tier_change.54	- prince/grand duke > king
country_event = {
	id = tier_change.54
	title = "tier_change.54.n"
	desc = "tier_change.54.t"
	picture = COURT_eventPicture

	trigger = {
		num_of_cities = 40
		prestige = 45
		OR = {
			DIP = 3
			MIL = 4
			advisor = statesman
		}
		OR = {
		    #non-vassals
			#HRE country
			AND = {
				capital_scope = { is_part_of_hre = yes }
				is_subject = no
				emperor = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE catholic country
			AND = {
				NOT = { is_part_of_hre = yes }
				religion = catholic
				is_subject = no
				PAP = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE non-catholic country
			AND = {
				NOT = { is_part_of_hre = yes }
				NOT = { religion = catholic }
				is_subject = no
				prestige = 55
			}
		    #vassals
			#HRE vassal
			AND = {
				is_part_of_hre = yes
				is_subject = yes
				overlord = { has_opinion = { who = ROOT value = 150 } }
				emperor = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE catholic vassal
			AND = {
				NOT = { is_part_of_hre = yes }
				religion = catholic
				is_subject = yes
				overlord = { has_opinion = { who = ROOT value = 150 } }
				PAP = { has_opinion = { who = ROOT value = 150 } }
			}
			#non-HRE non-catholic vassal
			AND = {
				NOT = { is_part_of_hre = yes }
				NOT = { religion = catholic }
				is_subject = yes
				overlord = { has_opinion = { who = ROOT value = 150 } }
				prestige = 55
			}
		}
		OR = {
				government = despotic_monarchy
				government = medieval_monarchy
				government = feudal_monarchy
				government = irish_monarchy
				government = administrative_monarchy
				government = absolute_monarchy
				government = constitutional_monarchy
				government = enlightened_despotism
				government = eastern_monarchy
				government = indian_monarchy
				government = rajput_monarchy
				government = tribal_monarchy
				government = tribal_republic
				government = altaic_monarchy
				government = steppe_horde
				government = colonial_government
				government = feudal_colonial_government
				government = trader_colonial_government
				government = population_colonial_government
				government = trade_company_gov
		}
		NOT = { has_country_modifier = newly_promoted }
		NOT = { has_country_modifier = promotion_denied }
		has_regency = no
		# is_female = no
		government_rank = 4
		NOT = { government_rank = 5 }
	}
	mean_time_to_happen = {
		months = 300
		modifier = {
			factor = 1.25
			is_female = yes
		}
		modifier = {
			factor = 0.9
			stability = -1
		}
		modifier = {
			factor = 0.9
			stability = 1
		}
		modifier = {
			factor = 0.9
			stability = 3
		}
		modifier = {
			factor = 0.9
			DIP = 3
		}
		modifier = {
			factor = 0.9
			DIP = 4
		}
		modifier = {
			factor = 0.9
			DIP = 5
		}
		modifier = {
			factor = 0.9
			DIP = 6
		}
		modifier = {
			factor = 0.9
			prestige = 20
		}
		modifier = {
			factor = 0.9
			prestige = 40
		}
	}

	option = {
		name = "OPT.VERYGOOD"
		add_country_modifier = { name = "newly_promoted" duration = 3000 }
		set_government_rank = 5
	}
}

# tier_change.55	- king > emperor
country_event = {
	id = tier_change.55
	title = "tier_change.55.n"
	desc = "tier_change.55.t"
	picture = COURT_eventPicture

	trigger = {
		num_of_cities = 60
		prestige = 85
		OR = {
			DIP = 4
			MIL = 5
			advisor = statesman
		}
		OR = {
		    #non-vassals
			#HRE countries cannot be emperors!
			#non-HRE catholic country
			AND = {
				NOT = { is_part_of_hre = yes }
				religion = catholic
				is_subject = no
				PAP = { has_opinion = { who = ROOT value = 190 } }
			}
			#non-HRE non-catholic country
			AND = {
				NOT = { is_part_of_hre = yes }
				NOT = { religion = catholic }
				is_subject = no
				prestige = 95
			}
		    #vassals cannot be emperors!
		}
		OR = {
				government = despotic_monarchy
				government = medieval_monarchy
				government = feudal_monarchy
				government = irish_monarchy
				government = administrative_monarchy
				government = absolute_monarchy
				government = constitutional_monarchy
				government = enlightened_despotism
				government = eastern_monarchy
				government = indian_monarchy
				government = rajput_monarchy
				government = tribal_monarchy
				government = tribal_republic
				government = altaic_monarchy
				government = steppe_horde
				government = colonial_government
				government = feudal_colonial_government
				government = trader_colonial_government
				government = population_colonial_government
				government = trade_company_gov
		}
		has_regency = no
		# is_female = no
		government_rank = 5
		NOT = { government_rank = 6 }
		NOT = { has_country_modifier = newly_promoted }
		NOT = { has_country_modifier = promotion_denied }
	}
	mean_time_to_happen = {
		months = 300
		modifier = {
			factor = 0.9
			stability = -1
		}
		modifier = {
			factor = 0.9
			stability = 1
		}
		modifier = {
			factor = 0.9
			stability = 3
		}
		modifier = {
			factor = 0.9
			DIP = 3
		}
		modifier = {
			factor = 0.9
			DIP = 4
		}
		modifier = {
			factor = 0.9
			DIP = 5
		}
		modifier = {
			factor = 0.9
			DIP = 6
		}
		modifier = {
			factor = 0.9
			prestige = 20
		}
		modifier = {
			factor = 0.9
			prestige = 40
		}
		modifier = {
			factor = 1.25
			is_female = yes
		}
	}

	option = {
		name = "OPT.VERYGOOD"
		add_country_modifier = { name = "newly_promoted" duration = 3000 }
		set_government_rank = 6
	}
}



# Removed pending reviewal of the feature

# tier_change.71-99 - special cases
# tier_change.71	- New Japan becomes an empire
# tier_change.72	- New Yavapeh becomes an empire
# tier_change.73	- New Gupta nation becomes an empire
# tier_change.74	- Prussia formed as a feudal duchy
# tier_change.75	- Prussia formed as an absolute monarchy

