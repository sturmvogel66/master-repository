#China
red_turban_rebel = { # GG - requires TM reactivity ?
	potential = {
		has_country_flag = red_turban_reb
		NOT = { num_of_cities = 50 }
	}
	trigger = {
		OR = {
			any_country = {
				war_with = ROOT
				OR = {
					has_country_modifier = the_mandate_of_heaven_lost
					has_country_flag = lost_mandate_of_heaven
					has_country_flag = red_turban_reb
				}
			}
			any_country = {
				truce_with = ROOT
				OR = {
					has_country_modifier = the_mandate_of_heaven_lost
					has_country_flag = lost_mandate_of_heaven
					has_country_flag = red_turban_reb
				}
			}
		}
	}
	#global_regiment_cost = -0.50
	global_regiment_recruit_speed = -0.50
	#land_morale = 0.30
	#stability_cost_modifier = -0.5
	#discipline = 0.10
	land_forcelimit_modifier = 1.0
	global_manpower = 10
	global_manpower_modifier = 2
	manpower_recovery_speed = 0.50
	global_unrest = -5
	war_exhaustion = -0.2
	war_exhaustion_cost = -1
	technology_cost = 1
	idea_cost = 1
	trade_efficiency = -0.4
	diplomatic_reputation = -3
	build_cost = 0.5
}
