extended_peacetime_officer_commissions = {

	category = 1

	allow = {
		mil_tech = 22 # WAS 16
		dip_tech = 22
	}
	
	effect = {
		
	}
	
	modifier = {
		navy_tradition_decay = -0.01
		army_tradition_decay = -0.01
	}

	ai_will_do = {
		factor = 1	
	}	
}