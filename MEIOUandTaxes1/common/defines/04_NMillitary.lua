
NDefines.NMilitary.BASE_MP_TO_MANPOWER = 1.00		-- 1000 men per manpower
NDefines.NMilitary.SIEGE_ATTRITION = 2
NDefines.NMilitary.HEAVY_SHIP_SAILORS_COST = 500				-- 1 = 1000 men in cost.
NDefines.NMilitary.LIGHT_SHIP_SAILORS_COST = 100				-- 1 = 1000 men in cost.
NDefines.NMilitary.GALLEY_SHIP_SAILORS_COST = 200				-- 1 = 1000 men in cost.
NDefines.NMilitary.TRANSPORT_SHIP_SAILORS_COST = 25				-- 1 = 1000 men in cost.
NDefines.NMilitary.MAX_MANPOWER = 25 								-- years in mp pool
NDefines.NMilitary.MIN_MONTHLY_MANPOWER = 0.075					-- 150 men/month is minimum    # DO MODIFIED FROM .15
NDefines.NMilitary.INFANTRY_COST = 14.0 						-- _MDEF_INFANTRY_COST = 10	
NDefines.NMilitary.CAVALRY_COST = 22.0 							-- _MDEF_CAVALRY_COST = 10	
NDefines.NMilitary.ARTILLERY_COST = 32.0 						-- _MDEF_ARTILLERY_COST = 10	
NDefines.NMilitary.FORTRESS_COST = 0.3							-- monthly cost per fortlevel.
NDefines.NMilitary.HEAVY_SHIP_COST = 60 						-- _MDEF_HEAVY_SHIP_COST = 10	
NDefines.NMilitary.LIGHT_SHIP_COST = 34 						-- _MDEF_LIGHT_SHIP_COST = 10
NDefines.NMilitary.GALLEY_COST = 20 							-- _MDEF_GALLEY_COST = 10	
NDefines.NMilitary.TRANSPORT_COST = 30							-- _MDEF_TRANSPORT_COST = 10	
NDefines.NMilitary.HEAVY_SHIP_TIME = 730 						-- _MDEF_HEAVY_SHIP_TIME = 10	
NDefines.NMilitary.LIGHT_SHIP_TIME = 545 						-- _MDEF_LIGHT_SHIP_TIME = 10
NDefines.NMilitary.GALLEY_TIME = 365 							-- _MDEF_GALLEY_TIME = 10	
NDefines.NMilitary.TRANSPORT_TIME = 275 						-- _MDEF_TRANSPORT_TIME = 10	
NDefines.NMilitary.EXTRA_LAND_REINFORCE_COST = 1.50				-- extra cost for reinforcing land units (as a multiplier of maintenance).
NDefines.NMilitary.TRADITION_GAIN_LAND = 15						-- Tradition gain base value from land combat.
NDefines.NMilitary.TRADITION_GAIN_NAVAL = 30					-- Tradition gain base value from naval combat.  
NDefines.NMilitary.BASE_COMBAT_WIDTH = 9.0						-- _MDEF_BASE_COMBAT_WIDTH_
NDefines.NMilitary.LOOTED_DAYS = 730							-- Time the "Looted" static modifier lasts.
NDefines.NMilitary.LOOTED_SCALE = 2								-- Scaling value from province development to number of ducats it contains.
NDefines.NMilitary.LOOTED_MAX = 5								-- Maximum amount of ducats (total) a province can be looted for each month.
NDefines.NMilitary.LOOTED_RECOVERY = 0.05						-- Speed at which a looted province recovers in fractional units per month (multiply by 100 for percentage).
NDefines.NMilitary.NAVAL_SUPPLY_RANGE = 120 					-- Supply range for ships.
NDefines.NMilitary.DEFAULT_WARGOAL_BATTLESCORE_BONUS = 8		-- Battle score bonus from winning battles
NDefines.NMilitary.NOMAD_LOOT_TRADITION = 0.005 				-- _MDEF_NOMAD_LOOT_TRADITION_;Military tradition a horde gets from looting territory
NDefines.NMilitary.SUPPLYLIMIT_BASE_MULTIPLIER = 1.0 			-- 
NDefines.NMilitary.WAR_LENGTH_DAMAGE_MODIFIER = 0.02			-- How much (by percentage) the damage dealt will be increased each day of the combat
NDefines.NMilitary.SHATTERED_RETREAT_SPEED_MODIFIER = 0.25		-- How much (by percentage) the movement speed will be modified when doing a shattered retreat
NDefines.NMilitary.OVERRUN_FACTOR_CANNOT_LEAVE = 2				-- Same as OVERRUN_FACTOR, but when combat cannot be left by the combattants. (integer)
NDefines.NMilitary.BLOCKADE_FACTOR = 3							-- (Total sail speed / blockade_factor) * blockade_efficiency / province base tax
NDefines.NMilitary.REBEL_LEADER_POWER = 18						-- The higher this value the more pips rebel leaders will have on average
NDefines.NMilitary.PRESTIGE_FROM_LAND = 5						-- Maximum base prestige from land battles (unmodified).
NDefines.NMilitary.PRESTIGE_FROM_NAVAL = 5						-- Maximum base prestige from naval battles (unmodified).
NDefines.NMilitary.MERCENARY_SUPPORT_LIMIT_BASE = 2						-- Base (and thus minimum) value for mercenary support limit.
NDefines.NMilitary.MERCENARY_SUPPORT_LIMIT_FRACTION = 0.3					-- Fraction of regular army support/force limit that goes into mercenary support limit.

NDefines.NMilitary.FORT_PER_DEV_RATIO = 100						-- 1 non-obsolete maintained fort per this amount of development = +1 AT
