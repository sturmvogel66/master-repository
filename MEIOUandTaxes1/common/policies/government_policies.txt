
recruitment_drive_act = {
	monarch_power = MIL
	
	potential = {
		mil_tech = 10
	}
	
	allow = {
		OR = {
			mil = 4
			advisor = army_reformer
			advisor = army_organiser
			advisor = recruitmaster
		}
	}	
	
	manpower_recovery_speed = 0.10
	
	ai_will_do = {
		factor = 0
	}
}

#######################################
#           Policy Sliders            #
#######################################
# System changed to decisions and policies
