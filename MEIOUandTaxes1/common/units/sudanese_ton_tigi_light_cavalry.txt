#0 - Clan Cavalry

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 2
defensive_morale = 2
offensive_fire = 0
defensive_fire = 0
offensive_shock = 1
defensive_shock = 1

trigger = {
	any_owned_province = { #Tsetse flies and jungle
		NOT = { region = central_africa_region }
		NOT = { region = kongo_region }
		NOT = { region = south_africa_region }
		NOT = { region = east_africa_region }
		}
	}
