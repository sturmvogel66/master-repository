#20 - Barque - Pattamar
type = transport

hull_size = 11 #250-700 tons, most around 300 tons
base_cannons = 4
sail_speed = 6 #Best guess

sailors = 25

sprite_level = 3

trigger = { 
	OR = {
		technology_group = indian
		technology_group = hawaii_tech
		}
	is_colonial_nation = no
	}