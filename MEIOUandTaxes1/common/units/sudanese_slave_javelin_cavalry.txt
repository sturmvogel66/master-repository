#34 - Javelineers

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 6
defensive_morale = 5
offensive_fire = 0
defensive_fire = 1
offensive_shock = 8
defensive_shock = 3

trigger = {
	any_owned_province = { #Tsetse flies and jungle
		NOT = { region = central_africa_region }
		NOT = { region = kongo_region }
		NOT = { region = south_africa_region }
		NOT = { region = east_africa_region }
		}
	}
