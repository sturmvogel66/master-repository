# Mounted Archers (10)

unit_type = indian
type = cavalry
maneuver = 2

#Tech 0
offensive_morale = 3
defensive_morale = 3
offensive_fire = 0
defensive_fire = 0
offensive_shock = 3
defensive_shock = 2

trigger = { NOT = { religion_group = muslim } }
