TIB_ideas_dg = {
	start = {
		global_unrest = -1
		global_missionary_strength = 0.01
	}

	bonus = {
		advisor_cost = -0.10
	}
	trigger = {
		OR = {
			primary_culture = tibetan
			primary_culture = bhutanese
			primary_culture = sikkimese
		}	
		religion_group = taoic #DEI GRATIA 
	}
	free = yes       #will be added at load.
	
	vajrayana_buddhism  = {
		stability_cost_modifier = -0.10
	}
	bon_influence = {
		leader_land_shock = 1
	}
	yaks = {
		production_efficiency = 0.1
	}
	way_of_virtue= {
		tolerance_heretic = 1
		tolerance_heathen = 1
	}
	potala_palace = {
		diplomatic_reputation = 3
	}
	way_of_ancients = {
		discipline = 0.025
	}
	pale_earth = {
		idea_cost = -0.1
	}
}

ikki_ideas = {
	start = {
		tolerance_own = 1
		missionaries = 1
		}
	bonus = {
		stability_cost_modifier = -0.1
	}
	
	trigger = {
		religion = shinto
		government = theocracy
	}
	free = yes       #will be added at load.
	
	pure_land_of_the_west = {
		land_morale = 0.10
		infantry_cost = -0.10
		}
	ikki_leagues = {                    
		global_manpower_modifier = 0.10
		}
	fortified_temples = {
		defensiveness = 0.20
		}
	temple_towns = {            
		global_trade_goods_size_modifier = 0.1
		}
	adoption_of_firearms = {
		mil_tech_cost_modifier  = -0.1
		}
	religious_monarchy = {
		prestige = 0.5
		legitimacy = 1
		}
	peasant_leadership = {
		advisor_cost = -0.25
		}
	}
	
israeli_ideas = {
	start = {
		land_morale = 0.10
		}
	bonus = {
		legitimacy = 0.5
		prestige = 0.5
		}
	trigger = { 
		tag = ISR
		religion = jewish
		}
	free = yes #Will be added on load
	the_promised_land = {
		enemy_core_creation = 0.5
		}
	right_of_return = {
		global_manpower_modifier = 0.1
		}
	legacy_of_the_ghetto = {
		trade_efficiency = 0.15
		}
	cousins_abroad = {
		diplomats = 1
		}
	crossroads_of_empires = {
		diplomatic_reputation = 2
		}
	never_again = {
		discipline = 0.05
		}
	new_convenant = {
		tolerance_own = 1
		}
	}
