# INSTRUCTIONS:
#
# condition				A diplomatic action can have any number of condition blocks, each with its own
#						tooltip, potential and allow section
#
# 	tooltip					Sets a custom text string similar to the hardcoded limits
# 							If no tooltip is scripted, the tooltip for the actual trigger will be shown
#							Note that the custom tooltip is only shown if the allow trigger is NOT met
#
# 	potential				Determines if the trigger is applicable or not
# 	disallow				Determines if the action is valid or not
#
# effect				A diplomatic action can only have one effect block

# ROOT					actor
# FROM					target

# milaccess
milaccess = {
	condition = {
		tooltip = MILACCESS
		potential = {
			NOT = {
				army_strength = {
					who = FROM
					value = 2.0
				}
			}
		}
		allow = {
			FROM = {
				has_opinion = {
					who = ROOT
					value = 30
				}
			}
		}
	}
	condition = {
		tooltip = AMALGAMATION
		potential = {
			OR = {
				government = amalgamation_government
				FROM = { government = amalgamation_government }
			}
			NOT = { tag = CIR }
			NOT = { tag = GAZ }
			FROM = { NOT = { tag = GAZ } }
			FROM = { NOT = { tag = CIR } }
		}
		allow = {
			always = no
		}
	}
}
