
colonial_arabia = {
	color = { 77 216 89 }
	
	provinces = {
		383 388 389 394 395 396 397 400 401 402 403 404 428 465 466 467 474 2538 2539 2653 3068 3069 3070 3074 3075 4095 4096 4097 4098
		431 2235
	}
	
	# Specific
	# Generic
	names = {
		name = "TRADE_COMPANY_ARABIA_Root_GetName"
	}
	names = {
		name = "TRADE_COMPANY_ARABIA_Arabia_Trade_Company"
	}
}
