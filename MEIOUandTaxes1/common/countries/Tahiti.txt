#Country Name: Please see filename.

graphical_culture = southamericagfx

color = { 214  110   17 }

historical_idea_groups = {
	naval_ideas
	logistic_ideas
	trade_ideas
	aristocracy_ideas
	administrative_ideas
	quantity_ideas
	spy_ideas
	merchant_marine_ideas
}

historical_units = { #Polynesian group
	polynesian_skirmish_infantry
	polynesian_pike_infantry
	polynesian_elite_infantry
	polynesian_musket_infantry
	polynesian_regular_infantry
	polynesian_line_infantry
	polynesian_carabinier_cavalry
	polynesian_drill_infantry
	polynesian_hunter_cavalry
	polynesian_impulse_infantry
	polynesian_lancer_cavalry
	polynesian_breech_infantry
}

monarch_names = { # Most Monarchs were named with "Nobu"
	"Nobuharu #1" = 50
	"Nobunari #1" = 50
	"Nobutake #1" = 50
	"Nobumune #1" = 50
	"Tokitsuna #1" = 50	
	"Nobutsuna #1" = 50	
	"Nobutoki #1" = 50	
	"Nobumasa #1" = 50	
	"Nobumitsu #1" = 50
	"Nobuyoshi #1" = 50
	"Nobumoto #0" = 50
	"Nobumitsu #0" = 50
	"Nobutaka #0" = 20
	"Nobushige #0" = 20
	"Nobumori #0" = 50
	"Nobunawa #0" = 50
    "Nobutora #0" = 50
	"Harunobu #0" = 50
	"Nobukado #0" = 50
	"Katsuyori #0" = 20	
	"Nobukatsu #0" = 20	
	"Nobuyoshi #0" = 50	
	"Nobuaki #0" = 50
	
	"Pomare #0" = -10
	"Teriimaevarua #0" = -1
}

leader_names = {
    # Daimyo names
	Shimazu Otomo Ryuzoji Aso Ito Shoni Ouchi Kono Chosokabe Miyoshi 
	Mori Amago Yamana Akamatsu Hatakeyama Hosokawa Shiba Asakura Oda Tokugawa
	Togashi Uesugi Takeda Imagawa Tokugawa Hojo Nagao Satomi Satake Ustunomiya
	Date Mogami Ando Rokkaku Uragami Nanbu Ashina Murakami Saionji Akizuki Kimotuki
	Kasai Soma Yuki Saito Ogasawara Azai Hatano Jinbo Sagara Osaki
	Kitabatake Mimura Ichijo Toki Kyogoku Sasaki Ashikaga Isshiki Honganji
    # Takeda frevor
    Unno Nishina Katsurayama Anayama Itagaki Amari Obu Baba Kosaka Yamagata Naito Akiyama
	Hara Tsuchiya Oyamada Komai Saegusa Sone Sanada Obata Yokota Hara Tada Kiso
}

ship_names = {
	"Mutsu Maru" "Dewa Maru" "Shimotsuke Maru"
	"Hitachi Maru"
	"Kazusa Maru" "Shimousa Maru" "Awa Maru" "Musashi Maru" "Kozuke Maru" "Izu Maru" "Echigo Maru" "Ettchu Maru"
	"Kaga Maru" "Noto Maru" "Suruga Maru" "Totomi Maru" "Joshu Maru" "Enshu Maru" "Bushu Maru" "Esshu Maru"
	"Mikawa Maru" "Ushu Maru" "Soushu Maru" "Boushu Maru" "Kai Maru" "Shinshu Maru" "Koshu Maru"
	"Owari Maru" "Bishu Maru" "Mino Maru" "Echizen Maru" "Ise Maru" "Shima Maru" "Omi Maru" 
	"Goushu Maru" "Yamashiro Maru" "Wakasa Maru" "Tanba Maru" "Tango Maru" "Yamato Maru" "Kii Maru"
	"Kishu Maru" "Iga Maru" "Sanuki Maru" "Tosa Maru"
	"Iyo Maru" "Harima Maru" "Banshu Maru"
	"Hoki Maru" "Inaba Maru" "Bizen Maru" "Mimasaka Maru" "Binshu Maru"
	"Bingo Maru" "Bittchu Maru" "Aki Maru" "Izumo Maru" "Iwami Maru" "Suou Maru" "Nagato Maru" "Bungo Maru"
	"Buzen Maru" "Chikuzen Maru"
	"Chikugo Maru" "Hizen Maru" "Higo Maru" "Chikushu Maru" "Hyuga Maru" "Osumi Maru" "Satsuma Maru" "Sasshu Maru"
}

army_names = {
	"$PROVINCE$ Gun" "$PROVINCE$ Zei"  "$PROVINCE$ Dan" 
}

fleet_names = {
	"$PROVINCE$ Suigun" "$PROVINCE$ Sendan" "$PROVINCE$ Tai" 
}