enforce_caste_system = {
	if = {
		limit = { 
			NOT = { has_country_flag = caste_system_harsh } 
			NOT = { has_country_modifier = caste_system_changing }
			}
		custom_tooltip = stricter_caste_system
		hidden_effect = {
			change_variable = { which = caste_strictness value = 1 }
			if = {
				limit = { check_variable = { which = caste_strictness value = 3 } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = stricter_caste
					}
				}
			if = {
				limit = { check_variable = { which = caste_strictness value = 6 } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = stricter_caste
					}
				}
			if = {
				limit = { check_variable = { which = caste_strictness value = 9 } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = stricter_caste
					}
				}
			if = {
				limit = { check_variable = { which = caste_strictness value = 12 } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = stricter_caste
					}
				}
			if = {
				limit = { check_variable = { which = caste_strictness value = 15 } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = stricter_caste
					}
				}
			if = {
				limit = { has_country_flag = stricter_caste }
				clr_country_flag = stricter_caste
				add_stability = 1 change_variable = { which = "national_stability" value = 1 }
				add_country_modifier = { name = caste_system_changing duration = 5000 }
				if = {
					limit = { 
						NOT = { has_country_flag = caste_system_relaxed }
						NOT = { has_country_flag = caste_system_loose }
						NOT = { has_country_flag = caste_system_harsh }
						}
					set_country_flag = caste_system_harsh
					}
				if = {
					limit = { has_country_flag = caste_system_loose }
					clr_country_flag = caste_system_loose
					}
				if = {
					limit = { has_country_flag = caste_system_relaxed }
					clr_country_flag = caste_system_relaxed
					set_country_flag = caste_system_loose
					}
				}
			}
		}
	}
	
relax_caste_system = {
	if = {
		limit = { 
			NOT = { has_country_flag = caste_system_relaxed } 
			NOT = { has_country_flag = caste_system_loose }
			NOT = { has_country_modifier = caste_system_changing }
			}
		custom_tooltip = looser_caste_system
		hidden_effect = {
			change_variable = { which = caste_strictness value = -1 }
			if = {
				limit = { NOT = { check_variable = { which = caste_strictness value = -2 } } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = looser_caste
					}
				}
			if = {
				limit = { NOT = { check_variable = { which = caste_strictness value = -5 } } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = looser_caste
					}
				}
			if = {
				limit = { NOT = { check_variable = { which = caste_strictness value = -8 } } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = looser_caste
					}
				}
			if = {
				limit = { NOT = { check_variable = { which = caste_strictness value = -11 } } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = looser_caste
					}
				}
			if = {
				limit = { NOT = { check_variable = { which = caste_strictness value = -14 } } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = looser_caste
					}
				}
			if = {
				limit = { has_country_flag = looser_caste }
				clr_country_flag = looser_caste
				add_stability = -1 subtract_variable = { which = "national_stability" value = 1 }
				add_country_modifier = { name = caste_system_changing duration = 5000 }
				if = {
					limit = { has_country_flag = caste_system_loose }
					clr_country_flag = caste_system_loose
					set_country_flag = caste_system_relaxed
					}
				if = {
					limit = { 
						NOT = { has_country_flag = caste_system_relaxed }
						NOT = { has_country_flag = caste_system_loose }
						NOT = { has_country_flag = caste_system_harsh }
						}
					set_country_flag = caste_system_loose
					}
				if = {
					limit = { has_country_flag = caste_system_harsh }
					clr_country_flag = caste_system_harsh
					}
				}
			}
		}
	}
	
oppose_caste_system = {
	if = {
		limit = { 
			NOT = { has_country_flag = caste_system_relaxed } 
			NOT = { has_country_modifier = caste_system_changing }
			}
		custom_tooltip = oppose_caste_system
		hidden_effect = {
			change_variable = { which = caste_strictness value = -2 }
			if = {
				limit = { NOT = { check_variable = { which = caste_strictness value = -2 } } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = looser_caste
					}
				}
			if = {
				limit = { NOT = { check_variable = { which = caste_strictness value = -5 } } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = looser_caste
					}
				}
			if = {
				limit = { NOT = { check_variable = { which = caste_strictness value = -8 } } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = looser_caste
					}
				}
			if = {
				limit = { NOT = { check_variable = { which = caste_strictness value = -11 } } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = looser_caste
					}
				}
			if = {
				limit = { NOT = { check_variable = { which = caste_strictness value = -14 } } }
				random = {
					chance = 10
					set_variable = { which = caste_strictness value = 0 }
					set_country_flag = looser_caste
					}
				}
			if = {
				limit = { has_country_flag = looser_caste }
				clr_country_flag = looser_caste
				add_stability = -2 subtract_variable = { which = "national_stability" value = 2 }
				add_country_modifier = { name = caste_system_changing duration = 5000 }
				if = {
					limit = { has_country_flag = caste_system_loose }
					clr_country_flag = caste_system_loose
					set_country_flag = caste_system_relaxed
					}
				if = {
					limit = { 
						NOT = { has_country_flag = caste_system_relaxed }
						NOT = { has_country_flag = caste_system_loose }
						NOT = { has_country_flag = caste_system_harsh }
						}
					set_country_flag = caste_system_loose
					}
				if = {
					limit = { has_country_flag = caste_system_harsh }
					clr_country_flag = caste_system_harsh
					}
				}
			}
		}
	}

	
