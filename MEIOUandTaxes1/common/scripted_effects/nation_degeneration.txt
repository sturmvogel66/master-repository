yua_degeneration = {
		change_government = steppe_horde
		remove_faction = temples
		remove_faction = enuchs
		remove_faction = bureaucrats
		change_technology_group = steppestech
		change_unit_type = steppestech
		swap_free_idea_group = yes
		remove_country_modifier = wrath_of_god
		clr_country_flag = mandate_of_heaven_claimed
		add_prestige = -15
		add_legitimacy = -5	
		set_variable = { which = "military_decay_variable" value = 0 }
}
