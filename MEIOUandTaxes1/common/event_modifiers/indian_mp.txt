indian_mp1 = {
	infantry_cost = -0.10
	global_regiment_recruit_speed = -0.050
	land_forcelimit_modifier = 0.5
	global_manpower = 10
	global_manpower_modifier = 1
	fort_maintenance_modifier = -0.5
	infantry_power = -0.15
}

indian_mp2 = {
	infantry_cost = -0.10
	global_regiment_recruit_speed = -0.050
	land_forcelimit_modifier = 0.5
	global_manpower = 7.5
	global_manpower_modifier = 0.75
	fort_maintenance_modifier = -0.5
	infantry_power = -0.15
}

indian_mp3 = {
	infantry_cost = -0.10
	global_regiment_recruit_speed = -0.050
	land_forcelimit_modifier = 0.5
	global_manpower = 5
	global_manpower_modifier = 0.5
	fort_maintenance_modifier = -0.5
	infantry_power = -0.15
}

indian_mp4 = {
	infantry_cost = -0.10
	global_regiment_recruit_speed = -0.050
	land_forcelimit_modifier = 0.5
	global_manpower = 2.5
	global_manpower_modifier = 0.25
	fort_maintenance_modifier = -0.5
	infantry_power = -0.15
}

indian_manpower = {
	infantry_cost = -0.10
	global_regiment_recruit_speed = -0.050
	land_forcelimit_modifier = 0.5
	global_manpower = 1.0
	global_manpower_modifier = 0.1
	fort_maintenance_modifier = -0.5
	infantry_power = -0.15
}
