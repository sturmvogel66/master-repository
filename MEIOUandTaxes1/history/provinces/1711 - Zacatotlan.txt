# 1711 - Coalcoman
# GG - 22/07/2008
# ENR : changed the name followiwng the addition of prov3527

owner = CLM
controller = CLM
add_core = CLM
culture = tecos
religion = nahuatl
capital = "Coalcoman" 

base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
citysize = 7000
trade_goods = fish 


hre = no

discovered_by = mesoamerican

1522.1.1   = {
	discovered_by = SPA
}
1530.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	religion = catholic
	naval_arsenal = yes
	marketplace = yes
	bailiff = yes
}
1542.1.1   = {
	owner = SPA
	controller = SPA
	capital = "Coalcomán de Vázquez Pallares"
	citysize = 2500
	culture = castillian
	religion = catholic
	base_tax = 2
base_production = 2
	trade_goods = tobacco
	}
1567.1.1   = {
	add_core = SPA
	citysize = 1000
}
1600.1.1   = {
	citysize = 2000
}
1650.1.1   = {
	citysize = 5000
}
1700.1.1   = {
	citysize = 10000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 20000
}
1800.1.1   = {
	citysize = 50000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cordóba
