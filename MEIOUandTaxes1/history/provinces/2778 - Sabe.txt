# 2778 - Sabe

owner = OYO
controller = OYO
culture = yorumba
religion = west_african_pagan_reformed
capital = "Sabe"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = palm
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	add_core = OYO
}
1520.1.1 = { unrest = 4 } #Benin raids and claimst to loyalty of local Yoruba chiefs
1525.1.1 = { unrest = 0 }
1585.1.1 = { unrest = 4 } #Nupe Raids
1590.1.1 = { unrest = 0 }
