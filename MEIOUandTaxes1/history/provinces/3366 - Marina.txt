# 3366 - Onteniente + Alcoy

owner = ARA
controller = ARA
culture = valencian
religion = catholic 
hre = no
base_tax = 6
base_production = 6
trade_goods = fish
base_manpower = 2
is_city = yes 

capital = "Alcoy"			
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = ARA
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no paved_road_network = yes 
	bailiff = yes
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1713.4.11  = {
	remove_core = CAS
}
1808.6.6   = {
	controller = REB
}
1811.1.1   = {
	controller = SPA
}
1812.10.1  = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
