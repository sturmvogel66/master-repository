# 3737 - Schwarzburg

owner = SCH
controller = SCH
culture = high_saxon
religion = catholic
capital = "Schwarzburg"
trade_goods = wheat
hre = yes
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
add_core = SCH
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1133.1.1 = { mill = yes }
1200.1.1 = { road_network = yes }
1530.1.4  = {
	bailiff = yes	
}
1540.1.1   = {
	religion = protestant
}
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
