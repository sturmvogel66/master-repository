# No previous file for Pasquotank

culture = powhatan
religion = totemism
capital = "Pasquotank"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 35
native_ferocity = 3
native_hostileness = 4

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1525.1.1 = {
	discovered_by = FRA
} # Giovanni da Verrazzano
1607.1.1  = {
	owner = ENG
	controller = ENG
	capital = "Jamestown"
	citysize = 500
	religion = protestant #anglican
	culture = english
	trade_goods = tobacco
	base_tax = 3
base_production = 3
#	base_manpower = 1.5
	base_manpower = 3.0
	set_province_flag = trade_good_set
	}
1632.1.1   = {
	add_core = ENG
	is_city = yes
}
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
    	remove_core = ENG
}
1750.1.1   = {
	add_core = USA
	culture = american
}
1775.4.19  = {
	spawn_rebels = { type = nationalist_rebels size = 3 win = yes friend = USA }
	unrest = 6
} # Battles of Lexington and Concord
1776.7.4  = {
	revolt = { }
	unrest = 0
	owner = USA
	controller = USA
	religion = protestant
} # Declaration of Independance
1782.9.2   = {
	remove_core = GBR 
} # Acknowledgement of "Thirteen United States", not the Colonies
