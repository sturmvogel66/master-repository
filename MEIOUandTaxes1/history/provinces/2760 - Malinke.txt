# 2760 - Malink�

owner = ASH
controller = ASH
culture = akaa
religion = west_african_pagan_reformed
capital = "Touba"
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
trade_goods = slaves
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = ASH
}
