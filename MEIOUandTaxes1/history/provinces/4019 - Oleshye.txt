# 4019 - Oleshye

owner = WHI
controller = WHI
culture = crimean
religion = sunni
capital = "Oleshye"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = wool
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
hre = no

1000.1.1   = {
	add_permanent_province_modifier = {
		name = dnieper_estuary_modifier
		duration = -1
	}
}
1200.1.1 = { road_network = yes }
1356.1.1 = {
	unrest = 3
	add_core = WHI
	add_core = CRI
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
	unrest = 0
}
1427.1.1 = {
	owner = CRI
	controller = CRI
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
}
#1449.1.1 = {
#	owner = LIT
#	controller = LIT
#	add_core = LIT
#	remove_core = CRI
#}
#1475.1.1 = {
#	add_core = TUR
#}
1502.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1515.1.1 = { training_fields = yes }
1774.7.21 = {
	add_core = RUS
	remove_core = CRI
} # Treaty of Kuchuk-Kainarji
1783.1.1 = {
	owner = RUS
	controller = RUS
}
