# 3359 - Hoki

owner = YMN
controller = YMN
culture = chugoku
religion = mahayana
capital = "Yonago"
trade_goods = tobacco
hre = no
is_city = yes # citysize = 1500
base_tax = 2
base_production = 2
base_manpower = 4
temple = yes

discovered_by = chinese

1356.1.1 = {
	add_core = YMN
}
1542.1.1 = { discovered_by = POR }
1552.1.1 = {
	add_core = ANG
	owner = ANG
	controller =ANG
}
1582.1.1 = {
	owner = ODA
	controller = ODA
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
