# 2831 - Klodzko

owner = BOH
controller = BOH
capital = "Klodzko"
culture = silesian
religion = catholic
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
trade_goods = beer
discovered_by = western
discovered_by = eastern
discovered_by = muslim
hre = yes

1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = BOH
	add_core = KLO
	add_permanent_province_modifier = {
		name = bohemian_estates
		duration = -1
	}
}
1419.8.16 = {
#	owner = HUN
#	controller = HUN
	add_core = HUN
}
1437.12.9 = {
#	owner = BOH
#	controller = BOH
	remove_core = HUN
}
1459.1.1   = {
	owner = KLO
	controller = KLO
	add_core = KLO
}
1492.9.21   = {
	owner = OLE
	controller = OLE
	add_core = OLE
}
#1500.1.1 = { road_network = yes }
1501.1.1   = {
	owner = BOH
	controller = BOH
	remove_core = OLE
}
1523.1.1   = { religion = reformed }
1526.8.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
} #Battle of Mohac where Lajos II dies -> Habsburg succession

1530.1.4  = {
	bailiff = yes	
}
1614.1.1   = { 
	owner = HAB
	controller = HAB
}
1618.1.1   = { unrest = 5 }
1619.3.1  = {
	revolt = { }
#	owner = PAL
#	controller = PAL
	add_core = PAL
}
1620.11.8 = {
#	owner = HAB
#	controller = HAB
	remove_core = PAL
}# Tilly beats the Winterking at White Mountain. Deus Vult!
1630.1.1   = { fort_14th = yes  }
1632.1.1   = {
	revolt = { }
	controller = PAL
}
1633.1.1   = {
	controller = HAB
}
1642.1.1   = {
	controller = SWE
}
1648.10.24 = {
	unrest = 0
	controller = HAB
} # Treaty of Westphalia, ending the Thirty Years' War
1653.1.1   = {
	owner = HAB
	add_core = HAB
	controller = HAB
}
1694.1.1   = { unrest = 4 }
1702.1.1   = { unrest = 0 }
1741.3.1   = {
	controller = PRU
} # Brilliant night attack by Beneral Prince Leopold II von Anhalt-Dessau
1742.1.1   = {
	owner = PRU
	add_core = PRU
} # Peace of Breslau after the first Silesian War
1750.1.1   = {  }
1763.1.1   = { remove_core = HAB } # End of the third Silesian War
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
