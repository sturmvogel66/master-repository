# No previous file for Yugh

culture = ket
religion = tengri_pagan_reformed
capital = "Yugh"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 50
native_ferocity = 2
native_hostileness = 1

1750.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
   	religion = orthodox
   	culture = russian
	citysize = 344
	trade_goods = fur
	capital = "Inbatskoye"
}
