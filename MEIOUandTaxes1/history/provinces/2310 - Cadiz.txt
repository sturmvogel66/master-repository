# 2310 - Cad�z

owner = GRA		# Mustapha Sa'd King of Granada
controller = GRA
add_core = GRA
culture = andalucian # culture = western_andalucian
religion = sunni
hre = no
base_tax = 10
base_production = 10
trade_goods = salt 
base_manpower = 4
capital = "Cad�z"
is_city = yes
fort_14th = yes
estate = estate_nobles
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1344.1.1  = {
	add_core = CAS
	owner = CAS
	controller = CAS
	set_province_flag = spanish_name
} # Conquest of Gibraltar by King Enrique of Castilla
1356.1.1  = {
	add_core = ENR
	add_permanent_province_modifier = {
		name = "lordship_of_sevilla"
		duration = -1
	}
}
1369.3.23  = { 
	remove_core = ENR
	remove_core = GRA
}
1499.12.1 = { unrest = 2 } # The Inquisition forces Spanish muslims to convert back to Christianity. Occasional revolts occur.
1502.2.1  = { unrest = 0 religion = catholic } # New capitulations where all the subjects of Granada are baptised and fully incorporated into the legal system of Castilla
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
	road_network = no paved_road_network = yes 
	bailiff = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille

1610.1.12 = {  } # Decree for the expulsion of the morisques in Andaluc�a, which is speedily and uneventfully performed
