# No previous file for Sukkur

owner = SND
controller = SND
culture = sindhi
religion = sunni
capital = "Sukkur"
trade_goods = cotton
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
add_local_autonomy = 25
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech
1100.1.1 = { plantations = yes }
#1180.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = SND
	fort_14th = yes
}
1362.1.1 = {
	owner = DLH
	controller = DLH
}
1388.1.1 = {
	owner = SND
	controller = SND
}
1498.1.1 = { discovered_by = POR }
1500.1.1 = { religion = sunni }
1520.7.1 = {
	controller = KAB
} # Arghuns
1521.1.1 = {
	controller = SND
	add_permanent_claim = MUG
} # Arghuns + tag change
1544.1.1 = {
	owner = BNG
	controller = BNG
} # Sur Expansionism
1545.1.1 = {
	owner = SND
	controller = SND
} # Sher Shah dies
1591.1.1 = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Integrated into Mughal Empire
1600.1.1 = {
	discovered_by = TUR
}
1739.1.1 = {
	owner = SND 
	controller = SND
	remove_core = MUG
}
1839.1.1 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
