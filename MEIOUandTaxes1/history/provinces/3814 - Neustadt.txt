# 3814 - Neustadt

owner = MEI
controller = MEI
culture = high_saxon
religion = catholic
capital = "Neustadt"
trade_goods = wheat
hre = yes
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes
add_core = MEI
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1133.1.1 = { mill = yes }
1356.1.1   = {
	add_core = REU
}
1423.6.1   = { 
	owner = SAX
	controller = SAX 
	add_core = SAX
	remove_core = MEI
} # Margraviate of Meissen inherits Saxony and becomes the Elector.
1500.1.1 = { road_network = yes }
1520.12.10 = {
	religion = protestant
}
1530.1.4  = {
	bailiff = yes	
}
1547.5.19   = { 
	owner = THU
	controller = THU
	add_core = THU
	remove_core = SAX
} #Treaty of Wittenberg
1560.1.1  = { fort_15th = yes }
1572.6.11   = { 
	owner = SWR
	controller = SWR
	add_core = SWR
	remove_core = THU
}#Division of Erfurt
1602.1.1   = { 
	owner = SWA
	controller = SWA
	add_core = SWA
	remove_core = SWR
}
1640.1.1   = {  }
1660.1.1   = { 
	owner = SAX
	controller = SAX
	add_core = SAX
	remove_core = SWA
}
1790.8.1  = { unrest = 5 } # Peasant revolt
1791.1.1  = { unrest = 0 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1815.9.6   = { 
	owner = SWR
	controller = SWR
	add_core = SWR
	remove_core = SAX
}
