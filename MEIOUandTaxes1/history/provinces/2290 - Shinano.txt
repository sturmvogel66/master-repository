# 2290 - Shinano
# GG/LS - Japanese Civil War

owner = OGA
controller = OGA
culture = chubu
religion = mahayana #shinbutsu
capital = "Matsumoto"
trade_goods = gold
hre = no
base_tax = 6
base_production = 6
base_manpower = 6
is_city = yes
discovered_by = chinese

1356.1.1 = {
	add_core = OGA
}
1542.1.1   = { discovered_by = POR }
1550.1.1   = {
	owner = TKD
	controller = TKD
}
1582.1.1 = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
