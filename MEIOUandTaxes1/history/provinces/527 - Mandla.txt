# 527 - Khandesh

owner = KHD
controller = KHD
culture = khandeshi
religion = hinduism
capital = "Burhanpur"
trade_goods = cotton
hre = no
base_tax = 10
base_production = 10
base_manpower = 6
is_city = yes

discovered_by = indian
discovered_by = muslim
discovered_by = turkishtech

1100.1.1 = { marketplace = yes }
1115.1.1 = { bailiff = yes }
1120.1.1 = { plantations = yes }
#1180.1.1 = { post_system = yes }
1199.1.1 = { road_network = yes }
1356.1.1 = { add_core = KHD
	fort_14th = yes }
1498.1.1 = { discovered_by = POR }
#1508.1.1 = {
#	owner = GUJ
##} # Conquered by the Ahmad Shahis
1530.2.3 = {
	add_permanent_claim = MUG
}
1543.1.1 = {
	owner = BNG
	controller = BNG
} # Conquered by Sher Shah Sur
1545.1.1 = {
	owner = KHD
	controller = KHD
} # Death of Sher Shah, governor declares independence
1600.4.8 = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Conquered by the Mughals
1615.1.1 = {
	controller = BAS
} # Ahmednagar Anti-Mughal Campaign
1622.1.1 = {
	controller = MUG
} # Ahmednagar Anti-Mughal Campaign
1650.4.8 = { add_core = MUG }
1712.1.1 = { add_core = HYD }	#Viceroyalty of the Deccan
1724.1.1 = {
	owner = HYD
	controller = HYD
	remove_core = MUG
} # Asif Jah declared himself Nizam-al-Mulk
1752.1.1 = {
	owner = MAR
	controller = MAR
	add_core = MAR
}
1818.6.3 = {
	owner = GBR
	controller = GBR
}
