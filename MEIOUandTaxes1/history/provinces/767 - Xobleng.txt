# 767

culture = tupinamba
religion = pantheism
capital = "Xobleng"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1
native_hostileness = 5

1514.1.1   = {
	discovered_by = POR
	add_permanent_claim = POR
} # Pedro Álvares Cabral
1526.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
	change_province_name = "Santa Catarina"
	rename_capital = "Santa Catarina"
	citysize = 100
	culture = castillian
	religion = catholic
	trade_goods = tobacco
}
1673.1.1   = {
	citysize = 1560
}
1726.1.1   = {
	change_province_name = "Vila do Desterro"
	rename_capital = "Vila do Desterro"
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
1893.1.1   = {
	change_province_name = "Florianópolis"
	rename_capital = "Florianópolis"
}
