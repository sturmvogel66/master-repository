# 224 - Huelva

owner = CAS		#Juan II of Castille
controller = CAS
add_core = CAS
culture = andalucian # culture = western_andalucian
religion = catholic
hre = no
base_tax = 8
base_production = 8
trade_goods = copper 
base_manpower = 4
is_city = yes
capital = "Huelva"
estate = estate_nobles
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

1356.1.1   = {
	set_province_flag = spanish_name
	add_core = ENR
	add_permanent_province_modifier = {
		name = "lordship_of_sevilla"
		duration = -1
	}
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1369.3.23  = { 
	remove_core = ENR
}

### 1503.1.1  = {  } # The "Casa de la Contratación" is established in Sevilla as the monarchy tries to control American trade through that port.
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no paved_road_network = yes 
	bailiff = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille


1610.1.12  = {  } # Decree for the expulsion of the morisques in Andalucía, which is speedily and uneventfully performed
1713.4.11  = { remove_core = CAS }
1730.1.1   = {  }
1808.6.6   = { controller = REB }
1813.12.11 = { controller = SPA }
