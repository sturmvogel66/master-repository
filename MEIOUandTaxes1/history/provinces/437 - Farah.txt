# 437 - Nik Shahr

owner = BAL
controller = BAL
culture = baluchi
religion = sunni
capital = "Nik Shahr"
trade_goods = wool
hre = no
base_tax = 3
base_production = 3
base_manpower = 4
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1   = {
	add_core = BAL
}
1450.1.1 = { citysize = 4000 }
1500.1.1 = { citysize = 5000 discovered_by = POR }
1550.1.1 = { citysize = 4000 }
1600.1.1 = { citysize = 6000 }
1649.1.1 = {
	controller = PER
}
1650.1.1 = {
	citysize = 3000
	owner = PER
}
1666.1.1 = {
	owner = BAL
	controller = BAL
} #Kingdom of Kalat
1747.10.1 = {
	owner = DUR
	controller = DUR
	add_core = DUR
	remove_core = MUG
} # Ahmad Shah established the Durrani empire
1758.1.1 = {
	owner = BAL
	controller = BAL
} #Kingdom of Kalat
