# 2408 - Huaxtepec
# GG - 22/07/2008

owner = HTP
controller = HTP
add_core = HTP
culture = nahuatl_c
religion = nahuatl
capital = "Oaxtepec" 

base_tax = 4
base_production = 4
#base_manpower = 1.5
base_manpower = 3.0
citysize = 5000
trade_goods = cacao


hre = no

discovered_by = mesoamerican

1428.1.1  = {
	owner = AZT
	controller = AZT
	add_core = AZT
	remove_core = TEN
	road_network = yes
}

1522.1.1 = {
	discovered_by = SPA

}
1529.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	marketplace = yes
	bailiff = yes
	courthouse = yes
} 
1540.1.1 = {
	culture = castillian
	religion = catholic
} 
1547.1.1   = {
	add_core = SPA
	citysize = 1000
}

1600.1.1   = {
	citysize = 2000
}
1650.1.1   = {
	citysize = 5000
}
1700.1.1   = {
	citysize = 10000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 20000
}
1800.1.1   = {
	citysize = 50000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba


