# 1491 - Ica

owner = NZC
controller = NZC
culture = nazca
religion = inti
capital = "Ica"
trade_goods = cotton
hre = no
base_tax = 4
base_production = 4
base_manpower = 3
is_city = yes
 
discovered_by = south_american


1356.1.1 = {
	add_core = NZC
}
1493.1.1   = {
	owner = INC
	controller = INC
	add_core = INC
	remove_core = CZC
	paved_road_network = yes
	bailiff = yes
	constable = yes
	marketplace = yes
	}
1529.1.1 = {
	owner = QUI
	controller = QUI
	add_core = CZC
	add_core = QUI
	remove_core = INC
}
1535.1.1  = {
	discovered_by = SPA
	add_core = SPA
	owner = SPA
	controller = SPA
	religion = catholic
	culture = castillian
}
1750.1.1  = {
	add_core = PEU
	culture = peruvian
}
1810.9.18  = {
	owner = PEU
	controller = PEU
}
1818.2.12  = {
	remove_core = SPA
}
