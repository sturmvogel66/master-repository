# 593 - Chaiya

owner = NST
controller = NST
culture = monic
religion = buddhism
capital = "Chaiya"

base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
trade_goods = fish
discovered_by = chinese
discovered_by = indian
discovered_by = muslim

hre = no

1356.1.1 = {
	add_core = NST
}
1467.4.8 = {
	owner = AYU
	controller = AYU
    add_core = AYU
	remove_core = NST
	unrest = 0
	culture = thai
}
1535.1.1 = { discovered_by = POR }
1767.4.8 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
	unrest = 0
}
