# 232 - Douro

owner = POR
controller = POR
culture = portugese
religion = catholic
capital = "Porto"
trade_goods = wine
hre = no
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes
fort_14th = yes
add_core = POR
discovered_by = western
discovered_by = muslim
discovered_by = eastern


1000.1.1   = {
	add_permanent_province_modifier = {
		name = douro_estuary_modifier
		duration = -1
	}
}
1088.1.1 = { dock = yes }
1250.1.1 = { temple = yes }

1357.3.17  = {
	revolt = { }
	controller = POR
	unrest = 0
} # Father and Son reconcile

1372.5.5   = { unrest = 4 } # Marriage between King Ferdinand and D. Leonor de Menezes that brought civil unrest and revolt.
1373.5.5   = { unrest = 0 } # Civil unrest repressed.

1515.1.1 = { training_fields = yes road_network = no paved_road_network = yes
	bailiff = yes }
1522.3.20 = { dock = no naval_arsenal = yes }
1580.8.25 = { controller = SPA }
1580.8.26 = { controller = POR }
1640.1.1  = { unrest = 8 } # Portugal revolt headed by John of Bragan�a
1640.12.1 = { unrest = 0 } # Portugal regained its independence & colonial possessions

1809.3.29 = { controller = FRA } # Occupied by France
1809.5.12 = { controller = POR }
1810.9.26 = { controller = FRA } # Wellington retreats into Lines of Torres Vedras fortification and Mass�na can't enter Lisboa due to the scorched earth policy
1811.1.1  = { controller = POR } # The Napoleonic army retreats from Portugal
