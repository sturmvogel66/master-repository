# 2569 - Sahyadri

owner = BAH
controller = BAH
culture = marathi
religion = hinduism
capital = "Dabhol"
trade_goods = millet
hre = no
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes

discovered_by = indian
discovered_by = muslim

1120.1.1 = { farm_estate = yes }
1356.1.1  = {
	add_core = BIJ
	add_core = BAH
}
#1380.1.1  = {
#	controller = VIJ
#	owner = VIJ
#} # Taken by Harihara II
#1469.1.1  = {
#	controller = BAH
#} # Taken by Bahmanis
#1470.1.1  = {
#	owner = BAH
#} # Taken by Bahmanis#Ceded in treaty to Mughals by Bijapur but never actually carried out. De facto under Sidhi, Maratha and Bijapur control without a clear stronger party or claim.
1490.1.1  = {
	owner = BIJ
	controller = BIJ
	add_core = BIJ
	remove_core = BAH
} # The Breakup of the Bahmani sultanate
1502.1.1  = { discovered_by = POR }
1515.12.17 = { training_fields = yes }
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	dock = yes
	road_network = yes
}
1571.1.1  = { 
	fort_15th = no
	fort_16th = yes 
} #Piram Khan, demolished the old fort and built an impregnable structure in 22 acres between A. D. 1567-1571. The fort was called Jazeere Mahroob Jazeera.
1596.2.1  = { discovered_by = NED } # Cornelis de Houtman
1600.1.1  = {discovered_by = turkishtech discovered_by = ENG discovered_by = FRA }
1659.11.30 = {
	owner = MAR
	controller = MAR
	capital = "Dabhol"
	add_core = MAR
} # battle of Pratapgarh
1686.1.1  = { controller = MUG } # Aurangzeb
1703.1.1  = { controller = MAR }
1707.5.12 = { discovered_by = GBR }
1818.6.3  = {
	owner = GBR
	controller = GBR
}
