# 3346 - Awa (kanto)

owner = USG
controller = USG
culture = kanto
religion = mahayana
capital = "Hojo"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 3
is_city = yes
discovered_by = chinese

1356.1.1 = { #held by Nanbu until it was lost during conflict
	add_core = USG
	add_core = YUK
	#Home province of Satomi ADD TAG
}
1363.1.1 = { #taken back by Uyesugi
	controller = USG
	owner = USG
}
1369.1.1 = { #held by yuki
	controller = YUK
	owner = YUK
}
1388.1.1 = {
	controller = USG
	owner = USG
}
1395.1.1 = { #held by yuki
	owner = YUK
	controller = YUK
}
1397.1.1 = { #Held by Kido (vassal of the kamakura kubo(?))
	controller = USG
	owner = USG
}
1423.1.1 = { #last recorded shugo
	controller = USG
	owner = USG
}
1490.1.1   = {
	owner = HJO
	controller = HJO
}
1542.1.1   = { discovered_by = POR }
1590.8.4 = {
	owner = ODA
	controller = ODA
} # Toyotomi Hideyoshi takes Odawara Castle, Hojo Ujimasa commits seppuku
1603.1.1   = { capital = "Mito" }
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
