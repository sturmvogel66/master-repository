# 175 Armagnac - Principal cities: Auch

owner = AMG
controller = AMG
culture = gascon
religion = catholic
capital = "Aux"
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
trade_goods = wine
add_local_autonomy = 25
estate = estate_nobles
 # La Cath�drale Sainte-Marie
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

1249.1.1 = {
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_permanent_claim = FRA
	add_core = GUY
	add_core = AMG
}
1360.10.24 = {
	owner = GUY
	controller = GUY
} # Treaty of Bretigny
1369.1.1   = {
	owner = AMG
	controller = AMG
} # Charles V's Reconquest
1475.8.29  = {
	remove_core = ENG
} # Treaty of Picquigny, ending the Hundred Year War
1488.1.1 = { temple = yes }
1527.1.1   = {
   owner = DAL
   controller = DAL
   add_core = DAL
}
1530.1.1 = {
   owner = FRA
   controller = FRA
   add_core = FRA
}
1540.1.1   = { fort_14th = yes }

1560.1.1   = { religion = reformed  }
1594.1.1   = { unrest = 10 } # 'Paris vaut bien une messe!', Henri converts to Catholicism

1598.4.13  = { unrest = 3 } # Edict of Nantes, alot more freedom to the protestants
1598.5.2   = { unrest = 0 } # Peace of Vervins, formal end to the Wars of Religion
1607.1.1   = { owner = FRA controller = FRA } # Armagnac to France proper, after Henri IV, King of France & Duke of Armagnac

1640.1.1   = {  }
1650.1.14  = { unrest = 7 } # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1651.4.1   = { unrest = 4 } # An unstable peace is concluded
1651.12.1  = { unrest = 7 } # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.10.21 = { unrest = 0 } # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1670.1.1   = { fort_15th = no fort_16th = yes }
1680.1.1   = { fort_16th = no fort_17th = yes } # Vauban's 'pointed' forts in Bayonne & Dax
1685.10.18 = { unrest = 8 } # Edict of Nantes revoked by Louis XIV
1686.1.17  = { religion = catholic  } # Dragonnard campaign succesful: region reverts back to catholicism
1689.1.1   = { unrest = 0 } # War of the Grand Alliance erupts: Louis XIV can't persue his religious policy anymore

