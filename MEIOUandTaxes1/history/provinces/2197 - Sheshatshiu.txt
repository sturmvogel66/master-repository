# No previous file for Sheshatshiu

culture = inuit
religion = animism
capital = "Sheshatshiu"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 5

1763.1.1 = {
	owner = FRA
	controller = FRA
	culture = francien
	religion = catholic
	trade_goods = fur
	citysize = 100
} # Rigolet Post
1763.2.10 = {
	owner = GBR
	controller = GBR
	    	culture = french_colonial
} # Treaty of Paris
1788.2.10 = { add_core = GBR }
1800.1.1 = { citysize = 1450 }
