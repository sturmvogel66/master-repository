# 4017 - Volkhovsky

owner = NOV
controller = NOV
culture = novgorodian
religion = orthodox
hre = no
base_tax = 4
base_production = 4
trade_goods = naval_supplies
base_manpower = 2
is_city = yes
capital = "Ladoga"
add_core = NOV
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = muslim

1478.1.14 = {
	owner = MOS
	controller = MOS
	add_core = MOS
	remove_core = NOV
	culture = russian
}
1484.1.1  = { unrest = 6 } # Massacres and deportation of leading citizens to inland Russia
1489.1.1  = { unrest = 0 }
1530.1.4  = {
	bailiff = yes	
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1598.1.1  = { unrest = 5 } # "Time of troubles"
1611.1.1  = { controller = SWE } # Occupied by the Swedes
1617.2.27 = { controller = RUS unrest = 0 }
1670.1.1  = { unrest = 8 } # Stepan Razin
1671.1.1  = { unrest = 0 } # Razin is captured
1707.1.1  = { unrest = 3 } # The Kondraty Bulavin Rebellion
1708.7.7  = { unrest = 0 } # Bulains was shot
