# 2839 - Mozyrz

owner = LIT
controller = LIT
culture = ruthenian
religion = orthodox
capital = "Mozyrz"
trade_goods = wax
hre = no
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech

1200.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = LIT
	add_permanent_province_modifier = {
		name = lithuanian_estates
		duration = -1
	}
}
1530.1.4  = {
	bailiff = yes	
}

1569.1.1   = { fort_14th = yes }
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1793.1.23  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = PLC
	culture = byelorussian
} # Second partition
1794.3.24  = { unrest = 5 } # Kosciuszko uprising, minimize the Russian influence
1794.11.16 = { unrest = 0 }
1795.10.1  = {
	add_core = LIT
	remove_core = PLC
}
