#135 - Szombathely

owner = HUN
controller = HUN  
culture = hungarian
religion = catholic
capital = "Sopron"
trade_goods = livestock
hre = no
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1200.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = HUN
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1506.1.1  = { revolt = { type = anti_tax_rebels size = 0 } controller = REB } # Szekely rebellion
1507.1.1  = { revolt = {  } controller = HUN } # Estimated
1514.4.1  = { revolt = { type = anti_tax_rebels size = 0 } controller = REB } # Peasant rebellion against Hungary's magnates
1515.1.1  = { revolt = {  } controller = HUN } # Estimated
1515.2.1 = { training_fields = yes }

1526.8.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_permanent_province_modifier = {
		name = guns_fortress
		duration = -1
	}
}
1529.10.7 = { bailiff = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1594.9.29 = { controller = TUR } # Ottoman capture of Gyor leads to domination of region
1598.3.28 = { controller = HAB } # Gyor is liberated

1685.1.1  = {
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
