# 1403 - Lika-Krbava

owner = CRO
controller = CRO
add_core = HUN
add_core = CRO
culture = croatian
religion = catholic
capital = "Senj"
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes
trade_goods = fish
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = no

1000.1.1 = {
	add_permanent_province_modifier = {
		name = minor_inland_center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
#	owner = HUN
#	controller = HUN
#	add_core = HUN
}
1300.1.1 = { road_network = yes }
1444.1.1 = {
	owner = HUN
	controller = HUN
	add_permanent_province_modifier = {
		name = croatian_kingdom
		duration = -1
	}
}	
1526.8.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1530.1.2 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1685.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
1809.10.14 = {
	controller = FRA
	owner = FRA
	add_core = FRA
	add_core = HAB
	remove_core = VEN
}#treaty of schonnbrunn
1814.4.11  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = FRA
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
