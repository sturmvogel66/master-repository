# 2629 - Coromandel

owner = GNG
controller = GNG
culture = tamil
religion = hinduism
capital = "Koodalur"
trade_goods = ebony
hre = no
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim

1250.1.1 = { temple = yes }
1356.1.1 = {
	owner = MAD
	controller = MAD
	add_core = GNG
}
1378.1.1 = {
	owner = VIJ
	controller = VIJ
	add_core = VIJ
}
1405.1.1 = { discovered_by = chinese }
1498.1.1 = { discovered_by = POR }
1530.1.1 = {
	#owner = GNG
	#controller = GNG
	add_core = GNG
	#remove_core = VIJ
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	dock = yes
	road_network = yes
}
1565.7.1 = {
	owner = GNG
	controller = GNG
	remove_core = VIJ
} # The Vijayanagar empire collapses, the Nayaks proclaimed themselves rulers
1600.1.1 = { discovered_by = ENG discovered_by = FRA discovered_by = NED }
1650.1.1 = { discovered_by = turkishtech }
1690.1.1 = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Conquered by the Mughal emperor Aurangzeb
1702.1.1 = {
	owner = MAD
	controller = MAD
} # Mughal power wanes
1707.5.12 = { discovered_by = GBR }
1710.1.1 = {
	owner = KRK
	controller = KRK
	add_core = KRK
	fort_15th = no
	fort_17th = yes
} # Nawab of Arcot / Carnatic
1724.1.1 = {
	#owner = HYD
	#controller = HYD
	#add_core = HYD
	remove_core = MUG
} # Asif Jah declared himself Nizam-al-Mulk of Hyderabad
1801.1.1 = {
	owner = GBR
	controller = GBR
}
1826.1.1 = { add_core = GBR }
