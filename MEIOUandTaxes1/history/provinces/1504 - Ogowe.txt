#1504 - Ogow�

culture = bakongo
religion = animism
capital = "Ogow�"
trade_goods = unknown # slaves
hre = no
native_size = 7
native_ferocity = 6
native_hostileness = 7
discovered_by = soudantech
discovered_by = sub_saharan
discovered_by = central_african

1483.1.1 = { discovered_by = POR } # Diogo Cao
