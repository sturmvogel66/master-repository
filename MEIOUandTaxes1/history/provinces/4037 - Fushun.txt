# No previous file for Fushun

owner = YUA
controller = YUA
culture = jurchen
religion = tengri_pagan_reformed
capital = "Fushun"
trade_goods = wheat
hre = no
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1355.1.1  = {
	add_core = YUA
	add_core = MXI
}
1387.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = YUA
}
1530.1.1 = { 
	marketplace = yes
	bailiff = yes
	mill = yes
}
1619.4.29 = {
	owner = JIN
	controller = JIN
	culture = jurchen
	add_core = JIN
	remove_core = MXI
}
1635.1.1 = {
	culture = manchu
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = JIN
	remove_core = MNG
} # The Qing Dynasty
