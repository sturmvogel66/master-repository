# 2796 - Omi

owner = KYO
controller = KYO
culture = kansai
religion = mahayana #shinbutsu
capital = "Otsu"
trade_goods = tea
hre = no
base_tax = 10
base_production = 10
base_manpower = 6
is_city = yes


discovered_by = chinese

1356.1.1 = {
	add_core = KYO
}
1542.1.1   = { discovered_by = POR }
1572.1.1   = {
	owner = ODA
	controller = ODA
}
1583.1.1   = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
