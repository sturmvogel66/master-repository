# No previous file for Zaskar

owner = KSH
controller = KSH
culture = kashmiri
religion = hinduism
capital = "Doda"
trade_goods = wheat
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
add_local_autonomy = 25


1000.1.1 = {
	add_permanent_province_modifier = {
		name = chamba_state
		duration = -1
	}
}
1101.1.1 = { plantations = yes }
1356.1.1  = {
	add_core = KSH
}
1400.1.1  = { religion = sunni }
1589.7.28 = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Integrated into the Mughal Empire
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1738.1.1  = {
	controller = PER
} # Captured by Persia, Nadir Shah
1739.5.1  = {
	owner = PER
} # Captured by Persia, Nadir Shah
1747.6.1  = {
	owner = DUR
	controller = DUR
	add_core = DUR
	remove_core = MUG
	unrest = 2
}
1755.1.1  = {
	owner = KSH
	controller = KSH
} # Governor declares independence
1762.1.1  = {
	owner = DUR
	controller = DUR
} # Revolt beaten
1799.1.1 = {
	owner = PUN
	controller = PUN
}
1849.3.30 = {
	owner = GBR
	controller = GBR
} # End of the Second Anglo-Sikh War
