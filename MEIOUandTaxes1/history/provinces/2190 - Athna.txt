# No previous file for Athna

culture = tanaina
religion = totemism
capital = "Athna"
trade_goods = unknown
hre = no
base_tax = 0
base_production = 0
base_manpower = 0
native_size = 5
native_ferocity = 3
native_hostileness = 8

1741.1.1 = { discovered_by = RUS } # Vitus Bering
1750.1.1 = { base_tax = 1 }
1778.1.1 = { discovered_by = GBR } # James Cook
1784.1.1 = {	owner = RUS
		controller = RUS
		citysize = 580
		culture = russian
		religion = orthodox
		trade_goods = fur
	   }
1786.1.1 = { discovered_by = FRA } # Jean Francois la Perouse
1791.1.1 = { discovered_by = SPA } # Alessandro Malaspina
1809.1.1 = { add_core = RUS }
1867.1.1  = {
	owner = USA
	controller = USA
}
