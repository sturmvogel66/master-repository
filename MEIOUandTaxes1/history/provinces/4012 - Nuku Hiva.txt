# 4012 - Nuku Hiva, Marquesas Islands

culture = polynesian
religion = polynesian_religion
capital = "Nuku Hiva"
trade_goods = unknown # fish
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 2
native_hostileness = 9

1521.1.1 = {
	discovered_by = POR
} # Discovered by Fern�o de Magalh�es
1616.1.1 = {
	discovered_by = NED
} # Visited by Willem Schouten
1765.1.1 = {
	discovered_by = GBR
} # Visited by John Byron
1768.1.1 = {
	discovered_by = FRA
} # Visited by Louis Antoine de Bougainville
