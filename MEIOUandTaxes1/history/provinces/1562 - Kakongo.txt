#1562 - Kakongo

owner = TYO
controller = TYO
culture = bakongo
religion = animism
capital = "Kakongo"
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes
trade_goods = copper
discovered_by = central_african
hre = no

1356.1.1   = {
	add_core = TYO
}
