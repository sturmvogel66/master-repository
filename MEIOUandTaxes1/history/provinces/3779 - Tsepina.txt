# 3779 - Melnik

owner = BUL
controller = BUL
culture = bulgarian
religion = orthodox
capital = "Melnik"
trade_goods = wheat
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

#1133.1.1 = { mill = yes }
1356.1.1  = {
	add_core = BUL
	add_permanent_claim = BYZ
}
1371.2.17 = {
	owner = TAR
	controller = TAR
	add_core = TAR
	remove_core = BUL
}
1371.9.27 = {
	owner = OTT
	controller = OTT
	add_core = OTT
	capital = "Menlik"
	change_province_name = "Menlik"
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1393.7.17 = {
	add_core = BUL
	remove_core = TAR
}
1400.1.1 = { remove_core = BYZ add_permanent_claim = BYZ }
1515.2.1 = { training_fields = yes }
1523.8.16 = { mill = yes }
1530.1.4  = {
	bailiff = yes	
}
1598.1.1  = {
	controller = REB
} # First Tarnovo Uprising
1600.1.1  = {
	controller = TUR
}
1686.1.1  = {
	revolt = { type = nationalist_rebels size = 1 }
	controller = REB
} # Second Tarnovo Uprising
1690.1.1  = {
	revolt = { }
	controller = TUR
}
