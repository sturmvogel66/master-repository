# 938 - Enok
# GG - 22/07/2008

culture = powhatan
religion = totemism
capital = "Enok"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 50
native_ferocity = 2
native_hostileness = 5
discovered_by = north_american

1525.1.1   = {
	discovered_by = FRA
} # Giovanni da Verrazzano
1720.1.1   = {
	owner = GBR
	controller = GBR
	capital = "Witmington"
	citysize = 500
	religion = protestant #anglican
	culture = english
	trade_goods = cotton
	base_tax = 3
base_production = 3
#	base_manpower = 1.5
	base_manpower = 3.0
	set_province_flag = trade_good_set
	}
1745.1.1   = {
	add_core = GBR
	citysize = 2000
}
1750.1.1   = {
	add_core = USA
	culture = american
	citysize = 3000
}
1775.4.19  = {
	spawn_rebels = { type = nationalist_rebels size = 3 win = yes friend = USA }
	unrest = 6
} # Battles of Lexington and Concord
1776.7.4  = {
	revolt = { }
	unrest = 0
	owner = USA
	controller = USA
	religion = protestant
} # Declaration of Independance
1782.9.2   = {
	remove_core = GBR 
} # Acknowledgement of "Thirteen United States", not the Colonies
