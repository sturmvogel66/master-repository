# 2673 - Karaikal

owner = TNJ
controller = TNJ
culture = tamil
religion = hinduism
capital = "Thanjavur"
trade_goods = cloth
hre = no
base_tax = 10
base_production = 10
base_manpower = 4
is_city = yes
# 
discovered_by = indian
discovered_by = muslim

1250.1.1 = { temple = yes }
1356.1.1 = {
	owner = MAD
	controller = MAD
	add_core = TNJ
	fort_14th = yes
}
1378.1.1 = {
	owner = VIJ
	controller = VIJ
}
1405.1.1 = { discovered_by = chinese }
1428.1.1 = { add_core = VIJ }
1498.1.1 = { discovered_by = POR }
1530.1.1 = {
	#owner = TNJ
	#controller = TNJ
	add_core = TNJ
	#remove_core = VIJ
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	dock = yes
	road_network = yes
}
1565.7.1 = {
	owner = TNJ
	controller = TNJ
} # The Vijayanagar empire collapses
1661.1.1 = {
	controller = BIJ
} # Bijapur Expansion
1662.1.1 = {
	owner = BIJ
} # Bijapur Expansion
1678.1.1 = {
	owner = MAR
	controller = MAR
} #Venkaji assumes independence
1694.1.1 = {
	controller = MUG
}
1707.1.1 = {
	controller = MAR
}
1728.1.1 = {
	add_core = MAR
}
1801.1.1 = {
	owner = GBR
	controller = GBR
}
