# No previous file for Yaka

owner = YAK
controller = YAK
add_core = YAK
culture = yaka
religion = animism
capital = "Yaka"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = cloth
hre = no
discovered_by = central_african
