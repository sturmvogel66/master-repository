# 4008 - Saent-Malo

owner = BRI
controller = BRI
add_core = BRI
culture = breton
religion = catholic
capital = "Saint Malo"
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
trade_goods = fish

discovered_by = western
discovered_by = eastern
discovered_by = muslim
hre = no

1088.1.1 = { dock = yes }
1249.1.1 = {
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1250.1.1 = { temple = yes }
1341.4.30  = {
	owner = MNF
	controller = MNF
	add_core = MNF
	add_core = BLO
	remove_core = BRI
	add_permanent_province_modifier = { name = saint_malo_city1 duration = -1 }
	set_local_autonomy = 75
} # Jean III de Bretagne dies in Caen
1365.4.12   = {
	owner = BRI
	controller = BRI
	add_core = BRI
	remove_core = BLO
	remove_core = MNF
} # End of the Brittany war of succession with the death of Charles de Blois
1378.1.1   = {
	add_core = FRA
} 
# Charles V invades Brittany without resistance
1393.1.1   = { owner = FRA controller = FRA } # The pope give the city to the crown of France, with the support of the population of Saint-Malo
1415.1.1   = { owner = BRI controller = BRI } # The city is retaken by Brittany.
1495.1.1   = { 
	add_permanent_province_modifier = { name = saint_malo_city2 duration = -1 } 
	remove_province_modifier = saint_malo_city1
}
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1522.3.20 = { dock = no naval_arsenal = yes }
1530.8.4   = { owner = FRA controller = FRA } # Union Treaty
1588.12.1  = { unrest = 5 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1590.3.11  = {
	add_permanent_province_modifier = {
		name = saint_malo_city3
		duration = -1
	}
	remove_province_modifier = saint_malo_city2
}
1594.12.5  = {
	remove_province_modifier = saint_malo_city3
	add_permanent_province_modifier = {
		name = saint_malo_city2
		duration = -1
	}	
	unrest = 0
} # 'Paris vaut bien une messe!', Henri converts to Catholicism
