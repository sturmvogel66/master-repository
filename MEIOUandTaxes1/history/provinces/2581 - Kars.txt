# 2581 - Kars

owner = ARM
controller = ARM
culture = ge_armenian
religion = orthodox
capital = "Kars"
trade_goods = wool
hre = no
base_tax = 4
base_production = 4
base_manpower = 3
is_city = yes
discovered_by = steppestech
discovered_by = muslim
discovered_by = turkishtech
discovered_by = eastern
discovered_by = western

1356.1.1  = {
	add_core = ARM
	add_core = GEO
}
1360.1.1   = {
	owner = QAR
	controller = QAR
	add_core = QAR
}
1393.1.1   = {
	owner = TIM
	controller = TIM
}
1408.1.1 = {
	controller = QAR
}
1409.1.1 = {
	owner = QAR
}
1444.1.1 = {
	add_core = AKK
	add_core = QAR
	remove_core = GEO
}
1466.1.1  = {
	owner = SMT
	controller = SMT
	add_core = SMT
	remove_core = GEO
	remove_core = AKK
	road_network = yes
}
1530.1.1 = { add_permanent_claim = TUR } #Ruled by local Turkish Beys
1530.1.4  = {
	bailiff = yes	
	training_fields = yes	
	city_watch = yes	
}
1550.1.1 = { fort_14th = yes }
1551.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_claim = TUR
}
1600.1.1 = {
	religion = sunni
}
1658.1.1 = { controller = REB } # Revolt of Abaza Hasan Pasha
1659.1.1 = { controller = TUR }