# 518 - Kotharia/Udaipur

owner = MEW
controller = MEW
culture = mewari
religion = hinduism
capital = "Kotharia"
trade_goods = gold #Silver mines
hre = no
base_tax = 10
base_production = 10
#base_manpower = 1.5
base_manpower = 3.0
citysize = 8000
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = dungarpur_state
		duration = -1
	}
}

1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = MEW
	fort_14th = yes
}
1443.1.1 = { 
	add_permanent_province_modifier = {
		name = kumbhalgarh_fortress
		duration = -1
	}
}
1450.1.1  = { citysize = 11000 }
1460.1.1  = {
	capital = "Kumbalgarh"
	fort_14th = no fort_15th = yes
}
1500.1.1  = { citysize = 15000 }
1515.12.17 = { training_fields = yes }
1530.1.1 = { 
	add_permanent_claim = MUG
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1550.1.1  = { citysize = 18000 }
1568.1.1  = { capital = "Udaipur" citysize = 48000 } #Udaipur
1600.1.1  = { citysize = 72000 }
1650.1.1  = { citysize = 100000 }
 	#Jagannath Rai
1690.1.1  = { discovered_by = ENG }
1700.1.1  = { citysize = 67000 }
1707.5.12 = { discovered_by = GBR }
1750.1.1  = { citysize = 48000 }
1800.1.1  = { citysize = 45000 }
