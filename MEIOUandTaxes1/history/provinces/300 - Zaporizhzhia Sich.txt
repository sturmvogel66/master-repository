# 300 - Zaporizhzhia Sich

owner = WHI
controller = WHI
culture = crimean
religion = sunni
capital = "Zaporozhia"
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes
trade_goods = livestock
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
hre = no

1356.1.1 = {
	unrest = 3
	add_permanent_claim = LIT
	add_core = WHI
	add_core = CRI
	add_core = KIE
}
1362.12.25 = {
	owner = LIT
	controller = LIT
} # Aftermath of the Battle of Blue Waters
1382.1.1   = {
	add_core = GOL
	remove_core = WHI
}
1399.8.12 = {
	owner = GOL
	controller = GOL
	unrest = 0
} # Aftermath of the Battle of Vorskla River
1427.1.1 = {
	owner = CRI
	controller = CRI
	add_core = CRI
	#remove_core = CRI
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
}
1502.1.1 = {
	owner = LIT
	controller = LIT
	add_core = LIT
	remove_core = GOL
	religion = orthodox
	culture = ukrainian
} # Final destruction of the Golden Horde
1515.1.1 = { training_fields = yes }
1555.1.1  = {
	owner = ZAZ
	controller = ZAZ
	add_core = ZAZ
	remove_core = CRI
	religion = orthodox
#	culture = ukrainian
} # Fortified military camp, independent state
1654.1.1  = {
	add_core = RUS
} # Treaty of Pereyaslav
1709.7.8  = { unrest = 6 } # Persecuted by the Russians after Zaporizhia became an ally of Hetman
1734.1.1  = { unrest = 0 }
1735.2.1  = { fort_14th = yes } # Russians established new forts to control the Cossacks 
1768.1.1  = { unrest = 8 } # Kolivshchyna rebellion, peasant uprising
1769.1.1  = { unrest = 0 } # Suppressed by Polish and Russian troops
1770.1.1  = {
	capital = "Aleksandrovsk"
} # Alexandrovskaya fortress, a central stronghold
1773.1.1  = { unrest = 5 } # Emelian Pugachev, Cossack insurrection
1774.9.14 = { unrest = 0 } # Pugachev is captured
1775.5.7   = {
	owner = RUS
	controller = RUS
} # Host disbanded by Empress Catherine II
