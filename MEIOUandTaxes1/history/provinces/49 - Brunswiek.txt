# 49 - Brunswiek

owner = BRU
controller = BRU
add_core = BRU
culture = old_saxon
religion = catholic
hre = yes
base_tax = 7
base_production = 7
trade_goods = silver
base_manpower = 3
is_city = yes
capital = "Brunswiek"
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1500.1.1 = { road_network = yes }
1529.1.1  = { religion = protestant }
1600.1.1  = { fort_14th = yes }
1737.1.1  = { early_modern_university = yes } # The university of Göttingen was founded in 1737 by George II August, King of Great Britain and prince-elector of Hanover.
1805.12.15 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HAN
} # Treaty of Schoenbrunn, ceded to Prussia
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.13 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = WES
} # The kingdom is dissolved
