# 144 - Gjirokaster

owner = SER
controller = SER
culture = albanian
religion = orthodox
capital = "Gjirokaster"
trade_goods = wool
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = CIR
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = turkishtech
add_permanent_claim = BYZ

1300.1.1 = { road_network = yes }
1356.1.1   = {
	owner = EPI
	controller = EPI
	add_core = EPI
	add_core = TSL
	add_permanent_claim = BYZ
	add_claim = SER
} # Epirus revolt
1386.1.1   = {
	owner = ALB
	controller = ALB
}
1400.1.1 = { remove_core = BYZ add_permanent_claim = BYZ }
1418.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1444.3.2  = {
	owner = ALB
	controller = ALB
} # Founding of the League of Lezh�
1479.4.25 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = SER
}
1481.6.1  = { revolt = { type = anti_tax_rebels size = 0 } controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { revolt = {  } controller = TUR } # Jem escapes to Rhodes
1515.2.1 = { training_fields = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1555.1.1  = { unrest = 5 } # General discontent against the Janissaries' dominance
1556.1.1  = { unrest = 0 }

1611.1.1  = { unrest = 3 } # Revolutionary movement of Dionysios, Christians driven away
1612.1.1  = { unrest = 0 } # Estimated

1700.1.1  = {  }
1750.1.1   = { add_core = GRE }
1788.1.1   = { fort_14th = yes } # Ali Pasha made it a stronghold
1797.1.1   = { revolt = { type = anti_tax_rebels size = 0 } controller = REB } # Suliot uprising
1803.12.17 = { revolt = {  } controller = TUR } # The Suliots are defeated
1821.3.1  = { revolt = { type = anti_tax_rebels size = 0 } controller = REB }
1829.1.1  = { revolt = {  } controller = TUR }
