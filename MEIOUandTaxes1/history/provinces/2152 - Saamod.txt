# 2152 - Saamod

culture = nenet
religion = tengri_pagan_reformed
capital = "Dudinka"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 50
native_ferocity = 2
native_hostileness = 1

1750.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
 	citysize = 10
   	culture = russian
   	religion = orthodox
	trade_goods = fur
	set_province_flag = trade_good_set
}
