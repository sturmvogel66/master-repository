# 4017 - Chuvash

culture = kazani
religion = sunni
capital = "�abaqsar"
trade_goods = lumber
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
native_size = 50
native_ferocity = 2
native_hostileness = 5
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1 = {
	owner = WHI
	controller = WHI
	is_city = yes
	trade_goods = lumber
	set_province_flag = trade_good_set
	unrest = 3
	add_core = WHI
	add_core = KAZ
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
	unrest = 0
}
1438.1.1  = {
	owner = KAZ
	controller = KAZ
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
}
1515.1.1 = { training_fields = yes }
1530.1.4  = {
	bailiff = yes	
}
1552.1.1 = {
	owner = RUS
	controller = RUS
	add_core = RUS
	religion = orthodox
	culture = pomor
} # Conquered by Muscovy
1606.1.1 = { add_core = RUS unrest = 3 } # Rebellions against Russian rule
1608.1.1 = { unrest = 5 }
1610.1.1 = { unrest = 2 }
1616.1.1 = { unrest = 6 }
1620.1.1 = { unrest = 0 }
