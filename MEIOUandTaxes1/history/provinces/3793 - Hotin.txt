# No previous file for Hotin

owner = MOL
controller = MOL  
add_core = MOL
culture = moldovian
religion =  orthodox
capital = "Hotin"
trade_goods = wine
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
fort_14th = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1200.1.1 = { road_network = yes }
#1498.1.1  = {
#	add_core = TUR
#} # Bayezid II forces Stephen the Great to accept Ottoman suzereignty.
1530.1.4  = {
	bailiff = yes	
}
1660.1.1  = {
	
}
1812.5.28 = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = TUR
} # Treaty of Bucarest ending the Russo-Turkidh War
