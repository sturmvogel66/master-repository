# 1379 - Umbria

owner = PAP
controller = PAP
culture = umbrian 
religion = catholic 
hre = no 
base_tax = 9
base_production = 9        
trade_goods = wine    
base_manpower = 4        
fort_14th = yes 
capital = "Spoleto" 
citysize = 35000	# Estimated 
add_core = PAP
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1309.1.1 = { add_core = PA2 owner = PA2 controller = PA2 }
1337.1.1 = { revolt = { type = noble_rebels size = 0 } controller = REB }
1354.1.1 = { revolt = { } controller = PA2 }
1356.1.1 = { citysize = 40000 }
1378.1.1 = { remove_core = PA2 owner = PAP controller = PAP }
1441.1.1 = {
	add_permanent_province_modifier = {
		name = "republic_of_cospaia"
		duration = -1
	}
	road_network = no paved_road_network = yes 
	bailiff = yes
}
1503.9.1 = { revolt = { type = anti_tax_rebels size = 0 } controller = REB } # Loss of Papal authority after the death of Alexander III, Venetian influence
1506.1.1 = { revolt = { } controller = PAP }
1550.1.1 = { citysize = 62000 } 
1600.1.1 = { citysize = 63000 } 
1650.1.1 = { citysize = 59000  } 
1700.1.1 = { citysize = 63000 } 
1750.1.1 = { citysize = 69000 } 
1800.1.1 = { citysize = 71000 }
1805.3.17 = { owner = ITA controller = ITA add_core = ITA } # Treaty of Pressburg
1814.4.11   = {	owner = PAP controller = PAP remove_core = ITA } # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1861.2.18 = {
	owner = ITA
	controller = ITA
	add_core = ITA
}
