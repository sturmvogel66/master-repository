# 338 - Arzew

owner = TLE
controller = TLE 
culture = kabyle
religion = sunni
capital = "Arzew"
base_tax = 10
base_production = 10
base_manpower = 2
is_city = yes
trade_goods = olive
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

hre = no

#1088.1.1 = { dock = yes shipyard = yes }
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1337.1.1  = {
	owner = FEZ
	controller = FEZ
}
1356.1.1  = {
	add_core = TLE 
	add_core = FEZ
}
1399.1.1  = {
	owner = TLE
	controller = TLE
}
1515.1.1 = { training_fields = yes }
1520.1.1 = {
	owner = ALG
	controller = ALG
	add_core = ALG
}
1530.1.1  = { fort_14th = yes } # Re-fortified
1530.1.2 = {
	remove_core = FEZ
	culture = algerian
}

1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1760.1.1  = {  }
1830.1.1 = {
	owner = FRA
	controller = FRA
	add_core = FRA
}
