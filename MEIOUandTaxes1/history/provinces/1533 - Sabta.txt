# 1533 - Sebta

owner = FEZ
controller = FEZ
culture = rifain
religion = sunni
capital = "Ceuta"
base_tax = 5
base_production = 5
base_manpower = 1
is_city = yes
trade_goods = olive
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

hre = no

900.1.1    = { set_province_flag = barbary_port }
1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "mediterranean_harbour" 
		duration = -1 
		}
}
1088.1.1 = { dock = yes }

1356.1.1 = {
	add_core = FEZ
	add_core = GRA
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1384.1.1   = {
	owner = GRA
	controller = GRA
}
1387.1.1   = {
	owner = FEZ
	controller = FEZ
	remove_core = GRA
}
1415.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
}
1450.1.1 = { temple = yes }
1522.3.20 = { dock = no naval_arsenal = yes }
1530.1.1 = {
	add_core = MOR
}
1530.1.2 = {
	remove_core = FEZ
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1556.1.1   = { remove_core = FEZ }
1580.1.1   = {
	owner = SPA
	controller = SPA
	add_core = SPA
	culture = andalucian # western_andalucian
	religion = catholic
	rename_capital = "Ceuta" 
	change_province_name = "Ceuta"
} # Conquered by the Duke of Medina Sidonia

1780.12.25 = { add_core = MOR } # Treaty of Aranjuez, Spain ceded some territory to Morocco
