# 2607 - jiangsu_area Tongzhou

owner = YUA
controller = YUA
culture = wuhan
religion = confucianism
capital = "Jiangdu"
trade_goods = chinaware
hre = no
base_tax = 9
base_production = 9
base_manpower = 4
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1000.1.1   = {
	add_permanent_province_modifier = {
		name = yangtze_estuary_modifier
		duration = -1
	}
	bailiff = yes constable = yes
}
#1111.1.1 = { post_system = yes }
1200.1.1 = { paved_road_network = yes }
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1356.1.1  = {
	
	add_core = MNG
#	remove_core = YUA
}
1358.1.1  = {
	owner = MNG
	controller = MNG
	fine_arts_academy = yes
}
1519.1.1  = { discovered_by = POR } # Tome Pires
1535.1.1  = { fort_14th = yes }
1542.1.1  = { unrest = 5 } # Assassination attemp on Shi Zong
1543.1.1  = { unrest = 0 }

1630.1.1  = { unrest = 6 } # Li Zicheng rebellion
1644.4.1  = { unrest = 8 } # Beijing is attacked
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty
