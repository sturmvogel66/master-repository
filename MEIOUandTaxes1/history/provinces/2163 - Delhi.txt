# 2163 - Delhi

owner = DLH
controller = DLH
culture = kanauji
religion = hinduism
capital = "Delhi"
trade_goods = cloth
hre = no
base_tax = 10
base_production = 10
base_manpower = 5
is_city = yes

discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = major_city
		duration = -1
	}
	add_permanent_province_modifier = {
		name = inland_center_of_trade_modifier
		duration = -1
	}
}
1115.1.1 = { bailiff = yes }
1120.1.1 = { textile = yes }
#1180.1.1 = { post_system = yes }

1200.1.1 = { marketplace = yes }
1201.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = DLH
	fort_14th = yes
}
1405.1.1  = { controller = DLH }
1444.1.1 = {
	add_core = PTA
}
1451.4.20 = {
	remove_core = PTA
}
1526.2.1 = { controller = TIM } # Babur's invasion
1526.4.21 = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = DLH
	training_fields = yes
} # Battle of Panipat
1530.1.1 = { 
	base_tax = 12
	base_production = 12
	base_manpower = 7
}
1530.1.2 = { add_core = TRT }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1540.1.1 = {
	owner = BNG
	controller = BNG
	add_core = BNG
} # Sher Shah Conquers Delhi
1546.1.1 = { fort_15th = yes } #Salimgarh Fort built by Shah Suri
1553.1.1 = {
	owner = DLH
	controller = DLH
	remove_core = BNG
} #Death of Islam Shah Sur, Suri empire split
1555.7.23 = {
	owner = MUG
	controller = MUG
} # Humayun Returns
1556.10.7 = { controller = DLH }	# Hemu
1556.11.5 = { controller = MUG }	#Aftermath to second battle of Panipat
1566.1.1 = { revolt = { type = noble_rebels size = 1 } }
1566.6.1 = { revolt = { } }
1638.1.1 = {
	capital = "Shahjahanabad"
} # Delhi became the capital & grew in importance again
1648.1.1 = { fort_15th = no fort_16th = yes } #The Red Fort
1660.1.1 = { trade_goods = sugar  }

1707.5.12 = { discovered_by = GBR }
1770.1.1 = {
	owner = MAR
	controller = MAR
	add_core = MAR
	remove_core = MUG
} # The Marathas
1784.1.1 = {
	capital = "Delhi"
} # The Marathas install puppet
1800.1.1 = {  }
1803.12.30 = {
	owner = GBR
	controller = GBR
}
1818.6.3 = { remove_core = MAR }
