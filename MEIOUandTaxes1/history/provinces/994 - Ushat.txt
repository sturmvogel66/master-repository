# No previous file for Ushat

culture = innu
religion = totemism
capital = "Uashat"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 1 
native_hostileness = 6

1679.1.1  = {	owner = FRA
		controller = FRA
		citysize = 100
		culture = francien
		religion = catholic
	    }
1704.1.1  = {	add_core = FRA trade_goods = fish }
1707.5.12 = { discovered_by = GBR }
1763.2.10  = { owner = GBR controller = GBR
	    	culture = french_colonial citysize = 500 }
1788.2.10 = { add_core = GBR }
