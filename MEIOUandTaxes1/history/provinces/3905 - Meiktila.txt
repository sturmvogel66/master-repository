# 3905 - Meiktila

owner = AVA
controller = AVA
culture = burmese
religion = buddhism
capital = "Meiktila"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = opium
discovered_by = chinese
discovered_by = indian
# Awa between 2 rivers and many moats made from swamps

1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = AVA
	add_core = MYA
	add_core = PEG
}
1510.1.16 = {
	owner = MYA
	controller = MYA
}
1527.1.1 = {
	#owner = TAU
	#controller = TAU
	add_core = TAU
	remove_core = MYA
	remove_core = PEG
	unrest = 8
}
1530.1.1 = {
	add_core = TAU
	remove_core = AVA
	remove_core = MYA
	remove_core = PEG
	#unrest = 50
}
1752.2.28 = {
	owner = BRM
	controller = BRM
	add_core = BRM
	remove_core = TAU
	remove_core = AVA
}
1885.1.1 = {		
	owner = GBR
	controller = GBR
}
