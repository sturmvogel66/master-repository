# 2709 - Rewa Kantha

owner = CMP
controller = CMP
culture = bhil
religion = adi_dharam
capital = "Champaner"
trade_goods = millet
hre = no
base_tax = 9
base_production = 9
#base_manpower = 1.0
base_manpower = 2.0
citysize = 8000
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = rajpipla_state
		duration = -1
	}
}
1100.1.1 = { marketplace = yes }
1120.1.1 = { farm_estate = yes }

1356.1.1   = {
	add_core = CMP
}
1450.1.1   = { citysize = 14000 }
1485.1.1   = { controller = GUJ }
1485.11.21 = {
	owner = GUJ
	capital = "Muhammadabad"
	fort_14th = yes
} # Mahmud Begada captures champaner
1490.1.1 = { temple = yes }
1498.1.1   = { discovered_by = POR }
1500.1.1   = { citysize = 30000 }
1515.12.17 = { training_fields = yes }
1530.1.1 = { add_core = GUJ }
1530.2.3 = {
	add_permanent_claim = MUG
}
1530.3.17 = {
	bailiff = yes
	road_network = yes
}
1544.1.1   = {
	owner = BNG
	controller = BNG
} # Sur expansionism
1545.1.1   = {
	owner = GUJ
	controller = GUJ
} # Death of Sher Shah
1550.1.1   = { citysize = 33000 }
1573.6.1   = {
	owner = MUG
	controller = MUG
	add_core = MUG
	capital = "Godhra"
} # Conquered by Akbar
1600.1.1   = { citysize = 16000 }
1700.1.1   = { citysize = 18000 }
1735.1.1   = {
	owner = GWA
	controller = GWA
	add_core = GWA
	remove_core = MUG
	capital = "Rajpipla"
} #
1750.1.1   = { citysize = 21000 }
1800.1.1   = { citysize = 21500 }
1803.1.1   = {
	owner = GBR
	controller = GBR
	remove_core = GWA
	capital = "Godhra"
}
