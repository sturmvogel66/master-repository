# 2721 - Cholistan

owner = MUL
controller = MUL
culture = multani
religion = sunni
capital = "Uch"
trade_goods = wool
hre = no
base_tax = 3
base_production = 3
#base_manpower = 1.5
base_manpower = 3.0
citysize = 4000
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1120.1.1 = { textile = yes }
#1180.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1  = {
	#owner = DLH
	#controller = DLH
	#add_core = DLH
	add_core = MUL
	add_core = PTA
	add_core = BHW
}
1398.8.1  = {
	revolt = {  }
	controller = TIM
}
1399.1.1  = {
	owner = MUL
	controller = MUL
}
1414.1.1  = {
	owner = DLH
	controller = DLH
	remove_core = MUL
}
1437.1.1  = {
	owner = MUL
	controller = MUL
	add_core = MUL
	remove_core = PUN
	remove_core = DLH
} # Captured by Langas and separated from Sultanate
1444.1.1 = {
	add_core = BHW
}
1526.1.1  = {
	owner = SND
	controller = SND
} # Conquered by Arguns
1527.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Turned over to Babur
1530.1.1 = { add_core = TRT }
1544.1.1 = {
	owner = BNG
	controller = BNG
} # Sur Expansionism
1545.1.1 = {
	owner = SND
	controller = SND
} # Sher Shah dies
1574.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Integrated into Mughal Empire
1690.1.1  = { discovered_by = ENG }
1690.1.1 = {
	owner = BHW
	controller = BHW
	add_core = BHW
	remove_core = MUG
}
1707.5.12 = { discovered_by = GBR }
#1752.1.1  = {
#	owner = DUR
#	controller = DUR
#	remove_core = MUG
#}
1802.1.1  = {
	owner = BHW
	controller = BHW
	add_core = BHW
}
