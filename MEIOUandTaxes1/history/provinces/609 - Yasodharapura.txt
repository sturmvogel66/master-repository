# 609 - Angkor Thom

owner = KHM
controller = KHM
culture = khmer
religion = buddhism
capital = "Yasodharapura"
base_tax = 12
base_production = 12
base_manpower = 4
is_city = yes
trade_goods = gems #rubis

discovered_by = chinese
discovered_by = indian
hre = no

1356.1.1 = {
	#controller = AYU
	add_core = KHM
	add_core = AYU
	fort_14th = yes
}
1360.1.1 = {
	controller = KHM
}
1432.1.1 = {
	capital = "Angkor Thom"
}  #sacked by Ayutthaya, city deserted since and capital moved to Lavack
1767.4.8 = { 
	add_core = SIA
	remove_core = AYU
}
1811.1.1 = { controller = REB } # The Siamese-Cambodian Rebellion
1812.1.1 = { controller = KHM }
1867.1.1 = { 			# agreement with France
	owner = SIA
	controller = SIA
}
1907.1.1 = { 			
	owner = FRA
	controller = FRA
}
