# 680 - fujian_area Gulou

owner = CMN
controller = CMN
culture = minyu
religion = confucianism
capital = "Minxian"
trade_goods = silk
hre = no
base_tax = 10
base_production = 10
base_manpower = 5
is_city = yes
fort_14th = yes

discovered_by = chinese
discovered_by = steppestech

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1200.1.1 = { paved_road_network = yes courthouse = yes }
1250.1.1 = { temple = yes }
1279.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
    remove_core = SNG
}
1315.1.1 = {	#Viceroy of Fujian
	owner = CEN
	controller = CEN
	add_core = CEN	
	add_core = CMN
}
#1356.1.1 = {	remove_core = YUA } # Red Turbans
1368.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = CMN
	remove_core = CEN
	remove_core = YUA
	textile = yes
}
1404.1.1 = {	 }
1435.1.1 = {	shipyard = no }
1662.1.1 = {
	owner = CMN
	controller = CMN
	add_core = CMN
	remove_core = MNG
}# Geng Jingzhong appointed as viceroy
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1680.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = CMN
}
