# 4116 - Chinguetti

culture = tuareg
religion = sunni
capital = "Chinguetti"
native_size = 60
native_ferocity = 4.5
native_hostileness = 9
trade_goods = unknown
discovered_by = CNA
discovered_by = DJO
discovered_by = TUA
discovered_by = MAL
discovered_by = MOR
discovered_by = FEZ
hre = no

1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}