# No previous file for Taif

owner = HEJ
controller = HEJ
culture = hejazi
religion = sunni
capital = "Taif"
trade_goods = wax
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = ADA
discovered_by = MKU
discovered_by = KIL
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	owner = MAM
	controller = MAM
	add_core = HEJ
	add_core = MAM
}
1516.1.1   = { add_core = TUR }
1517.4.13  = { 
	owner = TUR
	controller = TUR
	remove_core = MAM
} # Conquered by Ottoman troops
#1530.1.1   = {
#	owner = HEJ
#	controller = HEJ
#	add_core = HEJ
#	remove_core = TUR
#}
1530.1.5 = {
	owner = HEJ
	controller = HEJ
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1802.1.1 = {
	owner = NAJ
	controller = NAJ
	add_core = NAJ
} # Incorporated into the First Saudi State
1818.9.9 = {
	owner = TUR
	controller = TUR 
} # The end of the Saudi State
