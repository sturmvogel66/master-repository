# 1124 - Worodugu

owner = MAL
controller = MAL
culture = bambara
religion = west_african_pagan_reformed
capital = "Worodugu"
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
trade_goods = slaves
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = MAL
}
1469.1.1  = {
	owner = SON
	controller = SON
	add_core = SON
}
1591.3.15  = {
	owner = ZAF
	controller = ZAF
	add_core = ZAF
} #Collapse of Songhai in wake of Tondibi
1660.1.1  = {
	owner = SEG
	controller = SEG
	add_core = SEG
}
