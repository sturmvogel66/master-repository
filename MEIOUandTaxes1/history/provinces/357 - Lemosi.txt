owner = FRA
controller = FRA
culture = limousin
religion = catholic
capital = "Limotges"
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
trade_goods = livestock
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

1249.1.1 = {
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1258.1.1   = {
	owner = GUY
	controller = GUY
	add_core = FRA
	#add_core = ENG
	add_core = GUY
}
1300.1.1 = { road_network = yes }
1370.1.1   = {
	owner = FRA
	controller = FRA
	unrest = 0
} # The bishop opens the gates to the french troops
1389.1.1   = {
	owner = DAL
	controller = DAL
}
1475.8.29  = {
	remove_core = GUY
} # Treaty of Picquigny, ending the Hundred Year War
1530.1.1 = {
   owner = FRA
   controller = FRA
   add_core = FRA
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1550.1.1   = { fort_14th = yes }
1560.1.1   = { religion = reformed }
1565.1.1   = { unrest = 8 } # France is restless once again as ultra-catholic intentions become clear
1568.9.1   = { unrest = 15 } # Catherine de Medici and Charles IX side with the Guise faction, religious intolerance peaks
1570.8.8   = { unrest = 10 } # Edict of Saint-Germain: temporary pacification
1573.9.1   = { unrest = 15 } # Saint Barthelew's Day Massacre: the consequences in the land
1574.5.1   = { unrest = 7 } # Charles IX dies, situation cools a bit	
1584.1.1   = { unrest = 12 } # Situation heats up again
1588.12.1  = { unrest = 15 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1589.1.1   = { base_tax = 4
base_production = 4 } # At the walls of Paris - French War of Religion
1594.1.1   = { unrest = 10 } # 'Paris vaut bien une messe!', Henri converts to Catholicism
1598.4.13  = { unrest = 3 } # Edict of Nantes, alot more freedom to the protestants
1598.5.2   = { unrest = 0 } # Peace of Vervins, formal end to the Wars of Religion
1607.1.1   = {
	owner = FRA
	controller = FRA
	remove_core = DAL
}
1645.1.1   = { fort_14th = no fort_15th = yes }
1650.1.14  = { unrest = 7 } # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1650.3.1   = { revolt = { type = noble_rebels size = 0 } controller = REB unrest = 3} # Fronde rebels take control
1651.4.1   = { revolt = { } controller = FRA unrest = 4 } # An unstable peace is concluded
1651.12.1  = { unrest = 7 } # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.2.15  = { revolt = { type = noble_rebels size = 2 } controller = REB } # Fronde rebels make a foothold in the Guyenne area, under Cond�
1652.10.21 = { revolt = { } controller = FRA unrest = 0 } # The King is allowed to enter Paris again, Mazarin leaves France for good.


1685.10.18 = { unrest = 8 } # Edict of Nantes revoked by Louis XIV
1686.1.17  = { religion = catholic } # Dragonnard campaign succesful: region reverts back to catholicism
1689.1.1   = { unrest = 0 } # War of the Grand Alliance erupts: Louis XIV can't persue his religious policy anymore Second Fronde over.
1701.1.1   = { unrest = 7 } # War of Spanish Succession erupts: The Camisards see an opportunity to revolt against Louis XIV
1702.1.1   = { revolt = { type = revolutionary_rebels size = 2 } controller = REB } # Camisard rebels in control
1705.1.1   = { revolt = { } controller = FRA unrest = 3 } # Camisard rebellion ends, isolated aggression continues until 1710
1710.1.1   = { unrest = 0 fort_15th = no fort_16th = yes } # Rebellion ends



