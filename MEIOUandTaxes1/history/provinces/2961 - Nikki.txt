# 2961 - Nikki

owner = BOR
controller = BOR
culture = yorumba
religion = west_african_pagan_reformed
capital = "Nikki"
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
trade_goods = millet
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = BOR
}
1806.1.1 = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
