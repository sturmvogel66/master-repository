# 112 - Venexia

owner = VEN
controller = VEN
add_core = VEN
culture = venetian  
religion = catholic 
capital = "Venexia"

base_tax = 12
base_production = 12
base_manpower = 5
is_city = yes       
trade_goods = glassware

fort_14th = yes
shipyard = yes
extra_cost = 30



discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

hre = no 


1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
	}
	add_permanent_province_modifier = {
		name = major_city
		duration = -1
	}
}
1088.1.1 = { dock = yes }
1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1250.1.1 = { temple = yes }
1284.1.1 = {  }
1300.1.1 = { road_network = yes }
1356.1.1 = {
	add_permanent_province_modifier = { name = laguna_veneta duration = -1 }
} 
1470.1.1 = { medieval_university = yes }
1522.3.20 = { dock = no naval_arsenal = yes }
1530.1.2 = {
	road_network = no paved_road_network = yes 
	city_watch = yes
}
1797.10.17 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} # Treaty of Campo Formio
1805.12.26 = {
	owner = ITE
	controller = ITE
	add_core = ITE
    remove_core = HAB
} # Treaty of Pressburg
1814.4.11  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
	
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1866.1.1 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
