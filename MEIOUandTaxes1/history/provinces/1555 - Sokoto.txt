# 1555 - Zamfara

owner = KTS
controller = KTS
culture = haussa		
religion = sunni		 
capital = "Zamfara"
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes
trade_goods = millet
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	add_core = KTS
}
1805.1.1 = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
1810.1.1   = {
	remove_core = KTS
}
