# No previous file for Bujadur

culture = hassaniya
religion = sunni
capital = "Bujadur"
trade_goods = unknown # fish
hre = no
native_size = 50
native_ferocity = 4.5
native_hostileness = 9
discovered_by = soudantech
discovered_by = sub_saharan
discovered_by = muslim

1356.1.1 = {
    add_permanent_province_modifier = {
        name = trading_post_province
        duration = -1
    }
}
1437.1.1   = { discovered_by = POR } #Cadamosto
1445.1.1   = {
	owner = CAS
	controller = CAS
	add_core = CAS
	citysize = 200
	trade_goods = fish
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
