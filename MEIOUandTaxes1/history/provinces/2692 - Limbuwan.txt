# 2692 - Limbuwan

owner = NPL
controller = NPL
culture = nepali
religion = hinduism
capital = "Phedap"
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech

1356.1.1  = {
	add_core = NPL
	fort_14th = yes
}
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
