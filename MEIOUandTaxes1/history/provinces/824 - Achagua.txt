# 824 - Achagua

culture = jivaro
religion = pantheism
capital = "Achagua"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 2
discovered_by = south_american

1356.1.1 = {
}
1513.1.1   = {
	discovered_by = CAS
}
1516.1.23  = {
	discovered_by = SPA
}
1530.1.1 = {
	owner = PIZ
	controller = PIZ
	add_core = PIZ
	remove_core = SPA
	culture = castillian
	religion = catholic
	capital = "Santa F� de Bacat�"
	trade_goods = coffee
	is_city = yes
	road_network = yes
	training_fields = yes
	fort_15th = yes
}
1533.8.29 = {
	owner = SPA
	controller = SPA
	citysize = 540
	culture = castillian
	religion = catholic
	remove_core = PIZ
}
1550.1.1   = {
	citysize = 1170
	add_core = SPA
}
1750.1.1  = {
	add_core = COL
	culture = peruvian
}
1820.10.9  = {
	owner = COL
	controller = COL
} # Part of Gran Colombia
1824.12.1  = {
	remove_core = SPA
}
1830.5.24  = {
	owner = ECU
	controller = ECU
	add_core = ECU
} # Independance from Gran Colombia
1831.11.19 = {
	remove_core = COL
} # Gran Colombia dismantled
