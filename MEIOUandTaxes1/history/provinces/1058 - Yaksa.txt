# 1058 - Barguzinski

owner = MYR
controller = MYR
culture = daur
religion = tengri_pagan_reformed
capital = "Yaksa"
trade_goods = fur
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = steppestech

1356.1.1 = {
	add_core = MYR
}
1515.1.1 = { training_fields = yes }
1643.1.1 = { discovered_by = RUS } # Vasily Poyarkov
1650.9.1 = {
	owner = RUS
	controller = RUS
	add_core = RUS
	capital = "Albazin"
}
1652.1.1 = {
	owner = QNG
	controller = QNG
}
1655.1.1 = {
	owner = MYR
	controller = MYR
}
1675.1.1 = {
	owner = RUS
	controller = RUS
}
1689.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = RUS
}
1709.1.1 = { discovered_by = SPA }
