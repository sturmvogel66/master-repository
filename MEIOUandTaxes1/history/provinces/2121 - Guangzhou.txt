# 2121 - guangdong_area Guangzhou

owner = YUA
controller = YUA
culture = yueyu
religion = confucianism
capital = "Panyu"
trade_goods = iron
hre = no
base_tax = 12
base_production = 12
base_manpower = 7
is_city = yes
fort_14th = yes



discovered_by = chinese
discovered_by = muslim
discovered_by = steppestech
discovered_by = indian

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1000.1.1 = {
	add_permanent_province_modifier = {
		name = major_city
		duration = -1
	}
	add_permanent_province_modifier = {
		name = pearl_estuary_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
}
#1111.1.1 = { post_system = yes }
1200.1.1 = { paved_road_network = yes marketplace = yes courthouse = yes }
1250.1.1 = { temple = yes naval_arsenal = yes }
1279.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
1351.1.1 = {
	add_core = YUE
}
#1356.1.1 = { remove_core = YUA }
1356.1.1 = {
	owner = YUE
	controller = YUE
}
1368.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = YUE
	remove_core = YUA
}
1514.1.1 = { discovered_by = POR }
1662.1.1 = {
	owner = YUE
	controller = YUE
	add_core = YUE
	remove_core = MNG
}# Shang Kexi appointed as viceroy
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1680.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = YUE
}
1740.1.1 = {  }
