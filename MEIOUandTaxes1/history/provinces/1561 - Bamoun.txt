# 1561 -Bamoun

owner = NRI
controller = NRI
culture = bakongo		
religion = animism		 
capital = "Bamoun"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = slaves
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = IGB
}
1807.1.1   = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
