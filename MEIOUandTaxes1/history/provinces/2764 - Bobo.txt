# 2764 - Bobo

owner = MAL
controller = MAL
culture = mossi
religion = west_african_pagan_reformed
capital = "Bobo"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = ivory
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = MAL
}
1600.1.1  = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = SON
} # Battle of Tondibi
1612.1.1   = {
	owner = TBK
	controller = TBK
	remove_core = MOR
} # Moroccans establish the Arma as new independent ruling class
1618.1.1   = {
	owner = JNN
	controller = JNN
}
1715.1.1  = {
	owner = KNG
	controller = KNG
	add_core = KNG
}
