# Province: Songkhla

owner = NST
controller = NST
culture = malayan
religion = buddhism
capital = "Singgora"
trade_goods = sandal
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
discovered_by = austranesian

1356.1.1 = {
	add_core = AYU
	add_core = NST
	fort_14th = yes
}
1467.4.8 = {
	owner = AYU
	controller = AYU
    	add_core = AYU
	remove_core = NST
	unrest = 0
	culture = thai
	rename_capital = "Songkhla" 
	change_province_name = "Songkhla"
}
1564.2.1 = { add_core = TAU } # Burmese vassal
1584.5.3 = { remove_core = TAU }
1767.4.8 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
	unrest = 0
}
