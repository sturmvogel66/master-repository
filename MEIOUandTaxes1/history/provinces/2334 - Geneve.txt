# 2334 - Gen�ve

owner = GNV
controller = GNV  
culture = arpitan
religion = catholic
capital = "Gen�ve"
trade_goods = livestock
hre = yes
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes
fort_14th = yes
add_core = GNV
discovered_by = western
discovered_by = eastern
discovered_by = muslim

1250.1.1 = { temple = yes }
1500.1.1 = { road_network = yes }
1515.1.1 = { training_fields = yes }
1523.1.1 = {
	religion = reformed
	#reformation_center = reformed
}
1530.1.2 = {
	road_network = no paved_road_network = yes 
}

1530.1.4  = {
	bailiff = yes	
}
1618.1.1 = { hre = no }

1653.1.1 = { unrest = 5 } # Peasant rebellion against overtaxation
1654.1.1 = { unrest = 0 }
1656.1.1 = { unrest = 7 } # Protestants are expelled from Schwyz which lead to the First War of Villmergen
1657.1.1 = { unrest = 2 } # An agreement is concluded at Villmergren but tensions remain
1705.1.1 = {  }
1798.3.5  = { controller = FRA } # French occupation
1798.4.12 = {
	owner = SWI
	controller = SWI
	add_core = SWI
	hre = no
} # The establishment of the Helvetic Republic
1798.4.15 = { controller = REB } # The Nidwalden Revolt
1798.9.1  = { controller = SWI } # The revolt is supressed
1802.6.1  = { controller = REB } # Swiss rebellion
1802.9.18 = { controller = SWI }
