# 1033 - Wakasa
# GG/LS - Japanese Civil War

owner = ISK
controller = ISK
culture = kansai
religion = mahayana #shinbutsu
capital = "Miyazu"
trade_goods = fish
hre = no
is_city = yes # citysize = 1500
base_tax = 3
base_production = 3
base_manpower = 3

#fort_14th = yes # Wakasahime Shirine (Obama) and Kehi Shirine (Tsuruga). 

discovered_by = chinese

1356.1.1 = {
	add_core = NIK
	add_core = ISK
}
1525.1.1 = {
	owner = ODA
	controller = ODA
}
1542.1.1 = { discovered_by = POR }
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
