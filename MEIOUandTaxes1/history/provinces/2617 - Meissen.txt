# 2617 - Meissen

owner = MEI
controller = MEI
culture = high_saxon
religion = catholic
capital = "Meissen"
trade_goods = lumber
hre = yes
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
add_core = MEI
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1423.6.1   = { 
	owner = SAX
	controller = SAX 
	add_core = SAX
	remove_core = MEI
}#Margraviate of Meissen inherits Saxony and becomes the Elector.
1485.11.11   = { 
	owner = MEI
	controller = MEI
	add_core = MEI
	remove_core = SAX
} #Treaty of Leipzig
1500.1.1 = { road_network = yes }
1520.12.10 = {
	religion = protestant
	#reformation_center = protestant
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1547.5.19   = { 
	owner = SAX
	controller = SAX 
	add_core = SAX
	remove_core = MEI
} #Treaty of Wittenberg
1560.1.1  = { fort_16th = yes }
1790.8.1  = { unrest = 5 } # Peasant revolt
1791.1.1  = { unrest = 0 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
