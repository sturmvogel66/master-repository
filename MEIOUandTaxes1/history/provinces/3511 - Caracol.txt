# 3511 - Caracol

owner = CTM
controller = CTM
add_core = CTM
culture = itza
religion = mesoamerican_religion
capital = "Caracol"


base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
citysize = 5000
trade_goods = maize 




discovered_by = mesoamerican

1502.1.1   = {
	discovered_by = CAS
}
1516.1.23  = {
	discovered_by = SPA
}
1638.1.1   = {
	discovered_by = ENG
	owner = ENG
	controller = ENG
	capital = "Dangriga"
	citysize = 200
	religion = protestant #anglican
	culture = english
	trade_goods = lumber
	base_tax = 3
base_production = 3
	}
1655.1.1   = {
	add_core = ENG
	citysize = 1000
}
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
	citysize = 2000
}
#1750.1.1   = {
#	add_core = USA
#	culture = american
#	citysize = 5000
#}
1800.1.1   = {
	citysize = 10000
}
