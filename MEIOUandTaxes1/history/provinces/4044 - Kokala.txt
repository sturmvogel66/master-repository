# 4044 - Kokala

culture = bugis
religion = polynesian_religion
capital = "Kokala"
trade_goods = unknown # lumber
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 60
native_ferocity = 2
native_hostileness = 5
add_core = BNE
discovered_by = chinese
discovered_by = austranesian

1000.1.1   = {
	set_province_flag = bugis_natives
}
1511.1.1 = { discovered_by = POR }
1608.1.1 = { religion = sunni }
1644.1.1 = {
	discovered_by = MKS
	owner = MKS
	controller = MKS
	add_core = MKS
	is_city = yes
	trade_goods = lumber
}
1667.1.1  = {
	owner = NED
	controller = NED
} # Under Dutch control
1700.1.1 = { add_core = NED }
1811.9.1 = {
	owner = GBR
	controller = GBR
} # British take over
1816.1.1 = { owner = NED controller = NED } # Returned to the Dutch
