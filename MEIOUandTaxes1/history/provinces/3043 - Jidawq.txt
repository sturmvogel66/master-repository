# No previous file for Jidawq

owner = MED
controller = MED
culture = tigrean
religion =coptic
capital = "Antalo"
base_tax = 2
base_production = 2
#base_manpower = 1.0
base_manpower = 2.0
citysize = 3000
trade_goods = copper
discovered_by = ETH
discovered_by = ADA
discovered_by = ZAN
discovered_by = AJU
discovered_by = MBA
discovered_by = MLI
discovered_by = ZIM
discovered_by = indian
discovered_by = muslim
discovered_by = turkishtech
hre = no

1356.1.1   = {
	add_core = MED
}
#1486.1.1 = { unrest = 5 add_core = ADA } #Raids by Mahfuz Of Zayla
1495.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1499.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1510.1.1 = { unrest = 9 } #Raids by Mahfuz Of Zayla
1515.2.1 = { training_fields = yes }
1530.1.1 = { owner = AFA controller = AFA add_core = AFA remove_core = ADA }
1588.1.1 = { unrest = 7 } # Raids by Sarsa Dengel
1589.1.1 = { unrest = 0 }
