# 2217 - Cagayan

culture = tagalog
religion = hinduism
capital = "Cagayan"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 1.0
base_manpower = 2.0
native_size = 50
native_ferocity = 1
native_hostileness = 3
discovered_by = chinese
discovered_by = austranesian

1000.1.1   = {
	set_province_flag = tagalog_natives
}
1521.1.1 = { discovered_by = SPA } # Ferdinand Magellan
1570.1.1 = {
	owner = SPA
	controller = SPA
	culture = castillian
	religion = catholic
	citysize = 150
	base_tax = 3
base_production = 3
	trade_goods = clove
	set_province_flag = trade_good_set
}
1595.1.1 = { add_core = SPA }
1600.1.1 = { citysize = 700 }
1650.1.1 = { citysize = 1254 }
1700.1.1 = { citysize = 2800 }
1750.1.1 = { citysize = 4050 }
1800.1.1 = { citysize = 6800 }
