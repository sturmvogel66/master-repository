# 2883 - Kunta Kinteh

culture = senegambian
religion = west_african_pagan_reformed
capital = "Kunta Kinteh"
trade_goods = unknown
hre = no
base_tax = 1
native_size = 50
native_ferocity = 4.5
native_hostileness = 9
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1 = {
    add_permanent_province_modifier = {
        name = trading_post_province
        duration = -1
    }
}
1437.1.1   = { discovered_by = POR } #Cadamosto
1588.1.1   = {
	owner = KUR
	controller = KUR
	add_core = KUR
	add_core = ENG
	capital = "Fort Jacob"
	citysize = 200
		trade_goods = slaves
}
1661.1.1   = {
	owner = ENG
	controller = ENG
	remove_core = KUR
	capital = "Fort James"
}
1695.1.1   = {
	owner = FRA
	controller = FRA
}
1702.1.1   = {
	owner = ENG
	controller = ENG
}
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
    	remove_core = ENG
}
1795.10.24 = {
	remove_core = KUR
}
1807.1.1   = {
	trade_goods = fish
} # Slave trade abolished
