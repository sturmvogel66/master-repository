# No previous file for Arad

owner = HUN
controller = HUN  
culture = transylvanian
religion = catholic
capital = "Arad"
trade_goods = wheat
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

#1133.1.1 = { mill = yes }
1356.1.1  = {
	add_core = HUN
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1506.1.1  = {
	controller = REB
} # Szekely rebellion
1507.1.1  = {
	controller = HUN
} # Estimated
1514.4.1  = {
	controller = REB
} # Peasant rebellion against Hungary's magnates
1515.1.1  = {
	controller = HUN
} # Estimated
1515.2.1 = { training_fields = yes }
1520.1.1  = { 
	
}
1523.8.16 = { mill = yes }
1526.8.30  = {
	owner = TRA
	controller = TRA
	add_permanent_claim = HAB
	add_core = HUN
	add_core = TRA
}
#1530.1.1 = { controller = HAB }
1530.1.2 = { add_claim = TUR }
1530.1.3 = {
	road_network = no paved_road_network = yes fort_14th = yes
}
1530.1.4  = {
	bailiff = yes	
}
1552.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	capital = "Tamisvar"
}
1592.1.1  = {
	controller = REB
} # Serb rebellion
1600.1.1  = {
	controller = TUR
}
1650.1.1  = {
	
}
1716.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
}
1760.1.1  = {
	
}
