# 1520 - Kovai

owner = MAD
controller = MAD
culture = tamil
religion = hinduism
capital = "Coimbatore"
trade_goods = tea
hre = no
base_tax = 3
base_production = 3
#base_manpower = 0.5
base_manpower = 1.0
citysize = 7400
discovered_by = indian
discovered_by = muslim
add_local_autonomy = 25
1101.1.1 = { plantations = yes }
1356.1.1  = { add_core = MAD }
1378.1.1 = { owner = VIJ controller = VIJ }
1400.1.1 = { citysize = 8004 }
1428.1.1 = { add_core = VIJ }
1500.1.1 = { citysize = 9210 }
1530.1.1 = {
	#owner = MAD
	#controller = MAD
	add_core = MAD
	#remove_core = VIJ
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1550.1.1 = { citysize = 9763 }
1565.7.1 = {
	owner = MAD
	controller = MAD
	remove_core = VIJ
} # The Vijayanagar empire collapses
1600.1.1 = { citysize = 11865 }
1650.1.1 = { citysize = 13044  discovered_by = turkishtech }
1700.1.1 = { citysize = 16457 }
1750.1.1 = { citysize = 18760  }
1752.1.1 = {
	owner = MYS
	controller = MYS
	add_core = MYS
}
1799.1.1 = {
	owner = GBR
	controller = GBR
} # Fourth Anglo-Mysore War
1800.1.1 = { citysize = 21500 }
1824.1.1 = { add_core = GBR }
