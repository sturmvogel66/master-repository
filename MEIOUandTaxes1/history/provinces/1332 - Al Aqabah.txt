# 1332 - Al Aqabah

owner = MAM
controller = MAM
culture = hejazi
religion = sunni
capital = "Al Aqabah"
trade_goods = wheat
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes

discovered_by = ADA
discovered_by = MKU
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech
discovered_by = indian

1133.1.1 = { mill = yes }
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = HEJ
	add_core = MAM
}
1516.1.1   = { add_core = TUR }
1517.1.1   = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
1530.1.1   = {
#	owner = HEJ
#	controller = HEJ
#	add_core = HEJ
	add_core = HEJ
#	remove_core = TUR
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.5 = {
	owner = HEJ
	controller = HEJ
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1802.1.1 = {
	owner = NAJ
	controller = NAJ
	add_core = NAJ
} # Incorporated into the First Saudi State
1811.1.1 = {
	add_core = HEJ
} # Intervention of Mehmet Ali on behalf of the Sultan
1812.6.1 = {
	owner = TUR
	controller = TUR
	remove_core = NAJ
}
