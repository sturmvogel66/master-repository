#Province: Gulf of Zambeze

discovered_by = ANT
discovered_by = BAA
discovered_by = BET
discovered_by = BNA
discovered_by = MER
discovered_by = MHF
discovered_by = MNB
discovered_by = SIH
discovered_by = TAM
discovered_by = ZAN
discovered_by = KIL
discovered_by = QLM
discovered_by = SOF
discovered_by = MLI
discovered_by = MBA
discovered_by = PTA
discovered_by = MOG

1498.3.1 = { discovered_by = POR } #Vasco Da Gama
