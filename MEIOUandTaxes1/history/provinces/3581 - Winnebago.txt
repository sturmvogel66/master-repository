# No previous file for Winnebago

culture = winnebago
religion = totemism
capital = "Winnebago"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 45
native_ferocity = 2
native_hostileness = 5

1650.1.1 = {
	owner = FOX
	controller = FOX 
	is_city = yes
	trade_goods = fur
	add_core = FOX
}
1684.1.1  = {  } # Nicolas Perrot
1733.1.1  = {	owner = FRA
		controller = FRA
		citysize = 250
		culture = francien
		religion = catholic
	    	 trade_goods = iron } #End of the second Fox war
1750.1.1  = { citysize = 1390 }
1758.1.1  = { add_core = FRA }
1763.2.10 = {	
		owner = GBR
		controller = GBR
		remove_core = FRA
		culture = english
		religion = protestant
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1763.10.9 = {	owner = FOX
		controller = FOX
		add_core = FOX
		culture = winnebago
		religion = totemism
	    } # Royal Proclamation, British recognize native territory
1800.1.1  = { citysize = 3700 }
1813.10.5 = {	owner = USA
		controller = USA
		culture = american
		religion = protestant } #The death of Tecumseh mark the end of organized native resistance 
