# 618 - Batak

owner = ARU
controller = ARU
culture = batak
religion = vajrayana		#this region began to be Islamified c1300
capital = "Aru"
trade_goods = opium		#tobacco?
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1356.1.1 = {
 	add_core = ARU
}
1540.1.1 = {
	owner = ATJ
	controller = ATJ
 	add_core = ATJ
}
1550.1.1 = {
	religion = sunni
}
1683.1.1 = {
	add_core = NED
}
1825.1.1 = {
	owner = NED
	controller = NED
	unrest = 2
} # The Dutch gradually gained control
