# 1434 - Euboia

owner = VEN
controller = VEN
culture = greek
religion = orthodox
capital = "Chalkis"
trade_goods = wine
hre = no
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes
fort_14th = yes
discovered_by = CIR
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = turkishtech
add_permanent_claim = BYZ

1088.1.1 = { dock = yes }
1356.1.1  = {
	add_core = VEN
}
1444.1.1 = {
	add_core = TUR
}
1453.5.29 = {
	add_core = TUR
} # Constantinople has fallen
1470.1.1  = {
	owner = TUR
	controller = TUR
}
1519.1.1 = { bailiff = yes }
1555.1.1  = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { unrest = 0 }

1750.1.1  = { add_core = GRE }
1821.3.1  = {
	controller = REB
}
1829.1.1  = {
	owner = GRE
	controller = GRE
}
1832.5.7  = {
	remove_core = TUR
} # Treaty of Constantinople
