#  - Churru

owner = BIK
controller = BIK
culture = jati
religion = hinduism
capital = "Churru"
trade_goods = wool
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
citysize = 3000
add_local_autonomy = 25
add_core = BIK
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1120.1.1 = { textile = yes }
1515.12.17 = { training_fields = yes }
1530.1.1 = { 
	add_core = TRT
	add_permanent_claim = MUG
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1544.1.1 = {
	owner = BNG
	controller = BNG
} # Sur expansion
1545.1.1 = {
	owner = BIK
	controller = BIK
} # Independence regained after death of Sher Shah
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
