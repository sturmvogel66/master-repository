# 3820 - Waren

owner = WRL
controller = WRL
culture = pommeranian
religion = catholic
hre = yes
base_tax = 3
base_production = 3
trade_goods = linen
base_manpower = 2
is_city = yes
capital = "Waren"
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1   = {
	add_core = WRL
}
1436.9.8 = {
	owner = MKL
	controller = MKL
	add_core = MKL
}
1500.1.1 = { road_network = yes }
1530.1.1   = {
	religion = protestant
}
1530.1.4  = {
	bailiff = yes	
}
1695.1.1   = {
	owner = MKL
	controller = MKL
	add_core = MKL
}
1806.7.12  = {
	hre = no
} # The Holy Roman Empire is dissolved
