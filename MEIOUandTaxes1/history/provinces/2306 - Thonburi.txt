# 2306 - Thonburi

owner = AYU
controller = AYU
add_core = AYU
culture = thai
religion = buddhism
capital = "Thonburi"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = fish
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
}
1200.1.1 = { marketplace = yes }
1250.1.1 = { temple = yes }
1400.1.1 = {
	fort_14th = yes
}
1564.2.1 = { add_core = TAU } # Burmese vassal
1584.5.3 = { remove_core = TAU }
1767.4.1 = { unrest = 7 } # The Ayutthaya kingdom began to crumble
1767.4.8 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	remove_core = AYU
	unrest = 0
	base_tax = 9
	base_production = 9			
	base_manpower = 5
	
}
1768.1.1 = { fort_14th = no
	fort_18th = yes
}
1782.1.1 = { 
	capital = "Bangkok" 
	trade_goods = jewelery
}
