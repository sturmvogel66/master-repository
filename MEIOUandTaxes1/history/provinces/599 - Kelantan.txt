#Province: Kelantan
#file name: 599 - Kelantan
# MEIOU-FB Indonesia mod
#Note: if MEIOU ever has a start date prior to 1280 then this province should be part of the
#Srivijaya Empire.
#MEIOU-FB IN updates

owner = KEL
controller = KEL
add_core = KEL
culture = malayan
religion = buddhism
capital = "Kelantan"
base_tax = 4
base_production = 4
base_manpower = 3
is_city = yes
trade_goods = lumber
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
discovered_by = austranesian
hre = no

1375.1.1 = {
	owner = AYU
	controller = AYU
	rename_capital = "Kalantan" 
	change_province_name = "Kalantan"
}
1411.1.1 = {
	owner = KEL
	controller = KEL
	rename_capital = "Kelantan" 
	change_province_name = "Kelantan"
}
1500.1.1 = { religion = sunni }
1506.1.1 = {
	owner = MLC
	controller = MLC
}
1511.9.10 = {
	owner = KEL
	controller = KEL
}
1725.1.1 = {
	owner = TRG
	controller = TRG
	add_core = TRG
}
