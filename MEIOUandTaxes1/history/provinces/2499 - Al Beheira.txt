# 2499 - Al Beheira

owner = MAM
controller = MAM
culture = egyptian
religion = sunni
capital = "Damanhu"
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes
trade_goods = millet
hre = no
discovered_by = MKU
discovered_by = muslim
discovered_by = turkishtech
discovered_by = western
discovered_by = eastern

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = MAM
	#add_core = EGY
}
1530.1.1   = {
	owner = TUR
	controller = TUR
	add_core = TUR
	#remove_core = MAM
} # Invaded by the Ottomans
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.5 = {
	owner = MAM
	controller = MAM
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1571.1.1 = { unrest = 8 } # Local Beja revolt against Ottoman governors
1572.1.1 = { unrest = 0 } # Ottomans crush Beja revolt
1802.5.13  = {
	controller = TUR
	unrest = 8
} # Turkish rule is restored but a few troublesome years follow
1805.1.1 = {
	unrest = 0
	owner = EGY
	controller = EGY
}
1811.6.1   = {
	owner = TUR
	controller = TUR
} # Order is restored
