# 1306 - Miass

owner = BLU
controller = BLU
culture = bashkir
religion = sunni
capital = "Myass"
base_tax = 5
base_production = 5
base_manpower = 3
trade_goods = iron
discovered_by = steppestech
discovered_by = muslim
hre = no

1356.1.1 = {
	add_core = BLU
	add_core = SIB
	add_core = KAZ
	add_core = BSH
	unrest = 3
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = BLU
}
1441.1.1 = {
	owner = SHY
	controller = SHY
	add_core = SHY
	remove_core = GOL
}
1468.1.1 = {
	owner = BSH
	controller = BSH
}
1502.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1515.1.1 = { training_fields = yes }
1530.1.1 = {
	owner = KZH
	controller = KZH
	add_core = KZH
}
1552.1.1   = {
	owner = RUS
	controller = RUS
	add_core = RUS
}
1600.1.1   = {
	culture = russian
	religion = orthodox
	remove_core = KAZ
}
