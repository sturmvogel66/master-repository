# 762 - Nac-Poruk

culture = tupinamba
religion = pantheism
capital = "Nac-Poruk"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 50
native_ferocity = 1
native_hostileness = 3

1510.1.1   = {
	discovered_by = POR
	add_permanent_claim = POR
} # Pedro �lvares Cabral 
1516.1.23 = {
	discovered_by = SPA
}
1554.1.1   = {
	owner = POR
	controller = POR
	change_province_name = "S�o Paulo"
	rename_capital = "S�o Paulo"
	citysize = 560
	culture = portugese
	religion = catholic
	trade_goods = sugar
	base_tax = 3
base_production = 3
}
1600.1.1   = {
	citysize = 1000
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
