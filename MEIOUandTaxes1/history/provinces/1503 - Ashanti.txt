# 1503 - Ashanti

owner = ASH
controller = ASH
culture = akaa
religion = west_african_pagan_reformed
capital = "Kumasi"
base_tax = 8
base_production = 8
base_manpower = 5
is_city = yes
trade_goods = palm
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = ASH
}
