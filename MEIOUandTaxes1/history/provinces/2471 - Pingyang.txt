# 2471 - shanxi_area Pingyang

owner = YUA
controller = YUA
culture = hanyu
religion = confucianism
capital = "Linfen"
trade_goods = rice
hre = no
base_tax = 6
base_production = 6
base_manpower = 4
is_city = yes



discovered_by = chinese
discovered_by = steppestech

1133.1.1 = { mill = yes }
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1280.1.1 = {#Yu Viceroy
	owner = CYU
	controller = CYU
	add_core = CYU
}
#1356.1.1 = {	remove_core = YUA } # Red Turbans
1368.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = CYU
}
1529.3.17 = { 
	marketplace = yes
	courthouse = yes
	road_network = yes
}

1540.1.1  = { fort_14th = yes }

1630.1.1  = { unrest = 6 } # Li Zicheng rebellion
1640.1.1  = { controller = REB } # Financhial crisis
1641.1.1  = { controller = MNG } 
1644.1.1 = {
	controller = MCH
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty
1645.5.27 = { unrest = 0 } # The rebellion is defeated
1740.1.1  = {  }
