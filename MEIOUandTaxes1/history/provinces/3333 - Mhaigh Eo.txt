# No previous file for Mhaigh Eo

owner = CNN
controller = CNN
culture = irish
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = wool
base_manpower = 2
is_city = yes
capital = "Caisle�n an Bharraigh" # Castlebar
discovered_by = western
discovered_by = muslim
discovered_by = eastern

700.1.1 = {
	add_permanent_province_modifier = { name = clan_land duration = -1 }
}
1356.1.1  = {
	add_core = CNN
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1543.7.1 = {
	owner = ENG
	controller = ENG
   	add_core = ENG
} # Burkes submit to England
1642.1.1  = { controller = REB } # Estimated
1642.6.7  = { owner = IRE controller = IRE } # Confederation of Kilkenny
1652.4.1  = { owner = ENG controller = ENG } # End of the Irish Confederates
1655.1.1  = { fort_14th = yes }
1689.3.12 = { controller = REB } # James II Lands in Ireland
1691.9.23 = { controller = ENG } # Capture of Limerick
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
  	remove_core = ENG
}
 # Estimated
1798.8.27 = { controller = REB } # Republic of Connaught
1798.9.8  = { controller = GBR }
