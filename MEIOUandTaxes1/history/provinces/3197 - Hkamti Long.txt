# No previous file for Hkamti Long

owner = MYA
controller = MYA
culture = kachin
religion = animism
capital = "Hkamti Long"
trade_goods = rice
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = steppestech

1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = MYA
	#add_core = SST
	add_core = DLI
}
#1502.1.1 = {
#	controller = SST
#	owner = SST
#	add_core = SST
#	remove_core = MYA
#}
1530.1.1 = { remove_core = AVA remove_core = PEG add_core = TAU }
1557.1.1 = {
	owner = TAU
	controller = TAU
	add_core = TAU
	remove_core = SST
	remove_core = PEG
	remove_core = DLI
}
1599.1.1 = {
		owner = SST
		controller = SST
}
1605.1.1 = {
		owner = TAU
		controller = TAU
}
1732.1.1 = {
		owner = QNG
		controller = QNG
		add_core = QNG
}
1769.12.13 = {
	owner = BRM
	controller = BRM
	add_core = BRM
	remove_core = QNG
}
