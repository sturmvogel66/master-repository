# 393 - Kuwait

owner = BKL
controller = BKL
culture = iraqi
religion = sunni #Dei Gratia
capital = "Kuwait"
trade_goods = wool
hre = no
base_tax = 4
base_production = 4
base_manpower = 1
is_city = yes
discovered_by = muslim
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1   = {
	add_core = BKL
}
1444.1.1 = {
	add_core = IRQ
	dock = yes
}
1530.1.1 = { add_permanent_claim = TUR } 
1547.1.1   = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_claim = TUR
}
1624.1.1   = { controller = PER }
1638.12.24 = { controller = TUR }
1705.1.1   = { capital = "Kuwait" }
