# 789 - Taluhet

culture = het
religion = pantheism
capital = "Taluhet"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 10
native_ferocity = 3
native_hostileness = 0	

1604.1.1   = {
	discovered_by = SPA
	add_core = SPA
} # Juan D�az de Sol�s
1750.1.1   = {
	unrest = 2
   	add_core = LAP
}
1780.1.1   = { unrest = 4 }
1790.1.1   = { unrest = 6 }
1810.5.25  = {
	unrest = 0
}
1816.7.9   = {
	remove_core = SPA
}
1865.1.1   = {
	owner = LAP
	controller = LAP
	capital = "Santa Rosa"
	citysize = 1000
	culture = platean
	religion = catholic
	trade_goods = livestock
	set_province_flag = trade_good_set
}
