# 676 - Hong Kong
# LS - Chinese Civil War

owner = YUA
controller = YUA
culture = hakka
religion = confucianism
capital = "Hong Kong"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
#base_manpower = 0.5
base_manpower = 1.0
citysize = 1250
discovered_by = chinese
discovered_by = steppestech

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1000.1.1   = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1279.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1300.1.1 = {
	citysize = 4000
}
1320.1.1 = {
	remove_core = SNG
}
1351.1.1 = {
	add_core = YUE}
#1356.1.1 = {	remove_core = YUA } # Red Turbans
1356.1.1 = {
	owner = YUE
	controller = YUE
}
1368.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = YUE
	remove_core = YUA
	marketplace = yes
}
1500.1.1 = { citysize = 4750 }
1513.1.1 = { discovered_by = POR } # The area's earliest recorded European visitor was the Portuguese mariner Jorge �lvares.
1550.1.1 = { citysize = 5100 }
1594.1.1 = { unrest = 6 } # Rebels
1599.1.1 = { unrest = 0 }
1600.1.1 = { citysize = 5876 }

1650.1.1 = { citysize = 6130 }
1662.1.1 = {
	owner = YUE
	controller = YUE
	add_core = YUE
	remove_core = MNG
}# Shang Kexi appointed as viceroy
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1680.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = YUE
}
1700.1.1 = { citysize = 7214 }
1750.1.1 = { citysize = 8533 }
1800.1.1 = { citysize = 11400 }
1842.8.29 = {
	owner = GBR		# treaty of Nanking
	controller = GBR
	add_core = GBR
	remove_core = QNG
}
