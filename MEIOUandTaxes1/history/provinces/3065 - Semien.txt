# No previous file for Semien

owner = SEM
controller = SEM
culture = amhara 
religion = jewish
capital = "Dabareq"
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
trade_goods = wax
discovered_by = DAR
discovered_by = ALW
discovered_by = MKU
discovered_by = eastern
discovered_by = muslim
discovered_by = east_african

hre = no

1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = SEM
}
1462.1.1 = {
	owner = ETH
	controller = ETH
} # After a failed invasion of Ethiopia, Semien is all but destroyed
1469.1.1 = {
	owner = SEM
	controller = SEM
} # Jewish kingdom eventually recovers
1486.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1495.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1499.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1510.1.1 = { unrest = 9 } #Raids by Mahfuz Of Zayla
1515.2.1 = { training_fields = yes }
1550.1.1 = { discovered_by = TUR }
1583.1.1 = { unrest = 6 } #Shaykh Ajib expelled from Sennar, Abdallabi discontent grows
1584.1.1 = { unrest = 0 } #Dakin and Ajib reach agreement to end conflict
1612.1.1 = { unrest = 5 } #Funj destroy Ajib at Karkoj
1613.1.1 = { unrest = 0 } #Funj restore autonomy to the Abdallabi
1627.1.1 = {
	owner = ETH
	controller = ETH
	add_core = ETH
} # Beta Israel comes to an end in 1627 during the reign of emperor Susenyos of Ethiopia
1706.1.1 = { unrest = 3 } #Badi III faces revolt over development of slave army at aristocrats expense
1709.1.1 = { unrest = 0 } #Badi III faces revolt over development of slave army at aristocrats expense
1718.1.1 = { unrest = 6 } #Unsa III comes into conflict with aristocrats
1720.1.1 = { unrest = 0 } #Unsa III deposed, new Funj dynasty set up by aristocrats
