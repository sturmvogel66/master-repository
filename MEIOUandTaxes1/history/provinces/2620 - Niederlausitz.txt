# 2620 - Dolna ?u�yca

owner = BRA
controller = BRA
add_core = BRA
capital = "Lubin"
culture = sorbs
religion = catholic
trade_goods = lumber
hre = yes
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1367.1.1  = { #sold by Otto V, Duke of Bavaria to Bohemia
	owner = BOH
	controller = BOH
	add_core = BOH
	add_permanent_province_modifier = {
		name = bohemian_estates
		duration = -1
	}
}
1444.1.1 = {
	add_core = BRA
}
1457.1.1  = { unrest = 5 } # George of Podiebrand had to secure recognition from the German and Catholic towns. Pilsen is very hostile towards him, the Roman church being dominant throughout Pilsen's history.

1500.1.1 = { road_network = yes }
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} #Battle of Mohac where Lajos II dies -> Habsburg succession
1530.1.4  = {
	bailiff = yes	
}
1531.1.2  = { religion = protestant }
1618.4.23 = {
	revolt = {
		type = religious_rebels
		size = 2
	}
	controller = REB
} # Defenstration of Prague
1619.3.1  = {
	revolt = { }
	controller = PAL
	owner = PAL
	add_core = PAL
} #Fredrik of PAL accepts to become King of BOH.
1620.11.8 = {
	controller = BRA
	owner = BOH
	remove_core = PAL
}# Tilly beats the Winterking, but HAB gives the province to Saxony as security against a loan.
1635.5.1  = {
	owner = SAX
	controller = SAX
	add_core = SAX
	remove_core = HAB
}  
1650.1.1  = {
	
}
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved

1815.6.9 = { owner = PRU controller = PRU add_core = PRU remove_core = SAX } # Seeded to Prussia
