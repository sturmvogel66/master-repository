#1155 - Mbamba

owner = KON
controller = KON 
add_core = KON
culture = kongolese
religion = animism
capital = "Mbanza Mbamba"
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = subsistence
discovered_by = central_african

1483.1.1   = { discovered_by = POR } # Diogo C�o
