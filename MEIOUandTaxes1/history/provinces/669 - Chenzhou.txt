# 669 - Huguang Shuangqing

owner = YUA
controller = YUA
culture = miao
religion = animism
capital = "Yuanling"
trade_goods = rice
hre = no
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech


0967.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1133.1.1 = { mill = yes }
1265.1.1 = {
	owner = YUA
	controller = YUA
}
1283.1.1 = {
	capital = "Shunyuan"
	add_core = YUA
	remove_core = SNG
}
1356.1.1 = {
#	remove_core = YUA # Red Turbans
}
1368.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	capital = "Guiyang"
}
1529.3.17 = { 
	marketplace = yes
	courthouse = yes
	road_network = yes
}
1662.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty

