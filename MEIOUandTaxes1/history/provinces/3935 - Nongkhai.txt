# 3935 - Nongkhai

owner = LXA
controller = LXA
add_core = LXA
culture = laotian
religion = buddhism
capital = "Nongkhai"
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no

1133.1.1 = { mill = yes }
1250.1.1 = { temple = yes }
1707.1.1 = {
	owner = VIE
	controller = VIE
	add_core = VIE
	remove_core = LXA
} # Declared independent, Lan Xang was partitioned into 4 kingdoms; Vientiane, Champasak & Luang Prabang + Muang Phuan
1829.1.1 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	unrest = 0
} # Annexed by Siam
