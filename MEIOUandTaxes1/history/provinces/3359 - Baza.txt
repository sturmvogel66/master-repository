# 3359 - Baza + Guadix

owner = GRA		#Mustapha Sa'd King of Granada
controller = GRA
culture = andalucian # culture = eastern_andalucian
religion = sunni
hre = no
base_tax = 8
base_production = 8
trade_goods = wheat
base_manpower = 4
is_city = yes
capital = "Batza" 
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

#1133.1.1 = { mill = yes }
1356.1.1  = {
	add_core = GRA
	set_province_flag = granada_emirate
	set_province_flag = arabic_name
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1469.1.1   = {
	add_core = CAS
} # Union of the Crowns of Castile and Aragon
1482.7.1   = { controller = REB } # While his father is absent becuase of the war, prince Boabdil stages a coup and the Kingdom of Granada is split in two halves, both at war with Castilla
1484.5.1   = { controller = GRA } # Boabdil is captured by the christians and forced to a truce, Muhammad el Zagal returns to Granada and retakes power, extending the war with Castilla.
1492.1.2   = {
	owner = CAS
	controller = CAS
	rename_capital = "Baza" 
	change_province_name = "Baza"
	remove_core = GRA
} # Conquest of Gibraltar by King Enrique of Castilla
1499.12.1  = { unrest = 2 } # Increased pressure from the Inquisition to convert Spanish muslims
1502.2.1   = { unrest = 0 religion = catholic  } # All the subjects of Castilla are forced to convert or emigrate
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no paved_road_network = yes 
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castilla

1523.8.16 = { mill = yes }
1568.12.25 = { controller = REB unrest = 5 } # Sublevation of the morisques in the kingdom of Granada.
1570.10.28 = {
	controller = SPA
	unrest = 0
} # To quell the revolt, the morisques in Granada are forcefully deported to other Spanish territories

1713.4.11  = { remove_core = CAS }
1808.6.6   = { controller = REB }
1811.1.1   = { controller = SPA }
1812.10.1  = { controller = REB }
1813.12.11 = { controller = SPA }
