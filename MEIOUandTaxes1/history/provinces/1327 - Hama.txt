# 1327 - Homs

owner = MAM
controller = MAM
culture = shami
religion = sunni
capital = "Hama"
trade_goods = wheat
hre = no
base_tax = 8
base_production = 8
base_manpower = 3
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

1100.1.1 = { marketplace = yes }
1133.1.1 = { mill = yes }
1200.1.1 = { road_network = yes }
1249.1.1 = {
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = MAM
	add_core = SYR
}
1516.1.1   = { add_core = TUR }
1516.8.28  = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1530.1.5 = {
	owner = SYR
	controller = SYR
	remove_core = TUR
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1831.1.1 = {
	controller = EGY
}
1833.6.1 = {
	owner = EGY
}
1841.2.1  = {
	owner = TUR
	controller = TUR
} # Part of the Ottoman Empire
