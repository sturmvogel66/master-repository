# 1466 - Aha macauc
# GG - 22/07/2008

culture = hopi
religion = totemism
capital = "Aha macauc"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 25
native_ferocity = 2
native_hostileness = 7
discovered_by = mesoamerican
discovered_by = north_american

1533.1.1   = {
	discovered_by = SPA
}
1697.1.1  = {
	owner = SPA
	controller = SPA
	capital = "Mis�on San Miguel"
	citysize = 100
	culture = mexican
	religion = catholic
	set_province_flag = trade_good_set
	trade_goods = maize
} # Dominican missionary Luis Sales
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1817.1.1   = {
	capital = "Mis�on El Descanso"
}
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
