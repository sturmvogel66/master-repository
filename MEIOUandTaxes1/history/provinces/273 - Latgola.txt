owner = LVO
controller = LVO
culture = latvian
religion = catholic
capital = "Rzezyca"
trade_goods = lumber
hre = no
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes
add_core = LVO

discovered_by = western
discovered_by = eastern
discovered_by = steppestech

1530.1.4  = {
	bailiff = yes	
}
1561.11.18 = {
	owner = LIT
	controller = LIT
	add_core = LIT
	add_core = LIV
	remove_core = LVO
} # Wilno Pact
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
	remove_core = POL
} # Union of Lublin
1577.1.1   = { fort_15th = no } # Castle destroyed
1580.1.1   = { fort_14th = yes } # Castle near the hill fort rebuilt, Riekstu hill

1703.1.1   = { fort_14th = yes }
1730.1.1   = {  }
1772.8.5  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = LIT
	remove_core = PLC
} # First partition of Poland
