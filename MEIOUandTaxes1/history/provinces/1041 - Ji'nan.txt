# 1041 - Shantong Wuding

owner = YUA
controller = YUA
culture = hanyu
religion = confucianism
capital = "Ji'nan"
trade_goods = silk
hre = no
base_tax = 9
base_production = 9
base_manpower = 5
is_city = yes
fort_14th = yes
discovered_by = chinese
discovered_by = steppestech

1000.1.1   = {
	add_permanent_province_modifier = {
		name = huang_he_estuary_modifier
		duration = -1
	}
}
##1111.1.1 = { post_system = yes }
1200.1.1 = { paved_road_network = yes courthouse = yes }
1250.1.1 = { temple = yes }
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1350.1.1 = {
	owner = QII
	add_core = QII
}
1356.1.1 = {
	controller = YUA
}
1357.1.1 = {
	owner = YUA
}
1368.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = QII
}
1529.3.17 = { 
	dock = yes
	marketplace = yes
	bailiff = yes
}
1640.1.1 = {
	controller = MCH
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
#	remove_core = MNG
} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
