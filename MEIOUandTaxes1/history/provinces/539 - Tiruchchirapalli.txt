# 539 - Tiruchchirapalli

owner = MAD
controller = MAD
culture = tamil
religion = hinduism
capital = "Tiruchchirapalli"
trade_goods = ebony
hre = no
base_tax = 8
base_production = 8
#base_manpower = 1.5
base_manpower = 3.0
citysize = 18370
discovered_by = indian
discovered_by = muslim
add_local_autonomy = 25

1250.1.1 = { temple = yes }
1350.1.1 = {
	add_permanent_province_modifier = {
		name = pudukkottai_state
		duration = -1
	}
}
1356.1.1  = {
	add_core = MAD
	fort_14th = yes
}
1378.1.1  = {
	owner = VIJ
	controller = VIJ
}
1428.1.1  = { add_core = VIJ
	fort_14th = no
	fort_15th = yes
}
1450.1.1  = { citysize = 22000 }
1498.1.1  = { discovered_by = POR }
1500.1.1  = { citysize = 24000 }
1530.1.1 = {
	#owner = MAD
	#controller = MAD
	#add_core = MAD
	#remove_core = VIJ
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1550.1.1  = { citysize = 25000 }
1565.7.1  = {
	owner = MAD
	controller = MAD
} # The Vijayanagar empire collapses, the Nayaks proclaimed themselves rulers
1570.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1600.1.1  = { citysize = 27000 }
1650.1.1  = { citysize = 31000 }
1661.1.1  = {
	controller = BIJ
} # Bijapur Expansion
1662.1.1  = {
	owner = BIJ
} # Bijapur Expansion
1685.1.1 = {
	controller = MUG
}
1686.1.1 = {
	owner = MUG
}
1700.1.1 = { citysize = 35000 }
1710.1.1 = {
	owner = KRK
	controller = KRK
	add_core = KRK
} # Nawab of Arcot / Carnatic
1741.3.1 = {
	controller = MAR
} # Falls to the Marathas
1743.3.1 = {
	controller = KRK
} # Falls to the Nizam
1750.1.1 = { citysize = 47000 }
1752.6.1 = {
	controller = GBR
} # Conquered by British
1754.1.1 = {
	controller = KRK
}
1800.1.1 = { citysize = 80000 }
1801.1.1 = {
	owner = GBR
	controller = GBR
}
