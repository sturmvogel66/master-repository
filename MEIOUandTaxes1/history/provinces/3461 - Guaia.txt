# No previous file for Guai�

culture = ge
religion = pantheism
capital = "Guai�"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 45
native_ferocity = 3
native_hostileness = 1

1580.1.1   = { discovered_by = POR }
1682.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 280
	culture = portugese
	religion = catholic
	trade_goods = iron
	change_province_name = "Goias"
	rename_capital = "Goias"
}

1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
