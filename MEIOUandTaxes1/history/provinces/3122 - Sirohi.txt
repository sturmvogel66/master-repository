# No previous file for Sirohi

owner = MAW
controller = MAW
culture = marwari
religion = hinduism
capital = "Barmer"
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
add_local_autonomy = 50


1120.1.1 = { textile = yes }
1356.1.1  = {
	add_core = MAW
}

1405.1.1 = {
	add_permanent_province_modifier = {
		name = sirohi_state
		duration = -1
	}
}
1438.1.1 = {
	owner = MEW
	controller = MEW
} #Conquered by Mewar after Rathore, Sisodiya cooperation breaks down in plots and murder.
1459.1.1 = {
	owner = GUJ
	controller = GUJ
	add_core = GUJ
}
1515.12.17 = { training_fields = yes }
1530.1.1 = { 
	add_permanent_claim = MUG
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1538.1.1 = {
	owner = MAW
	controller = MAW
}
1540.1.1 = {
	owner = GUJ
	controller = GUJ
}
1573.6.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = GUJ
} # Conquered by Akbar
1690.1.1  = { discovered_by = ENG }
1707.3.15 = {
	controller = MEW
} # Jordphur is recaptured & the Mughals are driven out
1707.4.1  = {
	controller = MAW
	owner = MAW
} # Jodphur is recaptured & the Mughals are driven out
1707.5.12 = { discovered_by = GBR }
