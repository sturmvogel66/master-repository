# 3006 - Tajura

owner = ADA
controller = ADA
add_core = ADA
culture = afar #afar
religion = sunni
capital = "Tajura"
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes
trade_goods = wool
hre = no
discovered_by = ALW
discovered_by = MKU
discovered_by = ADA
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = indian
discovered_by = east_african

1200.1.1 = { road_network = yes }
1499.1.1   = { discovered_by = POR }
1515.2.1 = { training_fields = yes }
1530.1.1 = { owner = AFA controller = AFA add_core = AFA remove_core = ADA }
1550.1.1   = { discovered_by = TUR }
1554.1.1 = { unrest = 9 } # Invasion by Oromo causes widespread destruction
1559.1.1 = { unrest = 8 } # Invasion by Galawdewos
1562.1.1 = { unrest = 5 } # Invasion by Oromo
1567.1.1 = { unrest = 5 } # Invasion by Oromo
1568.1.1 = {
	unrest = 0
	owner = HAR
	controller = HAR
}
1573.1.1   = {
	owner = AFA
	controller = AFA
	add_core = AFA
	remove_core = HAR
}
1648.1.1   = { unrest = 7 } # Oromo Raids
1649.1.1   = { unrest = 0 }
1672.1.1 = {
	owner = HAR
	controller = HAR
	add_core = HAR
} #Awsa is ended by invading Mudaito
1734.1.1 = {
	owner = AFA
	controller = AFA
} #Mudaito establish new sultanate
