# 1296 - Saratow

owner = WHI
controller = WHI       
culture = tartar
religion = sunni
hre = no
base_tax = 3
base_production = 3
trade_goods = wool 
base_manpower = 3
is_city = yes
capital = "Uvek"
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1  = {
	add_core = AST
	add_core = WHI
	unrest = 3
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
	unrest = 0
}
1502.6.1   = {
	owner = KAZ
	controller = KAZ
	add_core = KAZ 
	culture = kazani
	remove_core = GOL
} # Independance of Astrakhan
1515.1.1 = { training_fields = yes }
1530.1.4  = {
	bailiff = yes	
}
1556.1.1   = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = KAZ
} # Conquest of the Khanante by Ivan Grozny
1590.7.1  = {
	capital = "Saratov"
} # Construction of the Saratow fortress 
1598.1.1  = {
	unrest = 5
} # "Time of troubles"
1600.1.1  = {
	culture = russian
	religion = orthodox
}
1613.1.1  = { unrest = 0 } # Order returned, Romanov dynasty
1670.1.1  = { unrest = 8 } # Stepan Razin
1671.1.1  = { unrest = 0 } # Razin is captured

1773.1.1  = { revolt = { type = nationalist_rebels size = 2 leader = "Yemelyan Pugachev" } controller = REB } # Emelian Pugachev, Cossack insurrection
1774.9.14 = { revolt = {} controller = RUS } # Pugachev is captured

