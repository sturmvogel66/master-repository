# 3441 - Lipe

culture = diaguita
religion = pantheism
capital = "Lipe"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 20
native_ferocity = 1
native_hostileness = 8
discovered_by = south_american

1356.1.1 = {
	add_core = AYM
}
1523.1.1   = {
	owner = INC
	controller = INC
	add_core = INC
	remove_core = AYM
	trade_goods = silver
	is_city = yes
	paved_road_network = yes
	bailiff = yes
	constable = yes
	marketplace = yes
}
1529.1.1 = {
	owner = CZC
	controller = CZC
	add_core = QUI
	add_core = CZC
	remove_core = INC
}
1535.1.1  = {
	discovered_by = SPA
	add_core = SPA
	owner = SPA
	controller = SPA
	religion = catholic
	culture = castillian
}
1750.1.1   = {
   	add_core = BOL
	culture = peruvian
} # Spanish administration required all trade to pass via Lima, extracted taxes
1809.1.1   = {
	owner = BOL
	controller = BOL
}
1825.8.6   = {
	remove_core = SPA
}
