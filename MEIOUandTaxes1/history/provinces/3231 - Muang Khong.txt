# No previous file for Champassak

owner = LXA
controller = LXA
add_core = LXA
culture = laotian
religion = buddhism
capital = "Champassak"
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no

1356.1.1 = {
	fort_14th = yes
}
1707.1.1 = {
	owner = VIE
	controller = VIE
	add_core = VIE
	remove_core = LXA
	fort_14th = no
	fort_17th = yes
} # Declared independent, Lan Xang was partitioned into 4 kingdoms; Vientiane, Champasak & Luang Prabang + Muang Phuan
1713.1.1 = {
	owner = CHK
	controller = CHK
	add_core = CHK
	remove_core = VIE
}
1811.1.1 = { unrest = 5 } # The Siamese-Cambodian Rebellion
1812.1.1 = { unrest = 0 }
1829.1.1 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	unrest = 0
} # Annexed by Siam
1893.1.1 = {
	owner = FRA
	controller = FRA
    	unrest = 0
}
