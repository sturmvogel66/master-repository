# 545 - Haydarabad

owner = TLG
controller = TLG
culture = telegu
religion = hinduism
capital = "Hyderabad"
trade_goods = livestock
hre = no
base_tax = 10
base_production = 10
base_manpower = 5
is_city = yes
#fort_14th = yes

discovered_by = indian
discovered_by = muslim

1356.1.1  = {
	add_core = GOC
	add_core = TLG
}
1365.1.1  = {
	owner = BAH
	controller = BAH
	add_core = GOC
	add_core = TLG
}
1405.1.1  = {
	add_core = BAH
}
1515.12.17 = { training_fields = yes }
1518.1.1  = {
	owner = GOC
	controller = GOC
	add_core = GOC
	remove_core = BAH
} # The Breakup of the Bahmani sultanate
1530.3.17 = {
	bailiff = yes
	marketplace = yes
}
1550.1.1  = { fort_15th = yes }
1591.1.1  = { 
	fort_14th = no
	fort_16th = yes
	trade_goods = jewelery
}
1600.1.1  = {  discovered_by = turkishtech }
1687.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Conquered by the mughal emperor Aurangzeb

1707.5.12 = { discovered_by = GBR }
1724.1.1 = {
	owner = HYD
	controller = HYD
	add_core = HYD
	remove_core = MUG
	capital = "Haydar�b�d"
} # Asif Jah declared himself Nizam-al-Mulk of Haydar�b�d

1800.1.1 = {  }
