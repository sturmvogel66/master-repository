# 1518 - Ihorombe

owner = BAA
controller = BAA
add_core = BAA
culture = vazimba
religion = animism
capital = "Ihorombe"
is_city = yes
trade_goods = rice
base_tax = 4
base_production = 4
base_manpower = 2
hre = no
discovered_by = ANT
discovered_by = BAA
discovered_by = BET
discovered_by = BNA
discovered_by = MER
discovered_by = MHF
discovered_by = MNB
discovered_by = SIH
discovered_by = TAM

1640.1.1   = {
	owner = MHF
	controller = MHF
	add_core = MHF
}
1800.1.1   = {
	owner = BAA
	controller = BAA
}
1840.1.1   = {
	owner = MER
	controller = MER
	add_core = MER
}
1885.12.17 = {
	add_core = FRA
}
1897.2.28  = {
	owner = FRA
	controller = FRA
}
