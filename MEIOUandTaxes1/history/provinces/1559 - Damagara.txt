# 1559 - Damagaram

culture = tuareg		
religion = sunni		 
capital = "Mashina"
base_tax = 2
base_production = 2
base_manpower = 1
native_size = 60
native_ferocity = 4.5
native_hostileness = 9
trade_goods = wool
hre = no
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1500.1.1  = {
	owner = SON
	controller = SON
	add_core = SON
	is_city = yes
}
1591.3.15 = {
	owner = AIR
	controller = AIR
}
