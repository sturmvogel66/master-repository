# 3517 - B�ak'

# owner = MAY
# controller = MAY
# add_core = MAY
culture = cholan
religion = mesoamerican_religion
capital = "B�ak'" 
base_tax = 3
base_production = 3
base_manpower = 2
trade_goods = maize 
discovered_by = mesoamerican
hre = no

1517.1.1   = {
	discovered_by = SPA
}
1522.1.1   = {
	owner = SPA
	controller = SPA
	add_core = SPA
	
	
}
1546.1.1   = {
	add_core = SPA
	capital = "Palenque" 
	citysize = 500
}
1750.1.1   = {
	add_core = MEX
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
