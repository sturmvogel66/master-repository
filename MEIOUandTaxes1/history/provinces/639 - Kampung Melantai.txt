# Province: Kampung Melantai
# file name: 639 - Kampung Melantai

owner = KUT
controller = KUT
culture = dayak
religion = vajrayana
capital = "Samarinda"
trade_goods = lumber
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1356.1.1 = { add_core = KUT }
1521.1.1 = { discovered_by = POR }
1600.1.1 = { religion = sunni }
1606.1.1 = { discovered_by = NED }
1844.1.1 = { owner = NED controller = NED }
