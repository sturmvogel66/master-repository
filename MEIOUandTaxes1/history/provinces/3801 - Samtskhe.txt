# 3801 - Samtskhe

owner = GEO
controller = GEO
culture = ge_armenian
religion = orthodox
capital = "Artaani"
trade_goods = wool
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1  = {
	add_core = GEO
}
1444.1.1  = {
	add_core = SMT
}
1466.1.1  = {
	owner = SMT
	controller = SMT
	remove_core = GEO
	fort_14th = yes 
	temple = yes
	road_network = yes
}
1530.1.1 = { add_permanent_claim = TUR } #Ruled by local Turkish Beys
1530.1.4  = {
	bailiff = yes	
}
1550.1.1 = { fort_14th = no fort_15th = yes }
1551.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_claim = TUR
}
1600.1.1 = {
	religion = sunni
}
1658.1.1 = { controller = REB } # Revolt of Abaza Hasan Pasha
1659.1.1 = { controller = TUR }
