#1524 - Fezzan

owner = FZA
controller = FZA
culture = toubous
religion = sunni
capital = "Al Qatrun"
native_size = 20
native_ferocity = 4.5
native_hostileness = 9
base_tax = 2
base_production = 2
base_manpower = 2
trade_goods = livestock
hre = no
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1   = {
	add_core = FZA
}
1551.1.1 = {
	owner = TRP
	controller = TRP
	add_core = TRP
} # Conquered by Ottoman troops
1711.1.1 = {
	owner = TRP
	controller = TRP
	add_core = TRP
	remove_core = TUR
} # The Karamanli pashas gain some autonomy as the Ottoman central power weakens
