# 1206 - Shewa

owner = ETH
controller = ETH
culture = amhara
religion =coptic
capital = "Tegulet"
base_tax = 6
base_production = 6
base_manpower = 3
citysize = 3000
trade_goods = gold
hre = no
discovered_by = ETH
discovered_by = ADA
discovered_by = ALW
discovered_by = MKU
discovered_by = SID
discovered_by = muslim
discovered_by = east_african

1100.1.1 = { marketplace = yes }
1356.1.1 = {
	add_core = ETH
}
1486.1.1 = { unrest = 5 add_core = ADA } #Raids by Mahfuz Of Zayla
1495.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1499.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1510.1.1 = { unrest = 9 } #Raids by Mahfuz Of Zayla
1514.1.1 = {
	owner = ADA
	controller = ADA
	unrest = 6
} #Mahfuz drives Ethiopians out of Ifat region
1515.2.1 = { training_fields = yes }
1517.7.1 = {
	owner = ETH
	controller = ETH
} #Lebna Dengel occupies region during campaign that defeats Mahfuz
1527.1.1 = { unrest = 5 } #Raids by Ahmad Gran
1529.1.1 = { unrest = 7 } #Raids by Ahmad Gran
1530.1.1 = { religion = sunni } #Spread of Islam leads to shift in religion affiliation of region
1531.1.1 = { unrest = 7 } #Raids by Ahmad Gran
1549.1.1 = { unrest = 5 } #Invasion by Oromo
1550.1.1 = { discovered_by = TUR }
1553.1.1 = { unrest = 5 } #Invasion by Adal 
1558.1.1 = { unrest = 5 } #Invasion by Adal 
1564.1.1 = { controller = REB } #Oromo seize control of region
1566.1.1 = { controller = ETH } #Sarsa Dengel defeats Oromo invaders
1573.1.1 = { unrest = 5 } #Invasion by Oromo
1578.1.1 = { unrest = 5 } #Invasion by Oromo
1588.1.1 = { unrest = 5 } #Invasion by Oromo
