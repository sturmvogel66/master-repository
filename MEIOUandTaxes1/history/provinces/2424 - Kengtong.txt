# 2424 - Kengtong

owner = SST
controller = SST
culture = lanna
religion = buddhism
capital = "Kengtong"
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
trade_goods = tin
discovered_by = chinese
discovered_by = indian
hre = no

1356.1.1 = {
	add_core = AVA
	add_core = SST
	add_core = PEG
}
1530.1.1 = { remove_core = AVA remove_core = PEG add_core = TAU }
1557.1.1 = {
	owner = TAU
	controller = TAU
	add_core = TAU
	remove_core = AVA
	remove_core = PEG
	rename_capital = "Chiang Tung" 
	change_province_name = "Chiang Tung"
} # The Shan dynasty is overthrown
1581.1.1 = {
	owner = SST
	controller = SST
  	remove_core = TAU
} # Very loosely controlled
1605.1.1 = {
	owner = TAU
	controller = TAU
}
1732.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
}
1739.1.1 = { unrest = 5 } # Rebellion
1740.1.1 = { unrest = 0 }
1752.2.28 = {
	owner = BRM
	controller = BRM
	add_core = BRM
	remove_core = TAU
}
1852.1.1 = { unrest = 7 }	# out of control after burmese defeated by british
1885.1.1 = {
	owner = GBR
	controller = REB
}
1890.1.1 = { controller = GBR }
