# 883 - Guamare

owner = GMR
controller = GMR
add_core = GMR
is_city = yes
culture = chichimecha
religion = aztec_reformed
capital = "Quanaxhuato"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1.0
native_size = 20
native_ferocity = 4
native_hostileness = 8


1542.1.1   = {
	
} # Francisco Vázquez de Coronado y Luján
1690.1.1   = {
	owner = SPA
	controller = SPA
	citysize = 20
	culture = castillian
	capital = "Guanajuato"
}
1715.1.1   = {
	add_core = SPA
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cordóba
1823.1.1   = {
	culture = american
	religion = protestant
	citysize = 1000
	add_core = TEX
} # Anglo-american authorized
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
#1836.3.2   = {
#	owner = TEX
#	controller = TEX
#}
#1836.4.21  = {
#	unrest = 0
#} # Victory at the battle of San Jacinto, Treaty of Velasco
