# 2307 - Guines

owner = ARS
controller = ARS
culture = picard
religion = catholic
capital = "Boulonne"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = fish

discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

1300.1.1 = { road_network = yes }
1329.1.1   = {
	owner = AUV
	controller = AUV
}
1356.1.1   = {
	add_core = FRA
	add_core = BUR
	add_core = FLA
	add_core = ARS
	add_core = AUV
}
1361.11.21 = {
	owner = ARS
	controller = ARS
	remove_core = AUV
}
1369.6.19  = {
	owner = BUR
	controller = BUR
}
1444.1.1 = { remove_core = FRA }
1477.1.5 = { add_core = FRA }
1482.3.27 = {
	owner = FRA
	controller = FRA
	#add_core = HAB
} # Charles the Bold dies and transfers Bourgogne to France
1500.1.1 = { road_network = yes }

1515.1.1   = { fort_14th = yes }
1530.1.2 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1544.9.14  = {
	owner = ENG
	controller = ENG
} # Captured by Henri VIII
1550.3.24  = {
	owner = FRA
	controller = FRA
} # Treaty of Boulogne
1559.5.12  = { unrest = 3 } # New bishoprics established in the Lowlands create an outrage
1566.8.1   = { unrest = 4 } # 'Beeldenstorm' at hand
1566.8.10  = { controller = REB } # 'Beeldenstorm' also hits parts of Artois
1567.1.8   = { controller = SPA } # Spain is back in control
1569.1.1   = { unrest = 7 } # The Duke of Alba reforms the taxation system ('tiende penning')
1570.1.1   = { unrest = 11  } # The Duke of Alba reforms the penal system, 'Blood Council' (Bloedraad) established
1577.2.12  = { unrest = 5 } # The 'Perpetual Edict' (Eeuwig Edict) is accepted by Don Juan
1579.1.6   = { unrest = 0 } # The Union of Arras is formed
1610.1.1   = { capital = "Lille" } # Lille becomes more important than Arras
1630.1.1   = { fort_14th = no fort_15th = yes }
1635.1.1   = { controller = FRA } # French troops capture parts of the Southern Lowlands
1648.1.30  = { controller = SPA } # Peace of M�nster/Westphalia
1650.1.1   = { add_core = FRA } # Chambers of Reunion
1658.6.14  = { controller = FRA } # French troops capture most of the area
1659.10.28 = { owner = FRA remove_core = SPA hre = no } # Peace of the Pyrennees
1670.10.15 = { fort_15th = no fort_16th = yes  } # Vauban's fort in Lille is finished, state of the art for its time, troops aimed at the Lowlands based there
