# 1159 - Mapungo

owner = NDO
controller = NDO
add_core = NDO
culture = mbundu
religion = animism
capital = "Mapungo"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = ivory
hre = no
discovered_by = LOA
discovered_by = KON
discovered_by = NDO
discovered_by = central_african

1628.1.1   = { discovered_by = POR } # Diogo C�o
