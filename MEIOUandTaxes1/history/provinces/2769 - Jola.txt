# 2769 - Jola

culture = senegambian
religion = west_african_pagan_reformed
capital = "Jola"
trade_goods = unknown
native_size = 50
native_ferocity = 4.5
native_hostileness = 9
discovered_by = soudantech
discovered_by = sub_saharan
hre = no
