# 679 - fujian_area Xiangcheng

owner = YUA
controller = YUA
culture = minyu
religion = confucianism
capital = "Zhangzhou"
trade_goods = chinaware
hre = no
base_tax = 9
base_production = 9
base_manpower = 5
is_city = yes

discovered_by = chinese
discovered_by = steppestech

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1279.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1315.1.1 = {	#Viceroy of Fujian
	owner = CEN
	controller = CEN
	add_core = CEN
}
1320.1.1 = {
	remove_core = SNG
	add_core = CMN
}
#1356.1.1 = {	remove_core = YUA } # Red Turbans
1368.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = CEN
	remove_core = CMN
	remove_core = YUA
}
1515.1.1 = { }
1529.3.17 = { 
	dock = yes
	marketplace = yes
	bailiff = yes
	courthouse = yes
	fine_arts_academy = yes
}
1662.1.1 = {
	owner = CMN
	controller = CMN
	add_core = CMN
	remove_core = MNG
}# Geng Jingzhong appointed as viceroy
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1680.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = CMN
}
