# 2124 - Strumica

owner = BYZ
controller = BYZ
culture = bulgarian
religion = orthodox
capital = "Strumica"
trade_goods = wine
hre = no
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1350.1.1  = {
	owner = VBZ
	controller = VBZ
	add_core = VBZ
	add_permanent_claim = BYZ
	add_permanent_claim = SER
	add_core = BUL
	add_local_autonomy = 15
}
1371.2.17 = {
	owner = OTT
	controller = OTT
	add_core = OTT
	add_local_autonomy = -15
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1400.1.1 = { remove_core = BYZ add_permanent_claim = BYZ }
1444.1.1  = {
	remove_core = SER
}
1515.2.1 = { training_fields = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1555.1.1  = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { unrest = 0  }
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
1689.1.1  = { controller = REB } # Karposh uprising against Ottoman rule
1690.1.1  = { controller = TUR }
1750.1.1  = { add_core = GRE }
1821.3.1  = { controller = REB }
1829.1.1  = { controller = TUR }
