# NNZ - Nanzhao

government = chinese_monarchy_5 government_rank = 3
# centralization_decentralization = -5
mercantilism = 0.0
technology_group = chinese
religion = vajrayana
primary_culture = yizu
capital = 2510

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 1 }
}

