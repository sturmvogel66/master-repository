# LOU - Louisiana
# 2010-jan-21 - FB - HT3 changes

government = constitutional_republic
mercantilism = 0.0
technology_group = western
primary_culture = acadian
religion = catholic
capital = 922	# Mobile

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1720.1.1 = { capital = 921 } # Biloxi
