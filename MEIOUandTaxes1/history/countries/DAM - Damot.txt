# DAM - Damot

government = feudal_monarchy government_rank = 3
mercantilism = 10
primary_culture = amhara
religion =coptic
technology_group = east_african unit_type = soudantech
capital = 1207

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1344.1.1 = {
	monarch = {
		name = "Egwale"
		dynasty = "Solomonid"
		DIP = 3
		ADM = 4
		MIL = 3
	}
}

1530.1.1 = {
	monarch = {
		name = "Egwale I"
		dynasty = "Solomonid"
		DIP = 3
		ADM = 4
		MIL = 3
	}
}
