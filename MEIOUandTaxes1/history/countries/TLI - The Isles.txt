# TLI - The Isles

government = feudal_monarchy government_rank = 4
mercantilism = 0.0
primary_culture = norse_gaelic
religion = catholic
technology_group = western
capital = 1417


1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1303.1.1 = {
	monarch = {
		name = "Angus"
		dynasty = "Mac Domhnaill"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1329.1.1 = {
	monarch = {
		name = "Eoin"
		dynasty = "Mac Domhnaill"
		ADM = 1
		DIP = 3
		MIL = 2
	}
}

1386.1.1 = {
	monarch = {
		name = "Domhnall"
		dynasty = "Mac Domhnaill"
		ADM = 2
		DIP = 4
		MIL = 2
	}
}

1423.1.1 = {
	monarch = {
		name = "Alasdair"
		dynasty = "Mac Domhnaill"
		ADM = 4
		DIP = 4
		MIL = 5
	}
}

1449.1.1 = {
	monarch = {
		name = "Eoin II"
		dynasty = "Mac Domhnaill"
		ADM = 3
		DIP = 1
		MIL = 1
	}
}
