# ERF - Erfurt

government = imperial_city
mercantilism = 0.0
primary_culture = high_saxon
religion = catholic
technology_group = western
capital = 3741
fixed_capital = 3741

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}


1349.1.1 = {
	monarch = {
		name = "Balthasar"
		DIP = 4
		ADM = 1
		MIL = 3
	}
}
