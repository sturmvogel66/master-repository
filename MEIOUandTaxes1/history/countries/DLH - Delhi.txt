# DLH - Delhi

government = indian_monarchy government_rank = 6
mercantilism = 0.0
primary_culture = kanauji
religion = sunni
technology_group = indian
capital = 2163	# Delhi
fixed_capital = 2163 # Delhi
historical_friend = KSH

1000.1.1 = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
	set_country_flag = indian_sunni_state
}

1329.1.1 = { #ruled much later but this gives him the correct age
	monarch = {
		name = "Firuz Shah"
		dynasty = "Thugluq"
		ADM = 1
		DIP = 1
		MIL = 1
	}
	add_country_modifier = { name = obstacle_feudal_fragmentation duration = -1 }
	add_country_modifier = { name = obstacle_feuding duration = -1 }
	add_country_modifier = { name = obstacle_succession duration = -1 }
	add_country_modifier = { name = obstacle_shifting_loyalties duration = -1 }
}

1349.1.1 = {
	heir = {
		name = "Tughluq Khan"
		monarch_name = "Tughluq Khan"
		dynasty = "Thugluq"
		birth_date = 1349.1.1
		death_Date = 1389.1.1
		claim = 45
		ADM = 0
		DIP = 0
		MIL = 0
	}
}

1388.1.1 = {
	monarch = {
		name = "Tughluq Khan"
		dynasty = "Thugluq"
		ADM = 0
		DIP = 0
		MIL = 0
	}
	heir = {
		name = "Abu Bakr Shah"
		monarch_name = "Abu Bakr Shah"
		dynasty = "Thugluq"
		birth_date = 1350.1.1
		death_Date = 1390.1.1
		claim = 95
		ADM = 0
		DIP = 1
		MIL = 0
	}
}

1389.1.1 = {
	monarch = {
		name = "Abu Bakr Shah"
		dynasty = "Thugluq"
		ADM = 0
		DIP = 1
		MIL = 0
	}
	heir = {
		name = "Muhammad Shah"
		monarch_name = "Muhammad Shah"
		dynasty = "Thugluq"
		birth_date = 1350.1.1
		death_Date = 1394.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1390.1.1 = {
	monarch = {
		name = "Muhammad Shah"
		dynasty = "Thugluq"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir = {
		name = "Nusrat Shah"
		monarch_name = "Nusrat Shah"
		dynasty = "Thugluq"
		birth_date = 1380.1.1
		death_Date = 1414.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1394.1.1 = {
	monarch = {
		name = "Nusrat Shah"
		dynasty = "Thugluq"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

 

1414.1.1 = {
	heir = {
		name = "Mubarrak"
		monarch_name = "Mubarrak Shah II"
		dynasty = "Sayyid"
		birth_date = 1380.1.1
		death_Date = 1435.1.1
		claim = 95
		ADM = 4
		DIP = 3
		MIL = 2
	}
}

1421.1.1 = {
	monarch = {
		name = "Mubarrak Shah II"
		dynasty = "Sayyid"
		ADM = 4
		DIP = 3
		MIL = 2
	}
}

1421.1.1 = {
	heir = {
		name = "Mohammed"
		monarch_name = "Mohammed Shah IV"
		dynasty = "Sayyid"
		birth_Date = 1415.1.1
		death_date = 1445.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1435.1.1 = {
	#add_country_modifier = { name = wrath_of_god duration = -1 }
	monarch = {
		name = "Mohammed Shah IV"
		dynasty = "Sayyid"
		ADM = 3
		DIP = 3
		MIL = 3
		leader = { name = "Mohammed Shah Sayyid"		type = general	rank = 0	fire = 0	shock = 1	manuever = 0	siege = 0 }
	}
}

1435.1.1 = {
	heir = {
		name = "Aladdin Alam"
		monarch_name = "Aladdin Alam Shah"
		dynasty = "Sayyid"
		birth_date = 1420.1.1
		death_Date = 1451.4.20
		claim = 95
		ADM = 1
		DIP = 4
		MIL = 2
		leader = { name = "Aladdin Sayyid"		type = general	rank = 0	fire = 0	shock = 0	manuever = 0	siege = 0 }
	}
}

1445.1.1 = {
	monarch = {
		name = "Aladdin Alam Shah"
		dynasty = "Sayyid"
		ADM = 1
		DIP = 4
		MIL = 2
		leader = { name = "Aladdin Sayyid"		type = general	rank = 0	fire = 0	shock = 0	manuever = 0	siege = 0 }
	}
}

1451.4.20 = {
	monarch = {
		name = "Bahl�l"
		dynasty = "Lodi"
		ADM = 4
		DIP = 5
		MIL = 6
		leader = { name = "Bahl�l Ludi"		type = general	rank = 0	fire = 4	shock = 4	manuever = 4	siege = 1 }
	}
	remove_country_modifier = obstacle_feudal_fragmentation
	remove_country_modifier = obstacle_feuding
	remove_country_modifier = obstacle_succession
	remove_country_modifier = obstacle_shifting_loyalties
}

1451.4.20 = {
	heir = {
		name = "Nizam Khan"
		monarch_name = "Sikandar Sh�h"
		dynasty = "Lodi"
		birth_Date = 1450.1.1
		death_Date = 1517.11.21
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 4
		leader = { name = "Sikandar Sh�h"	type = general	rank = 0	fire = 3	shock = 3	manuever = 4	siege = 0 }
	}
}


1489.7.17 = {
	monarch = {
		name = "Sikandar Sh�h"
		dynasty = "Lodi"
		ADM = 1
		DIP = 2
		MIL = 4
		leader = { name = "Sikandar Sh�h"	type = general	rank = 0	fire = 3	shock = 3	manuever = 4	siege = 0 }
	}
}

1489.7.17 = {
	heir = {
		name = "Ibr�h�m Sh�h"
		monarch_name = "Ibr�h�m Sh�h"
		dynasty = "Lodi"
		birth_Date = 1489.1.1
		death_Date = 1526.4.21
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 1
		leader = { name = "Ibr�h�m Sh�h"	type = general	rank = 0	fire = 2	shock = 2	manuever = 2	siege = 0 }
	}
}
		
1517.11.21 = {
	monarch = {
		name = "Ibr�h�m Sh�h"
		dynasty = "Lodi"
		ADM = 1
		DIP = 1
		MIL = 1
		leader = { name = "Ibr�h�m Sh�h"	type = general	rank = 0	fire = 2	shock = 2	manuever = 2	siege = 0 }
	}
	capital = 524
}

1526.4.21 = {
capital = 522	# Delhi
}

1553.1.1 = {
		monarch = {
		name = "Ibr�h�m Sh�h"
		dynasty = "Suri"
		ADM = 3
		DIP = 4
		MIL = 2
	}
}

1556.9.1 = {
		monarch = {
		name = "Hemu"
		dynasty = "Rewari"
		ADM = 6
		DIP = 4
		MIL = 6
		leader = {  name = "Hemu"	type = general	rank = 0	fire = 3	shock = 4	manuever = 2	siege = 1   }
		}
	religion = hinduism
}
	
1556.11.5 = { religion = sunni }
