# KAT - Kathiawar
# 2010-jan-16 - FB - HT3 changes

government = tribal_monarchy government_rank = 2
# innovative_narrowminded = 4
mercantilism = 0.0
technology_group = indian
religion = hinduism
primary_culture = saurashtri
capital = 503	# Kutch
fixed_capital = 503	# Kutch

#This was really an area with many small rajput tribes and therefore this state is a bit of an abstraction. Should it manage to change government type it's to be assumed one of the tribes prevailed over the other's.

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

#Jams of Kutch:
1386.1.1 = {
	monarch = {
		name = "Amarji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1429.1.1 = {
	monarch = {
		name = "Bhenji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1472.1.1 = {
	monarch = {
		name = "Hamirji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

#Nawanagar Branch

1540.1.1 = {
	monarch = {
		name = "Ravalj Lakhaji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1562.1.1 = {
	monarch = {
		name = "Vibhaji Ravalji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1569.1.1 = {
	monarch = {
		name = "Sataji Vibhaji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1608.1.1 = {
	monarch = {
		name = "Jasaji Sataji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1624.1.1 = {
	monarch = {
		name = "Lakhaji Ajoji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1645.1.1 = {
	monarch = {
		name = "Ranmalji Lakhaji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1661.1.1 = {
	monarch = {
		name = "Rajsinhji I Ajoji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1673.1.1 = {
	monarch = {
		name = "Tamachi Rajsinhji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1690.1.1 = {
	monarch = {
		name = "Lakhaji II Tamachi"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1710.1.1 = {
	monarch = {
		name = "Rajsinhji II Lakhaji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1718.1.1 = {
	monarch = {
		name = "Hardholji Lakhaji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1727.1.1 = {
	monarch = {
		name = "Tamachi Hardholji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1743.1.1 = {
	monarch = {
		name = "Lakhaji III Tamachi"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1768.1.1 = {
	monarch = {
		name = "Jasaji Lakhaji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1814.1.1 = {
	monarch = {
		name = "Sataji Lakhaji"
		dynasty = "Jadeja"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}
