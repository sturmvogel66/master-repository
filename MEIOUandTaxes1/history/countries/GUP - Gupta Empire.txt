# Country: GUP - Gupta Empire
# MEIOU-FB India
# Unification nation
# MEIOU-GG Govenemnt changes
# 2010-jan-15 - FB - HT3 changes
# 2013-aug-07 - GG - EUIV changes

government = indian_monarchy
government_rank = 6
# aristocracy_plutocracy = -3
# centralization_decentralization = 0
# innovative_narrowminded = 0
mercantilism = 0.1 # mercantilism_freetrade = -3
# offensive_defensive = -2
# land_naval = -1
# quality_quantity = -1
# serfdom_freesubjects = -1
# isolationist_expansionist = -1
# secularism_theocracy = 0
primary_culture = bihari
religion = hinduism
add_accepted_culture = bengali
add_accepted_culture = bihari
add_accepted_culture = oriya
add_accepted_culture = mewari
add_accepted_culture = dhundari
add_accepted_culture = marwari
add_accepted_culture = harauti
add_accepted_culture = malvi
add_accepted_culture = gujarati
add_accepted_culture = saurashtri
add_accepted_culture = bhil
add_accepted_culture = jati
add_accepted_culture = tomara
add_accepted_culture = meo
add_accepted_culture = gondi
add_accepted_culture = nagpuri
add_accepted_culture = sambalpuri
add_accepted_culture = pahari
add_accepted_culture = nepali
add_accepted_culture = marathi
add_accepted_culture = khandeshi
technology_group = indian
capital = 558 # (Patna)

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 0 }
}
