# MUR - Principality of Murom

government = feudal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = russian
religion = orthodox
technology_group = eastern
capital = 306	# Murom

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1353.1.1 = {
	monarch = {
		name = "Fedor I"
		dynasty = "Glebovich"
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

# 1392 to Muscowy
