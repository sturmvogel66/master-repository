# ZHE - Chu (was He Zhen)
# LS - Chinese Civil War

government = chinese_monarchy
mercantilism = 0.0
technology_group = chinese
religion = confucianism
primary_culture = wuhan
capital = 674

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = -2 }
}

