# ZIN - Kel Gres

government = tribal_republic government_rank = 5
mercantilism = 0.0
primary_culture = tuareg
religion = sunni
technology_group = soudantech
capital = 2929

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1337.1.1 = {
	monarch = {
		name = "Moussa"
		dynasty = "Amastan"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
