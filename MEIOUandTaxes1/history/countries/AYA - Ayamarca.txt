# AYA - Ayamarca

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = quechuan
religion = inti
technology_group = south_american
# capital = 2069
capital = 2067 #?!

1356.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 3 }
	monarch = {
		name = "Tribal Council"
		dynasty = "Ayamarca"
		ADM = 4
		DIP = 2
		MIL = 4
	}
}
