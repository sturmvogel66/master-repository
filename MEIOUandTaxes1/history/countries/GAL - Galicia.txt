# GAL - Galicia

government = feudal_monarchy government_rank = 2
mercantilism = 0.0
primary_culture = galician
religion = catholic
technology_group = western
capital = 206	# Galicia

historical_rival = CAS
historical_rival = LEO
historical_rival = SPA

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = -3 }
}
