# CTM - Chactemal

government = tribal_monarchy government_rank = 3
mercantilism = 0.0
technology_group = mesoamerican
primary_culture = yucatecan
religion = mesoamerican_religion
capital = 843 # Chactemal

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1349.1.1 = {
	monarch = {
		name = "Monarch"
		#dynasty = "Xiu"
		ADM = 5
		DIP = 3
		MIL = 5
	}
}
