# RAU - Riau

government = eastern_monarchy government_rank = 5
mercantilism = 0.0
primary_culture = malayan
religion = sunni
technology_group = austranesian
capital = 659	# Riau

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1818.1.1 = {
	monarch = {
		name = "Abdul Rahman Muadzam Syah"
		dynasty = Syah
		ADM = 3
		DIP = 4
		MIL = 2
	}
}
