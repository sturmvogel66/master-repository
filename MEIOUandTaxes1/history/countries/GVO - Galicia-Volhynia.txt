# Kingdom of Halych-Volhynia (Regnum Galici� et Lodomeri�)
# Tag : GVO

government = despotic_monarchy government_rank = 3
mercantilism = 0.0
technology_group = eastern
religion = orthodox
primary_culture = ruthenian
capital = 260	# Galich
historical_neutral = MOL

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1340.1.1 = {
	monarch = {
		name = "Liubartas"
		dynasty = "Gediminai"
		ADM = 2
		DIP = 2
		MIL = 2		
	}
}

1349.1.1 = {
	capital = 2537
}

1366.9.1 = {
	capital = 260
}
1384.1.1 = {
	monarch = {
		name = "Teodoras Liubartaitis"
		dynasty = "Gediminai"
		ADM = 2
		DIP = 2
		MIL = 2		
	}
}

1431.1.1 = {
	monarch = {
		name = "�vitrigaila Algirdaitis"
		ADM = 2
		DIP = 2
		MIL = 2		
	}
}

#Halych-Volhynia
#dynasty - Gediminai
#Liubartas (1300(?)-1384) ruler - 1342-1384
#heir - Teodoras Liubartaitis(son) - ?-1431)
#Aleksandras Karijotaitis - Liubartas nephew and ruler a part of Hlaych-Volynia
#Luibertas about 1366 lost a port of his reign.
#Hlaych Volynia dynasty coherence and politics was very complicate and difficult
#�vitrigaila Algirdaitis (1370-1452)
#Volynia ruler - 1440-1452
