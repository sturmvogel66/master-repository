# BAC - Bacan
# GG - Modified from Divide et Impera

government = eastern_monarchy government_rank = 5
mercantilism = 0.0
primary_culture = moluccan
religion = hinduism
technology_group = austranesian
capital = 2431	# Bacan

1000.1.1 = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1 = {
	monarch = {
		name = "Kolano"
		dynasty = "Madehe"
		ADM = 4
		DIP = 4
		MIL = 2
	}
}
1500.1.1 = { 
	religion = sunni 
	government_rank = 5
}

1557.1.1 = {
	religion = catholic
	}

1578.1.1 = {
	religion = sunni
	} # Ternate invades and the king apostatizes

1750.1.1 = {
	monarch = {
		name = "Sultan Hamarullah"
		ADM = 5
		DIP = 4
		MIL = 4
	}
}
