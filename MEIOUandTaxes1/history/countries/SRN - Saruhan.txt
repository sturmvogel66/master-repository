# SRN - Saruhan

government = ottoman_government government_rank = 1
mercantilism = 0.0
primary_culture = turkish
religion = sunni
technology_group = muslim
capital = 318
historical_rival = BYZ
historical_neutral = MAM

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	add_country_modifier = { name = turkish_beylik duration = -1 }
	add_country_modifier = { name = obstacle_traditional_military duration = -1 }
	set_variable = { which = "centralization_decentralization" value = 0 }
}

1356.1.10 = {
	monarch = {
		name = "Fahruddin Ilyas"
		dynasty = "Bey"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

# 1390 - Fall to the Ottomans
