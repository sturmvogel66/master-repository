# DSL - Dar Sila

government = tribal_monarchy government_rank = 2
mercantilism = 0.0
technology_group = soudantech
religion = animism
primary_culture = foora
capital = 3316

1000.1.1 = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1 = {
	monarch = {
		name = "Ahmed"
		dynasty = "Al Daj"
		ADM = 1
		DIP = 1
		MIL = 1
	}
} # Invention
