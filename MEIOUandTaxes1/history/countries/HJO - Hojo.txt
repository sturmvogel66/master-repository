# HOJ - Hojo clan

government = early_medieval_japanese_gov
government_rank = 3
mercantilism = 0.0
primary_culture = kanto
religion = mahayana
technology_group = chinese
capital = 2293		# Musashi

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1490.1.1 = {
	monarch = {
		name = "Soun" # 1453 - 1519 
		dynasty = "Hojo"
		ADM = 5
		DIP = 4
		MIL = 5
		}
}

1490.1.1 = {
	heir = {
		name = "Ujitsuna"
		monarch_name = "Ujitsuna"
		dynasty = "Hojo"
		birth_date = 1487.1.1
		death_date = 1541.8.10
		claim = 90
		ADM = 5
		DIP = 4
		MIL = 4
	}
}
	
1519.9.18 = { 
	monarch = {
		name = "Ujitsuna"
		dynasty = "Hojo"
		ADM = 5
		DIP = 4
		MIL = 4
		}
	}

1519.9.18 = {
	heir = {
		name = "Ujiyasu"
		monarch_name = "Ujiyasu"
		dynasty = "Hojo"
		birth_date = 1515.1.1
		death_date = 1571.10.21
		claim = 90
		ADM = 5
		DIP = 5
		MIL = 5
	}
}

1541.4.15 = { 
	monarch = {
		name = "Ujiyasu"
		dynasty = "Hojo"
		ADM = 5
		DIP = 5
		MIL = 5
		}
	}

1541.4.15 = {
	heir = {
		name = "Ujimasa"
		monarch_name = "Ujimasa"
		dynasty = "Hojo"
		birth_date = 1538.1.1
		death_date = 1590.8.10
		claim = 90
		ADM = 4
		DIP = 3
		MIL = 3
	}
}

1571.10.21 = { 
	monarch = {
		name = "Ujimasa"
		dynasty = "Hojo"
		ADM = 4
		DIP = 3
		MIL = 3
		}
	}

1571.10.21 = {
	heir = {
		name = "Ujinao"
		monarch_name = "Ujinao"
		dynasty = "Hojo"
		birth_date = 1562.1.1
		death_date = 1591.12.19
		claim = 90
		ADM = 4
		DIP = 3
		MIL = 3
	}
}

# 1590 - Toyotomi Hideyoshi takes Odawara Castle, Hojo Ujimasa commits seppuku 