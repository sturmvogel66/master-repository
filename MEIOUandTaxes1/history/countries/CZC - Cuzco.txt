# Cuzco city-state
# Tag : CZC

government = despotic_monarchy 
government_rank = 4
mercantilism = 0.0
primary_culture = quechuan
religion = inti
technology_group = south_american
capital = 2068	# Cuzco

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1356.1.1 = {
	monarch = {
		name = "Manco Capac"
		dynasty = "Hanan"
		ADM = 4
		DIP = 2
		MIL = 4
	}
	add_country_modifier = { name = blessing_of_god duration = -1 }
}

1388.1.1 = {
	monarch = {
		name = "Inca Yupanqui"
		dynasty = "Hanan"
		ADM = 3
		DIP = 5
		MIL = 4
	}
	set_government_rank = 5
}

1401.1.1 = {
	monarch = {
		name = "Viracocha"
		dynasty = "Hanan"
		ADM = 3
		DIP = 3
		MIL = 1
	}
}

1432.1.1 = {
	monarch = {
		name = "Inca Urco"
		dynasty = "Hanan"
		ADM = 3
		DIP = 1
		MIL = 3
	}
}

1438.1.1 = {
	monarch = {
		name = "Pachacuti"
		dynasty = "Hanan"
		ADM = 6
		DIP = 5
		MIL = 6
	}
}

1471.1.1 = {
	monarch = {
		name = "Tupac-Yupanqui"
		dynasty = "Hanan"
		ADM = 6
		DIP = 4
		MIL = 6
	}
}

1493.1.1 = {
	monarch = {
		name = "Huayna-Capac"
		dynasty = "Hanan"
		ADM = 6
		DIP = 3
		MIL = 4
	}
}

1521.1.1 = {
		set_variable = { which = "centralization_decentralization" value = 5 }
	remove_country_modifier = blessing_of_god
	#add_country_modifier = { name = wrath_of_god duration = -1 }
} # The Incan Civil War

1523.1.1 = { leader = {	name = "Atoc"      	type = general	rank = 1	fire = 1	shock = 3	manuever = 2	siege = 0	death_date = 1532.1.1 } }

1523.1.1 = {
	monarch = {
		name = "Hu�scar I"
		dynasty = "Hanan"
		ADM = 2
		DIP = 1
		MIL = 0
		leader = { name = "Hu�scar"	type = general	rank = 1	fire = 1	shock = 3	manuever = 2	siege = 0 }
	}
	set_country_flag = inca_civil_war
	add_country_modifier = { name = obstacle_traditional_military duration = -1 }
}

1532.1.1 = {
	monarch = {
		name = "Atahuallpa"
		dynasty = "Hanan"
		ADM = 3
		DIP = 1
		MIL = 4
	}
}

1533.7.28 = {
	monarch = {
		name = "Tupac-Huallpa"
		dynasty = "Hanan"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1534.1.1 = {
	monarch = {
		name = "Manco"
		dynasty = "Hanan"
		ADM = 4
		DIP = 4
		MIL = 5
	}
}

1545.1.1 = {
	monarch = {
		name = "Sayri-Tupac"
		dynasty = "Hanan"
		ADM = 3
		DIP = 4
		MIL = 2
	}
}

1560.1.1 = {
	monarch = {
		name = "Titu-Cusi"
		dynasty = "Hanan"
		ADM = 5
		DIP = 5
		MIL = 3
	}
}

1571.1.1 = {
	monarch = {
		name = "Tupac-Amaru"
		dynasty = "Hanan"
		ADM = 1
		DIP = 1
		MIL = 2
	}
}
