# TXL - Tlacala
# Tlaxcala was a confederation of four altepetl: Ocotelolco, Quiahuiztlan, Tepeticpac and Tizatlan, which each took turns providing a ruler for Tlaxcala as a whole.
# As a result of their alliance with the Spaniards, Tlaxcala had a somewhat privileged status within Spanish colonial Mexico

government = tribal_monarchy government_rank = 2
mercantilism = 0.0
primary_culture = tlaxcallan
religion = nahuatl
technology_group = mesoamerican
capital = 2201 # Tlaxcallán

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5	}
}

1356.1.1 = {
	monarch = {
		name = "Aztahua"
		dynasty = "Tizatlan"
		ADM = 5
		DIP = 3
		MIL = 3
	}
}

1425.1.1 = {
	monarch = {
		name = "Xicotencatl"
		dynasty = "Tizatlan"
		ADM = 3
		DIP = 4
		MIL = 5
	}
}#ruler from 1425-1522

#1520.1.1 Tlaxcala allies with Spain against the Aztec
