# Country : Tsardom of Tarnovo
# Tag : TAR

government = feudal_monarchy government_rank = 4 #PRINCIPALITY
mercantilism = 0.0
technology_group = eastern
primary_culture = bulgarian
religion = orthodox
capital = 157	# Tarnovo
historical_rival = OTT
historical_rival = TUR

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1331.1.1 = {
	monarch = {
		name = "Ivan Aleksander" # Nephew of Tsar Michael III Asen
		dynasty = "Sisman" # In fact the Stratzimir dynasty from the name of his father
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1356.1.1 = {	
	heir = {
		name = "Ivan"
		monarch_name = "Ivan II"
		dynasty = "Sisman"
		birth_Date = 1350.1.1 # Fourth son of Ivan Alexander. His elder brother is Tsar in Vidin.
		death_Date = 1395.6.3 # Last Tsar, beheaded by the ottomans. The Tsardom is then annexed to the Ottoman Empire.
		claim = 50
		DIP = 3
		ADM = 3
		MIL = 3
	}
}


1371.2.17 = {
	monarch = {
		name = "Ivan II"
		dynasty = "Sisman"		
		ADM = 3
		DIP = 3
		MIL = 3
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

# Vassal of the ottomans from 1376 to 1388 (had to marry his sister Keratamas to sultan Murad)
# Revolts against the turks in 1388 and allies with Hungary.
# Tarnovo is captured in 1393 (17.07). Ivan II is executed in 1395.