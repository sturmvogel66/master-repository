# LIP - Liptaki

government = tribal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = mossi
religion = west_african_pagan_reformed
technology_group = sub_saharan
capital = 2903

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1333.1.1 = {
	monarch = {
		name = "Saydu"
		dynasty = "Brahima"
		DIP = 3
		ADM = 3
		MIL = 4
	}
}
