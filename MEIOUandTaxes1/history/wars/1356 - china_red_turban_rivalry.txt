name = "Red Turban Rivals"

war_goal = {
	type = unify_china
	casus_belli = cb_chinese_civil_war
	province = 1068			# Changzhou
}

1356.1.1   = {
	add_attacker = MNG
	add_attacker = SNG
	add_defender = ZOU
}
1366.1.1   = {
	rem_attacker = SNG
}
1367.10.1   = {
	rem_attacker = MNG
	rem_defender = ZOU
}
