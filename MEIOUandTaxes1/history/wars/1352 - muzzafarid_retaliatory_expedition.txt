name = "Muzzafarid Retaliatory Expedition"
#casus_belli = cb_conquest
war_goal = {
	type = take_claim
	casus_belli = cb_conquest
	province = 3082				# Isfahan
}

1352.1.1 = {
	add_attacker = MUZ
	add_defender = ISF
}

1357.1.1 = {
	rem_attacker = MUZ
	rem_defender = ISF
}
