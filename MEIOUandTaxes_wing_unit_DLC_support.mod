name="M&T Winged Hussars DLC Support"
path="mod/MEIOUandTaxes_wing_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 1.27"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesWH.jpg"
supported_version="1.19.*.*"
