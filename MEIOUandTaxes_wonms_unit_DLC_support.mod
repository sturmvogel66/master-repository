name="M&T Muslim Ships Unit DLC Support"
path="mod/MEIOUandTaxes_wonms_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 1.27"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesMS.jpg"
supported_version="1.19.*.*"
